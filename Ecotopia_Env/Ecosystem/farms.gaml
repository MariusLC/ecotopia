/**
* Name: farms
* Model of the farms for crop, meat and coton,
* Also simulate the consommation 
* Author: Damien Legros, Cedric Cornède, Petar Calic
* Tags: 
*/

model farms

import "../models/california_environment.gaml"
import "weather.gaml"
import "ecosystem.gaml"
import "../general/parameters.gaml"
import "../general/mini_city.gaml"

global {
    //liste des images des fermes et forets
    list<file> images_farms <- [
		file("../images/meat.png"),
		file("../images/vegetables.png"),
		file("../images/cotton.png"),
		file("../images/eraser.png"),
		file("../images/forest.png")]; 
	//liste des images jour nuit
	list<file> images_day_night <- [
		file("../images/day.png"),
		file("../images/night.png")]; 
	//liste des images des saisons
	list<file> images_season <- [
		file("../images/winter.png"),
		file("../images/spring.png"),
		file("../images/summer.png"),
		file("../images/autumn.png")]; 
	
	// pour récupérer la consommation en eau de la production totale
	reflex get_water_consommation{
		loop i over: farm_cell.population{
			if i.image=0{
				water_consommation <- water_consommation + i. meat_per_day* animal_consommation_water;
			}
			if i.image=1{
				water_consommation <- water_consommation + i. crop_per_day *crop_consommation_water;
			}
			if i.image=2{
				water_consommation <- water_consommation + i. cotton_per_day *cotton_consommation_water;
			}
		}
	}
	
	//pour récupérer la consommation en GHG de la production totale
	reflex GHGProduction when:every(24 #hours) {
		loop i over: farm_cell.population{
			if i.image=0{
				greenhouse_emissions <- greenhouse_emissions + i. meat_per_day* animal_production_ghg;
				agriculture_ghg_emissions <- agriculture_ghg_emissions+ i. meat_per_day* animal_production_ghg;
				biomass_waste <- biomass_waste + i. meat_per_day* animal_production_waste; 
				biomass_agriculture <- biomass_agriculture + i. meat_per_day* animal_production_waste; 
				
				greenhouse_emissions_produced <- greenhouse_emissions_produced + i. meat_per_day* animal_production_ghg;
				biomass_waste_produced <- biomass_waste_produced + i. meat_per_day* animal_production_waste;
			}
			if i.image=1{
				greenhouse_emissions <- greenhouse_emissions + i. crop_per_day *crop_production_ghg;
				agriculture_ghg_emissions <- agriculture_ghg_emissions + i. crop_per_day *crop_production_ghg;
				biomass_waste <- biomass_waste - i. crop_per_day* crop_conso_waste;
				biomass_agriculture_conso <- biomass_agriculture_conso + i. crop_per_day* crop_conso_waste;
				
				greenhouse_emissions_produced <- greenhouse_emissions_produced + i. crop_per_day* crop_production_ghg;
				biomass_waste_consumed <- biomass_waste_consumed + i. crop_per_day* crop_conso_waste;
				
			}
			if i.image=2{
				greenhouse_emissions <- greenhouse_emissions + i. cotton_per_day *cotton_production_ghg;
				agriculture_ghg_emissions <-agriculture_ghg_emissions + i. cotton_per_day *cotton_production_ghg;
				biomass_waste <- biomass_waste - i. cotton_per_day* cotton_conso_waste;
				biomass_agriculture_conso <- biomass_agriculture_conso +  i. cotton_per_day* cotton_conso_waste;
				
				greenhouse_emissions_produced <- greenhouse_emissions_produced + i. cotton_per_day* cotton_production_ghg;
				biomass_waste_consumed <- biomass_waste_consumed + i. cotton_per_day* cotton_conso_waste;
			}
		}
	}
	
	//type de bouton
	int button_type <- -1;
	
	//active le bouton selectionné
	action activate_button {
		button selected_but <- first(button overlapping (circle(1.0) at_location #user_location));
		if(selected_but != nil) {
			ask selected_but {
				ask button {bord_contour<-#black;}
				if (button_type != id) {
					button_type<-id;
					bord_contour<-#yellow;
				} else {
					button_type<- -1;
				}
				
			}
		}
	}
	
	//met a jour les cases de la grille par rapport au bouton selectionné
	action cell_management {
		farm_cell selected_cell <- first(farm_cell overlapping (circle(1.0) at_location #user_location));
//		write selected_cell;
		if(selected_cell != nil) {
			ask selected_cell {
				image <- button_type;
				switch button_type {
					match 0 {color <- #red; quantity_available <- meat;}
					match 1 {color <- #green; quantity_available <- crop;}
					match 2 {color <- #blue; quantity_available <- cotton;}
					match 3 {color <- #white; image <- -1;}
				}
			}
		}
	}
	
	action load_grid {
		file my_csv_file <- csv_file(grid_filename,",");
        matrix data <- matrix(my_csv_file);
        ask farm_cell {
            image <- int(data[grid_x,grid_y]);
            switch int(data[grid_x,grid_y]) {
				match 0 {color <- #red; quantity_available <- meat;}
				match 1 {color <- #green; quantity_available <- crop;}
				match 2 {color <- #blue; quantity_available <- cotton;}
				match 3 {color <- #white; image <- -1;}
			}
        }
	}
}


grid farm_cell height: grid_size width: grid_size neighbors: 8 {
	
	float meat_per_1km2 <- 800000 #kg; //rendement pour 1km2
	float crop_per_1km2 <- 850000 #kg; //rendement pour 1km2
	float cotton_per_1km2 <- 100000 #kg; //rendement pour 1km2
	
	float meat_capacity <- meat_per_1km2 * (cell_size*cell_size) #kg; //rendement par case
	float crop_capacity <- crop_per_1km2 * (cell_size*cell_size) #kg; //rendement par case
	float cotton_capacity <- cotton_per_1km2 * (cell_size*cell_size) #kg; //rendement par case
	
	float meat_grow <- 90.0; //temps de croissance de la viande
	float crop_grow <- 120.0; //temps de croissance des legumes
	float cotton_grow <- 120.0; //temps de croissance du coton
	
	float meat_per_day <- meat_capacity / meat_grow #kg; //rendement par jour
	float crop_per_day <- crop_capacity / crop_grow #kg; //rendement par jour
	float cotton_per_day <- cotton_capacity / cotton_grow #kg; //rendement par jour
	
	float meat_consommation_per_day <- 0.0; //consommation par jour de viande
	float crop_consommation_per_day <- 0.0; //consommation par jour de plantes
	float meat_cell_consommation <- 0.0; //consommation des animaux de plantes
	
	int image <- -1; //type de fermes (0: Elevage, 1: Champ, 2: Coton 3: PAS DE FERMES->GOMME)
	
	float meat <- meat_capacity max: meat_capacity; //quantité de viande présente sur la case
	float crop <- crop_capacity max: crop_capacity; //quantité de plantes présente sur la case
	float cotton <- cotton_capacity max: cotton_capacity; //quantité de coton présente sur la case
	
	float quantity_available; //USED FOR COMMANDS TO CHECK WHAT HAS ALREADY BEEN RESERVED.
	
//	reflex calculate_nb_cell {
//		// VG
//		float conso_crop_people <- meal_consomation_agent_per_day*nb_agent_init*1;
//		float conso_crop_animal_per_cell <- meat_per_day*animal_consommation_crop;
//		float conso_meat_people <- meal_consomation_agent_per_day*nb_agent_init*0;
//		float nb_m <- conso_meat_people / meat_per_day;
//		write "nb_meat cells needed = "+nb_m;
//		float nb_c <- ((conso_crop_people + ceil(nb_m)*conso_crop_animal_per_cell)/crop_per_day) -1;
//		write "nb_crop cells needed = "+nb_c;
//		write "total bilans = "+((ceil(nb_m)*meat_per_day)-conso_meat_people)+" ; "+((ceil(nb_c)*crop_per_day)-(conso_crop_people + ceil(nb_m)*conso_crop_animal_per_cell));
//		
//		// 1/3 2/3
//		float conso_crop_people <- meal_consomation_agent_per_day*nb_agent_init*(2/3);
//		float conso_crop_animal_per_cell <- meat_per_day*animal_consommation_crop;
//		float conso_meat_people <- meal_consomation_agent_per_day*nb_agent_init*(1/3);
//		float nb_m <- conso_meat_people / meat_per_day;
//		write "nb_meat cells needed = "+nb_m;
//		float nb_c <- ((conso_crop_people + ceil(nb_m)*conso_crop_animal_per_cell)/crop_per_day) -1;
//		write "nb_crop cells needed = "+nb_c;
//		write "total bilans = "+((ceil(nb_m)*meat_per_day)-conso_meat_people)+" ; "+((ceil(nb_c)*crop_per_day)-(conso_crop_people + ceil(nb_m)*conso_crop_animal_per_cell));
//		
//		// CARNI
//		float conso_crop_people <- meal_consomation_agent_per_day*nb_agent_init*0;
//		float conso_crop_animal_per_cell <- meat_per_day*animal_consommation_crop;
//		float conso_meat_people <- meal_consomation_agent_per_day*nb_agent_init*1;
//		float nb_m <- conso_meat_people / meat_per_day;
//		write "nb_meat cells needed = "+nb_m;
//		float nb_c <- ((conso_crop_people + ceil(nb_m)*conso_crop_animal_per_cell)/crop_per_day) -1;
//		write "nb_crop cells needed = "+nb_c;
//		write "total bilans = "+((ceil(nb_m)*meat_per_day)-conso_meat_people)+" ; "+((ceil(nb_c)*crop_per_day)-(conso_crop_people + ceil(nb_m)*conso_crop_animal_per_cell));
////		
////		
////		write "crop conso ppl = "+meal_consomation_agent_per_day*nb_agent_init*crop_ratio;
////		write "meat conso ppl = "+meal_consomation_agent_per_day*nb_agent_init*meat_ratio;
////		write "prod crop = "+crop_per_day*16;
////		write "prod meat = "+crop_per_day*3;
////		write "crop conso animal = "+meat_per_day*animal_consommation_crop*3;
////		write "bilan crop = "+((crop_per_day*16)-(meat_per_day*animal_consommation_crop*3+meal_consomation_agent_per_day*nb_agent_init*crop_ratio));
////		write "bilan meat = "+((meat_per_day*3)-(meal_consomation_agent_per_day*nb_agent_init*meat_ratio));
//	}
	
	reflex test when: every(24 #hours) { // Tests pour verifier si il y a des champs des 3 types sur la carte
		assert get_meat_cell()>0;
		assert get_crop_cell()>0;
		assert get_cotton_cell()>0;
		//tester si il reste de la viande dispo dans les fermes
		assert sum(farm_cell where (each.image=0) collect(each.meat))>0;
		//tester si il reste des légumes  dispo dans les fermes
		assert sum(farm_cell where (each.image=1) collect(each.crop))>0;
		//tester si il reste du coton dispo dans les fermes
		assert sum(farm_cell where (each.image=2) collect(each.cotton))>0;
	}
	
	aspect base{
		if (image >= 0) {
			draw image_file(images_farms[image]) size:{shape.width * 0.5,shape.height * 0.5};
		}
	}

	rgb color_update <- #white;
	rgb color <- #white update: color_update;
	
	reflex update_color {
		if image=0{
			color_update <- hsb(0.0,1.0*meat/meat_capacity,1.0);
		}
		if image=1{
			color_update <- hsb(0.33,1.0*crop/crop_capacity,1.0);
		}
		if image=2{
			color_update <- hsb(0.66,1.0*cotton/cotton_capacity,1.0);
		}
	}
	
//	float get_meat_consommation{ // pour récupérer la consommation en viande des mini-villes
//		meat_consommation_per_day <- 0.0;
//		if image=0{
//			loop i over: agents of_generic_species MiniCity{
//				meat_consommation_per_day <- meat_consommation_per_day + 2*(meat_consommation_per_minicity); // en kg
//			}
//		}
//		return meat_consommation_per_day;
//	} when: every(1 #day) 
//	
//	float get_crop_consommation{ // pour récupérer la consommation en plantes des mini-villes
//		crop_consommation_per_day <- 0.0;
//		if image=1{
//			loop i over: agents of_generic_species MiniCity{
//				crop_consommation_per_day <- crop_consommation_per_day + 2*(crop_consommation_per_minicity); // en kg
//			}
//		}
//		return crop_consommation_per_day;
//	}
	
    int get_meat_cell{ // pour avoir le nombre de cases ou il y a de la viande
		int meat_cell<- 0; 
		loop i over: farm_cell.population{
			if i.image=0{
				meat_cell <- meat_cell + 1;
			}
		}
		return meat_cell;
	}
	
	int get_crop_cell{ // pour avoir le nombre de cases ou il y a des plantes
		int crop_cell<- 0; 
		loop i over: farm_cell.population{
			if i.image=1{
				crop_cell <- crop_cell + 1;
			}
		}
		return crop_cell;
	}
	
	int get_cotton_cell{ // pour avoir le nombre de cases ou il y a du coton
		int cotton_cell<- 0; 
		loop i over: farm_cell.population{
			if i.image=2{
				cotton_cell <- cotton_cell + 1;
			}
		}
		return cotton_cell;
	}
	
	float get_animal_consommation{ // pour récupérer la consommation en plante de la production de viande 
		meat_cell_consommation <- 0.0;
		loop i over: farm_cell.population{
			if i.image=0{
				meat_cell_consommation <- meat_cell_consommation + i.meat_per_day*animal_consommation_crop;
			}
		}
		return meat_cell_consommation;
	}
	
	reflex grow when: every(24 #hours) { // chaque case a une quantité de viande qui lui est propre et si la case est pas vache la quantité est à 0 
		if image=0 {
			float old_meat <- meat;
			//EFM
			meat <- min([meat_capacity, meat + meat_per_day]);
			meat_prod <- meat_prod + min(meat_per_day, meat_capacity - meat);
			//
			quantity_available <- quantity_available + (meat - old_meat);
		}
		if image=1 {
			float animal_consommation <- get_animal_consommation(); // consommation total de plantes par les élevages
			int number_crop_cell <- get_crop_cell();
			int number_meat_cell <- get_meat_cell();
			animal_consommation <- animal_consommation/number_crop_cell;
			float old_crop <- crop; 
			// EFM
			crop <- min([crop_capacity, max([0,crop + crop_per_day - animal_consommation])]);
			crop_prod <- crop_prod + min(crop_per_day, crop_capacity - crop);
			//
			quantity_available <- quantity_available + (crop - old_crop);
		}
		if image=2 and flip(0.5) {
			float old_cotton;
			// EFM
			cotton_prod <- cotton_prod + min(cotton_per_day, cotton_capacity - cotton);
			cotton_water_conso <- cotton_water_conso + min(cotton_per_day, cotton_capacity-cotton)*cotton_consommation_water;
			//
			cotton <- min(cotton_capacity, cotton + cotton_per_day);
			quantity_available <- quantity_available + (cotton - old_cotton);
		} 
	}
	
	/*reflex consume when: every(1 #day)  { // ce reflex s'applique sur chaque case du coup là on retire la quantité de viande consommé total à chaque case
		if meat!=0.0 {
			float meat_consommation <- get_meat_consommation(); // consommation total de viande des minivilles pour une journée
			int number_meat_cell <- get_meat_cell();
			meat_consommation <- meat_consommation/number_meat_cell; // on retire la quantité consommé de manière uniforme à chaque case viande
			meat <- max([0, meat - meat_consommation ]);
		}
		if crop!=0.0 {
			float crop_consommation <- get_crop_consommation(); // consommation total de plantes des minivilles pour une journée 
			int number_crop_cell <- get_crop_cell();
			crop_consommation <- crop_consommation/number_crop_cell; 
			crop <- max([0, crop - crop_consommation]);
		}
		if cotton!=0.0 and flip(0.5) {cotton <- max([0, cotton - cotton_per_day]);}
	}*/
}

//Grille des boutons pour placer les fermes
grid button width:4 height:1 {
	int id <- int(self);
	list<rgb> bord_rect<- [
		#red,
		#green,
		#blue,
		#white
	];
	rgb bord_contour<- #black;
	aspect base {
		draw rectangle(shape.width * 0.8,shape.height * 0.8).contour + (shape.height * 0.01) color: bord_contour;
		draw rectangle(shape.width * 0.7,shape.height * 0.7) + (shape.height * 0.01) color: bord_rect[id];
		draw image_file(images_farms[id]) size:{shape.width * 0.5,shape.height * 0.5};
	}
}

experiment FarmsTest type: gui {
	
	parameter "Total Forest Surface in km2" var: GforestSurface;
	parameter "Number of thousands of trees per km2" var: Gtreeperkm2;
	parameter "GHG consumed by a single tree" var: GtreeConsoGhg;
	parameter "Coef of Natural forest renewal" var: GcoefNaturalGrowth;
	parameter "Natural forest death" var: GcoefNaturalDeath;
	parameter "Mass of a tree in t" var: GtreeMass;
	parameter "Minum alowed tree proportion " var: Gnb_TreesMinFactor;
	parameter "Low Acceptable tree proportion" var: GproportionOk;
	parameter "Maximum possible number of trees" var: GnbTreesMaxFactor;
	
	output {
		layout horizontal([vertical([0::6721,3::3279])::5000,vertical([horizontal([1::5000,2::5000])::5000,horizontal([4::5000,5::5000])::5000])::5000]) tabs:false toolbars:false;
		
		//display map visualisation
		display farms_display type: opengl {
	    	image "fronteers" gis: "../includes/california_fronteers.shp" color: rgb('white');
	    	species mountains aspect: base;
	    	species forest aspect: base transparency: 0.65;
	    	species desert aspect: base transparency: 0.65;
	    	species river aspect: base;
	        species city aspect: base;
	        species MiniCity;
	        species BigCity transparency: 0.7;
	        species BigCity aspect: pt;
	        species geothermal aspect: base;
	        species nuclear aspect: base;
	        species hydroelectric aspect: base;
	        species farm_cell aspect: base transparency: 0.75;
	        grid farm_cell border: #black transparency: 0.75;
	        event mouse_up action: cell_management;
	        
	    }
	    //display the action buttons
		display farms_charts background:#white refresh: every(24 #hours){
			species button aspect: base transparency: 0.65 size: {1,0.2} position: {0, 0};
			event mouse_down action:activate_button;    
			chart "Supply available in farms" size: {1,0.8} position: {0, 0.2} type:series
			{
				data "meat" value:sum(farm_cell where (each.image=0) collect(each.meat)) color:#red;
				data "crop" value:sum(farm_cell where (each.image=1) collect(each.crop)) color:#green;
				data "cotton*5500" value:sum(farm_cell where (each.image=2) collect(each.cotton*5500)) color:#blue;
			}
		}
	    display season_temperature_charts refresh: every(12 #hours){
	    	chart "Average temperature  in Celsius" size: {1,0.7} position: {0, 0} type:series
			{
				data "day" value:mean(weather_cell collect(each.day_temperature)) color:#orange;
				data "night" value:mean(weather_cell collect(each.night_temperature)) color:#blue;
			}
			species ecosystime aspect: base size: {0.29,0.27} position: {0, 0.58};
			species ecosyseason aspect: base size: {0.29,0.27} position: {0.4, 0.58};
	    }
	    display weather_charts refresh: every(12 #hours){
			chart "Number of cells with rain" size: {0.33,0.9} position: {0, 0} type:series
			{
				data "no rain" value:sum(weather_cell where (each.rain=0) collect(each.rain+1)) color:#orange;
				data "rainy" value:sum(weather_cell where (each.rain=1) collect(each.rain)) color:#blue;
			}
			chart "Number of cells with sun" size: {0.33,0.9} position: {0.33, 0} type:series
			{
				//data "no sun" value:sum(weather_cell where (each.sun=0) collect(each.sun+1)) color:#blue;
				data "cloudy" value:sum(weather_cell where (each.sun=1) collect(each.sun)) color:#grey;
				data "sunny" value:sum(weather_cell where (each.sun=2) collect(each.sun-1)) color:#orange;
			}
			chart "Average wind speed" size: {0.33,0.9} position: {0.66, 0} type:series
			{
				data "km/h" value:mean(weather_cell collect(each.wind)) color:#green;
			}
		}
		display consommation_charts refresh: every(24 #hours){
			chart "Consommation of food" size: {1,0.5} position: {0, 0} type:series
			{
				data "meat for people" value:sum(farm_cell where (each.image=0) collect(each.meat_consommation_per_day)) color:#red;
				data "crop for people" value:sum(farm_cell where (each.image=1) collect(each.crop_consommation_per_day)) color:#green;
				data "crop for animals" value:sum(farm_cell where (each.image=1) collect(each.meat_cell_consommation)) color:#yellow;
			}
			chart "Water consommation" size: {1,0.5} position: {0, 0.5} type: series
			{
                data "Water" value: water_consommation color: #blue;
            }
		}
		display ghg_biomass_charts refresh: every(24 #hours){
			chart "Biomass waste" size: {1,0.5} position: {0, 0} type:series
			{
				data "kg" value: biomass_waste color:#green;
			}
			chart "Greenhouse emissions" size: {1,0.5} position: {0, 0.5} type:series
			{
				data "kg" value: greenhouse_emissions color:#red;
			}
		}
	}
}
