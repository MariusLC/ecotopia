/**
* Name: ecosystem
* Model for the ecosystem, to work with :
* - the time
* - the seasons
* Author: Damien Legros
* Tags: 
*/


model ecosystem
import "../general/parameters.gaml"
import "farms.gaml"

global {
	init{
		write 'eco start';
		create ecosystem;
		ecosystem ecirh <- ecosystem at 0;
	}
}

//Grille d'un agent pour afficher le jour ou la nuit
grid ecosystime width:1 height:1 schedules: [] use_regular_agents: false {
	
	aspect base {
		switch first(ecosystem).night {
			match 0 {draw image_file(images_day_night[0]);}
			match 1 {draw image_file(images_day_night[1]);}
		}
	}
}

//Grille d'un agent pour afficher la saison
grid ecosyseason width:1 height:1 schedules: [] use_regular_agents: false {
	aspect base {
		switch first(ecosystem).season {
			match 0 {draw image_file(images_season[0]);}
			match 1 {draw image_file(images_season[1]);}
			match 2 {draw image_file(images_season[2]);}
			match 3 {draw image_file(images_season[3]);}
		}
	}
}

//Agent Ecosystème, s'occupe des déchets, des GHG et du temps
species ecosystem {
	
	int day <- -1;
	int month <- -1;
	int year <- 2021;
	
	int night <- 1; //0: day, 1: night
	int season <- 3; //0: winter, 1: spring, 2: summer, 3: autumn
	
	reflex update_night when: every(12 #hours){
		if night=1 {night <- 0;} 
		else {night <- 1;}
	}
	reflex update_season when: every(2160 #hours){
		season <- mod((season + 1),4);
	}
	reflex update_day when: every(24 #hours){
		day <- mod((day + 1), 30);
	}
	reflex update_month when: every(720 #hours){
		month <- mod((month + 1), 12);
	}
	reflex update_year when: every(8640 #hours){
		year <- year + 1;
	}
	
}
