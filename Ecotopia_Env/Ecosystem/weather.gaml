/**
* Name: weather
* Model of the weather :
* - wind
* - temperature
* - rain
* - sun
* Author: Damien Legros
* Tags: 
*/


model weather

import "../models/california_environment.gaml"
import "ecosystem.gaml"
import "../general/parameters.gaml"

//grille des agents pour la météo (vent, temperature, soleil et pluie)
grid weather_cell height: grid_size width: grid_size neighbors: 8 {
	
	int rain_capacity <- 2; //0: no rain, 1: rainy
	int sun_capacity <- 3; //0: no sun, 1: cloudy, 2: sunny
	
	int rain_update <- 0;
	int sun_update <- 1;
	float wind_update <- 9.0;
	float temperature_update <- 8.9;
	float night_temperature_update <- 8.9;
	float day_temperature_update <- 8.9;
	
	float temperature <- temperature_update update: temperature_update;
	float night_temperature <- night_temperature_update update: night_temperature_update;
	float day_temperature <- day_temperature_update update: day_temperature_update;
	int rain <- rain_update max: rain_capacity-1 update: rain_update;
	int sun <- sun_update max: sun_capacity-1 update: sun_update; 
	float wind <- wind_update update: wind_update;

	float temperature_of_the_month;
	float rain_of_the_month;
	float sunshine_of_the_month;
	float wind_of_the_month;
	
	reflex update when: every(12 #hour) {
		
		//tests on verifie que le temps est bien mis à jour normalement
		//que la pluie est 0 ou 1
		assert rain>=0 and rain<rain_capacity;
		//que le soleil est 0, 1 ou 2
		assert sun>=0 and sun<sun_capacity;
		//que le vent est supérieur ou égal à 0
		assert wind>=0;
		
		//write first(ecosystem).month;
		temperature_of_the_month <- temperature_per_month[first(ecosystem).month];
		rain_of_the_month <- rain_per_month[first(ecosystem).month];
		sunshine_of_the_month <- sunshine_per_month[first(ecosystem).month];
		wind_of_the_month <- wind_per_month[first(ecosystem).month];
		

		//rain
		if flip(rain_of_the_month/30.0) {rain_update <- 1;}
		else {rain_update <- 0;}
		loop el over: desert.population {
			if self overlaps el.shape {rain_update <- 0;} 
		}
		//sun
		if flip(sunshine_of_the_month/100.0) {sun_update <- 2;}
		else {sun_update <- 1;}
		loop el over: desert.population {
			if self overlaps el.shape {sun_update <- 2;} 
		}
		if first(ecosystem).night=1 {sun_update <- 0;}
		//wind
		float bound_under <- wind_of_the_month - wind_noise;
		float bound_upper <-  wind_of_the_month + wind_noise;
		wind_update <- rnd(bound_under ,bound_upper);
		//temperature
		float bound_under <- temperature_of_the_month - temperature_noise;
		float bound_upper <-  temperature_of_the_month + temperature_noise;	
		if first(ecosystem).night=1 {		
			temperature_update <- rnd(bound_under, temperature_of_the_month);
			night_temperature_update <- temperature_update;
		}
		else {
			temperature_update <- rnd(temperature_of_the_month,bound_upper);
			day_temperature_update <- temperature_update;
		}
		
	}
	
}
