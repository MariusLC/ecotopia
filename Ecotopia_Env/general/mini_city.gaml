/**
* Name: MiniCity
* Based on the internal empty template. 
* Author: Marius Le Chapelier and Co ( ie Damien Marillet ), Johann Alaya
* Tags: 
*/


model mini_city

import "../models/california_environment.gaml"
import "individual.gaml"
import "parameters.gaml"
import "../transport/Transport Marchandise.gaml"



global {
	
	// Variables calculées avec les variables du dessus
	int nb_minicity_per_city;
	int nb_cities; // nombre de villes réelles a patir desquelles on va faire nos constellations de mini-villes.
//	float consomation_per_minicity <- nb_agent_per_minicity*meal_consomation_agent; // consommation de nourriture par Mini-ville
	
	
	
	geometry global_fronteers <- create_fronteers();
	
	init {
		
	}
	
	bool checkIndexs(list indexs, matrix mat){
		loop id_x from: indexs[0] to:indexs[1] {
			loop id_y from: indexs[2] to:indexs[3] {
				if mat[id_x, id_y] = false {
					return false;
				}
			}
		}
		return true;
	}
	
	action setIndexs(list indexs, matrix grid){
		loop id_x from: indexs[0] to:indexs[1] {
			loop id_y from: indexs[2] to:indexs[3] {
				grid[int(id_x), int(id_y)] <- false;
			}
		}
	}
	
	action getWeather_Cell(MiniCity mc, point locat){
//		ask weather_cell {
//            if(self overlaps locat) {
//                mc.weath_cell <- self;
//            }
//        }
		mc.weath_cell <- weather_cell(locat);
	}
	
	BigCity create_big_city(string cityname, point loc, int nb_minicities, int constellation_population){
		float dc <- mean_diameter_city; // gauss({mean_diameter_minicity,mean_diameter_minicity*0.1})
//		write "CREATE BIG CITY";
//		write "nb_minicities = "+ nb_minicities;
//		list grid_city <- createCircularGrid(loc, dc, nb_minicities, cellSizeBigCityGrid);
//		write grid_city;
//		write "nb_columns = "+matrix(grid_city[0]).columns;
//		write "nb_rows = "+matrix(grid_city[0]).rows;
		
		float dmc <- mean_diameter_minicity; // gauss({mean_diameter_minicity,mean_diameter_minicity*0.1})
		int pmc <- constellation_population div nb_minicities; // calculer a partir de la population de BigCity ?
		int surplus_pop <- constellation_population mod nb_minicities;
		int p <- 0;
		if surplus_pop > 0 {
			p <- pmc + 1;
			surplus_pop <- surplus_pop - 1;
		} else {
			p <- pmc;
		}
//		write "pmc = " + pmc;
//		write "surplus_pop = " + surplus_pop;
		int nb_buildings_to_create <- 0; // to change in Dwelling
//		list grid_minicity <- createCircularGrid(loc, dmc, nb_buildings_to_create, cellSizeMiniCityGrid);

		map<string,float> Rstock <- initial_resourceStock(p);

		create BigCity as: MiniCity returns: list_bigcities {
			// the city
			name <- cityname;
			location <- loc;
			constellationDiameter <- mean_diameter_city;
			constellationPopulation <- constellation_population;
//			availability_matrix_bigcity <- grid_city[0];
//			availability_matrix_bigcity_size <- grid_city[1];
			list_minicities_constellation <- [];
			resourceStock <- Rstock;
	
			//the mini_city
			diameter <- dmc;
			population <- p;
			
			shape <- circle(diameter/2);
//			availability_matrix_minicity <- grid_minicity[0];
//			availability_matrix_minicity_size <- grid_minicity[1];
		}
		
		// take the bigcity
		BigCity bigcity <- list_bigcities[0];
		
		do getWeather_Cell(bigcity,loc);
		
		// addTrainStation
		ask bigcity {
			do addTrainStation;
		}
		
		// add individus
		bigcity.individus <- create_individu(bigcity.population, bigcity);
		
		// add the bigcity's minicity to the constellation's list of minicities 
		add bigcity to: bigcity.list_minicities_constellation;
		
		// create the buildings (from dwelling)
//		BigCity bigcity <- list_bigcities[0];
		int a <- bigcity.initMiniCityBuildingsFromParameters();
		
		ask bigcity {
			do setBuildingsIndividual;
		}
		
		
		// create all minicities from bigcity
//		do create_minicities_from_bigcity(bigcity, nb_minicities, surplus_pop, pmc);
		
//		write "BIGCITY GRID AFTER MINCITIES:";
//		write bigcity.availability_matrix_bigcity;
		return bigcity;
	}
	
	action create_minicities_from_bigcity(BigCity bigcity, int nb_minicities, int surplus_pop, int pmc){
		// On compte avec la population de la minicity de BigCity
//		write "BIGCITY GRID BEFORE MINCITIES:";
//		write bigcity.availability_matrix_bigcity;
		int pop <- bigcity.population;
		bool can_still_build <- true;
		point minicity_location;
		list res;
		MiniCity minicity;
		float diameter_mc <- mean_diameter_minicity;
		if (nb_minicities-1) > 0 {
			loop i from:0 to:nb_minicities-2 {
//				write "CREATE MINICITY";
//				res <- add_MiniCity_to_Constellation(bigcity, can_still_build);
				res <- bigcity.addMiniCity(diameter_mc);
				can_still_build <- res[0];
				minicity_location <- res[1];
				if can_still_build {
//					write "instanciate minicity : "+bigcity.name+" , loc = "+minicity_location;
					if surplus_pop > 0 {
						minicity <- create_mini_city(bigcity, pmc+1, minicity_location, diameter_mc);
						surplus_pop <- surplus_pop - 1;
					} else {
						minicity <- create_mini_city(bigcity, pmc, minicity_location, diameter_mc);
					}
					pop <- pop + minicity.population;
					// add the minicities to the constellation's list of minicities 
					add minicity to: bigcity.list_minicities_constellation;
				} else {
					warn "ERROR : NE PEUT PAS CONSTRUIRE DE MINICITY DANS "+ bigcity.name +", minicity num "+i; // n'est pas sensé arriver
				}
//				write "BIGCITY GRID:";
//				write bigcity.availability_matrix_bigcity;
			}
		}
	}
	
	MiniCity create_mini_city(BigCity bigCity, int pop, point minicity_location, float diameter_mc){
//		point l <- bigCity.location;
		int nb_building_to_create <- 0; // a changer dans dwelling
//		list grid_minicity <- createCircularGrid(minicity_location, diameter_mc, nb_building_to_create, cellSizeMiniCityGrid); // matrix et sa taille
		map<string,float> Rstock <- initial_resourceStock(pop) ;
		
		create MiniCity returns: list_minicities {
			location <- minicity_location;
			diameter <- diameter_mc;
			population <- pop; 
//			availability_matrix_minicity <- grid_minicity[0];
//			availability_matrix_minicity_size <- grid_minicity[1];
			resourceStock <- Rstock;
			shape <- circle(diameter/2);
			bigcity <- bigCity;
		}
		
		MiniCity minicity <- list_minicities[0]; // on récupère la minicity à partir du returns (un seul élément dans la liste)
		
		do getWeather_Cell(minicity,minicity_location);
		
		// addTrainStation
		ask minicity {
			do addTrainStation;
		}
		
		// create the individuals
		minicity.individus <- create_individu(pop, minicity);
		
		// create the buildings (from dwelling)
		int a <- minicity.initMiniCityBuildingsFromParameters();
		
		ask minicity {
			do setBuildingsIndividual;
		}
		

		return minicity;
	}
	
	
	
	map initial_resourceStock(int population){
		float q <- meal_quantity_stock*population;
		map<string, float> resourceStock <- create_map(["wood","cotton","plastic","meat","crop"],[wood_consumption_per_floor*max_nb_floors_wooden_houses,plastic_consumption_per_house*cotton_consumption_for_plastic,plastic_consumption_per_house,q*meat_ratio,q*crop_ratio]);
		return resourceStock;
	}
	
	action instanciate_MiniCities {
		
		// calling cities initialisation in california_environment file
		int nb_cities <- self create_cities [];
//		do create_cities returns: nb_cities;
//		nb_cities <- int(nb_cities);
		
		// Print simulation variable :
		write "--------------------------------\n\tSIMULATION VARIABLES\n--------------------------------";
		write "nb_cities = "+nb_cities;
		write "nb_minicity_init = " + nb_minicity_init;
		write "nb_agent_init = "+ nb_agent_init;
		
		
		write "--------------------------------\n\tDISTRIBUTION MINICITIES AND INDIVIDUS\n--------------------------------";
		
		// Distribution MiniCities over BigCities 
		nb_minicity_per_city <- nb_minicity_init div int(nb_cities);
		int nb_agent_per_city <- nb_agent_init div int(nb_cities);
		list minicities_repartition_in_cities <- list_with(int(nb_cities), nb_minicity_per_city);
		list agents_repartition_in_cities <- list_with(int(nb_cities), nb_minicity_per_city*nb_agent_per_minicity);

		// Distribution extra MiniCities over BigCities
		int nb_minicities_repartition <- int(nb_cities)*nb_minicity_per_city;
		int i <- 0;
		loop while: nb_minicities_repartition < nb_minicity_init {
			minicities_repartition_in_cities[i] <- minicities_repartition_in_cities[i] + 1; // adding a minicity to the city
			agents_repartition_in_cities[i] <- agents_repartition_in_cities[i] + nb_agent_per_minicity; // adding the corresponding number of agents to the city
			nb_minicities_repartition <- nb_minicities_repartition + 1;
			i  <- i + 1;
		}
		int nb_agents_repartition <- sum(agents_repartition_in_cities);
		
		// Print simulation variable :
		write "MiniCity initialisation variables :";
		write "nb_minicity_per_city = " + nb_minicity_per_city;
		write "nb_agent_per_minicity = " + nb_agent_per_minicity;
		write "starting minicities_repartition_in_cities : \n" + minicities_repartition_in_cities;
		write "" + sum(minicities_repartition_in_cities) + " minicities distributed";
		write "" + sum(agents_repartition_in_cities) + " agents distributed";
		write "" + (nb_agent_init - nb_agents_repartition) + " agents left to distribute";
		write "starting agents_repartition_in_cities : \n" + agents_repartition_in_cities;
		
		// Distribution extra agents over BigCities
		int i <- 0;
		loop while: nb_agents_repartition < nb_agent_init {
			agents_repartition_in_cities[i] <- agents_repartition_in_cities[i] + 1;
			nb_agents_repartition <- nb_agents_repartition + 1;
			i <- (i = int(nb_cities)-1) ? 0 : i + 1;
		}
		
		// Agents Distribution after the extra agents distribution
		write "  ---- Agents distribution over BigCities.. ----";
		write "agents_repartition_in_cities : \n" + agents_repartition_in_cities; 
		write "" + sum(agents_repartition_in_cities) + "/" + nb_agent_init + " agents distributed";
		write "" + (nb_agent_init - nb_agents_repartition) + " agents left to distribute";
		write "Distribution over, let's instanciate the cities and agents";
		
		// Creating the BigCities
		write "--------------------------------\n\tINSTANCIATING MINICITIES AND INDIVIDUS\n--------------------------------";
		int nb_mc <- 0;
		BigCity bigcity;
		city c;
		// instanciate all bigcities
		loop i from:0 to:length(city)-1 {
			c <- city[i];
			write "instanciate city : "+c.name+" , loc = "+c.location;
			bigcity <- create_big_city(c.name, c.location, minicities_repartition_in_cities[i], agents_repartition_in_cities[i]);
		}
		
		// instanciate all minicities
		loop i from:0 to:length(city)-1 {
			BigCity bc <- BigCity[i];
			int pmc <- agents_repartition_in_cities[i] div minicities_repartition_in_cities[i];
			int surplus_pop<-0;
			if minicities_repartition_in_cities[i]!=1{
				surplus_pop <- (agents_repartition_in_cities[i] - bc.population) mod (minicities_repartition_in_cities[i]-1);
			}
			do create_minicities_from_bigcity(bc, minicities_repartition_in_cities[i], surplus_pop, pmc);
			nb_mc <- nb_mc + minicities_repartition_in_cities[i];
		}
		
		// set people's location in house
		loop ind over:individu {
			ind.location <- any_location_in(ind.house);
		}
	}
}
	
species MiniCity {
	float diameter;
	int population;
	list<individu> individus;
//	matrix availability_matrix_minicity;
//	int availability_matrix_minicity_size;
	list<building> l_buildings<-[];
	BigCity bigcity;
	map<string,float> resourceStock; // Resources are managed by a map. Keys = Name of resources. Values = Current Stock.
//<<<<<<< HEAD
	bool cottonCommand;
	bool meatCommand;
	bool cropCommand;
	bool woodCommand;
	
	//transport
	reflex commandCotton when:(resourceStock["cotton"] <STOCK_MIN and !cottonCommand){ 
		cottonCommand <- true;
//		write "cotton "+resourceStock["cotton"];
    	ask MerchandiseManager {
	    	do CommandMerchandise(myself, myself.location ,"cotton" , STOCK_MIN - myself.resourceStock["cotton"] + 100.0); // +100 pour que les villes aie un peu de marge avant de devoir refaire une commande
	    }
    }
	    
    reflex commandCrop when:(resourceStock["crop"] <STOCK_MIN and !cropCommand){ 
		cropCommand <- true;
//		write "crop "+resourceStock["crop"];
    	ask MerchandiseManager {
	    	do CommandMerchandise( myself, myself.location ,"crop" , STOCK_MIN - myself.resourceStock["crop"] + 100.0);
	    }
    }
    
    reflex commandMeat when:(resourceStock["meat"]<STOCK_MIN and !meatCommand){
    	meatCommand <- true;
//		write "meat "+resourceStock["meat"];
    	ask MerchandiseManager {
	    	do CommandMerchandise( myself, myself.location ,"meat" , STOCK_MIN - myself.resourceStock["meat"] + 100.0);
	    }
    }
    
    reflex commandWood when:(resourceStock["wood"]<STOCK_MIN and !woodCommand){
    	woodCommand <- true;
//		write "wood "+resourceStock["wood"];
    	ask MerchandiseManager {
	    	do CommandMerchandise( myself, myself.location ,"wood" , STOCK_MIN - myself.resourceStock["wood"] + 100.0);
	    }
    }
    
    reflex manageStocks when:(cottonCommand or meatCommand or cropCommand or woodCommand){
    	if resourceStock["meat"] > STOCK_MIN{meatCommand <- false;}
    	if resourceStock["crop"] > STOCK_MIN{cropCommand <- false;}
    	if resourceStock["cotton"] > STOCK_MIN{cottonCommand <- false;}
    	if resourceStock["wood"] > STOCK_MIN{woodCommand <- false;}
    	if resourceStock["plastic"] > maxPlasticStocks{
    		loop Fact over:(l_buildings of_species plasticFactory){
    			ask Fact{
    				do stopProduction;
    			}
    		}
    	}
    	if resourceStock["plastic"] < plastic_consumption_per_house{
    		loop Fact over:(l_buildings of_species plasticFactory){
    			ask Fact{
    				do startProduction;
    			}
    		}
    	}
    }
//    
//    reflex checkRessources {
//    	write("Stocks de la ville: "+self.name);
//    	write("viande: "+resourceStock["meat"]);
//    	write("bois: "+resourceStock["wood"]);
//    	write("légumes: "+resourceStock["crop"]);
//    	write("coton: "+resourceStock["cotton"]);
//    }
//	    
	//fin transport    
	
 
//=======
	weather_cell weath_cell;
//>>>>>>> merge_energy_logement_ecosystem*/
	
	point PickRandomLocationInCircle(float circle_diameter, point center, float size_cell, list<geometry> allShapes, geometry objectToFit) {
         /*  
         * circle_diameter, center : diameter and center of the bouding circle
         * size_cell : use to determine in the discretisation of the area of the bouding circle, two consecutive cells will be separated by a distance equals to size_cell
         * objectToFit: shape of the object we want to insert in the bouding circle
         *  allShapes: all shapes that are already in the bouding circle we want ton insert objectToFit in
         */ 
         int size <-  floor(circle_diameter/size_cell); //number of cells in a column/lign in our discretisation of the area of the bouding circle
         float radius <- circle_diameter/2;

         //bounding circle
         geometry c <- circle(radius,center);
         
//         int nb_cells_ava <- 0;
//         loop i over: shuffle(range(size)) {
//            loop j over:  shuffle(range(size)) {
//                point posCandidate <- {i*size_cell,j*size_cell} +center-{radius,radius};
//                geometry geom_at_ij <- objectToFit at_location posCandidate;
//                //check thats its in the circle
//                if ( c inter geom_at_ij = geom_at_ij) {
//                	// check if in fronteers (not in water)
//                	if (geom_at_ij inter global_fronteers = geom_at_ij ){
//                		nb_cells_ava <- nb_cells_ava + 1;
//            		}
//            	}
//            }
//        }
        
         // pass over all positions of discrete space ( in random order )
         loop i over: shuffle(range(size)) {
            loop j over:  shuffle(range(size)) {
                point posCandidate <- {i*size_cell,j*size_cell} +center-{radius,radius};
                geometry geom_at_ij <- objectToFit at_location posCandidate;

                //check thats its in the circle
                if ( c inter geom_at_ij = geom_at_ij) {
                	
//                	check if in fronteers (not in water)
                	if (geom_at_ij inter global_fronteers = geom_at_ij ){
	                    //check that it wont collide with another building/miniCity
	                    bool validSpot<-true;
	                    loop g over: allShapes {
	                        if (geom_at_ij inter g != nil) {
	                            validSpot<-false;
	                        }
	                    }
	                    //if a valid spot has been found
		                if validSpot {
		                	return posCandidate;
		                }
		        	}
                }
            }
        }
//        write " NB CELLS AVA = "+nb_cells_ava;
        return nil;
    }
    
    action setBuildingsIndividual{
    	loop indi over:individus {
    		indi.house <-getAvailableHouse(l_buildings);
    		ask indi.house {
    			 do addResident(indi);
    		}
    		assert indi.house != nil;
    		
    		indi.atFunZone <- any(l_buildings of_generic_species funZone);
    		
    		
    		switch indi.activity{
    			match "Inactif"{break;}
    			match "Actif"{
    				indi.workPlace <- any(l_buildings of_generic_species company); 
    				
    				assert indi.workPlace!=nil;
    				
    				ask indi.workPlace {
    					do addEmployee(indi);
    				}
    			}
    			match "Etudiant"{
    				indi.workPlace <- any(l_buildings of_generic_species school); 
    				school cast_workPlace <-school(indi.workPlace);
    				ask  cast_workPlace{
    					do addStudent(indi);
    				}
    			}
    			match "Scolarise"{
    				indi.workPlace <- any(l_buildings of_generic_species school); 
    				school cast_workPlace <-school(indi.workPlace);
    				ask  cast_workPlace{
    					do addStudent(indi);
    				}
    			}
    			default{
    				warn " could not instantiate a workPlace to the agent "+indi+ " at init";
    			}
    		}
    		//findNewAvailableBuildingOfSpecificType(list<building> buildingToAvoid, string typeOfBulding, individu ind)
    		
    		location <- any_location_in (indi.house);
    	}
    	
    }
    
    // Return a reference of an instance housing in the mini-city where there is still room for at least one person
    housing getAvailableHouse( list<building> lb){
    	list<housing> allHouses <- lb of_generic_species(housing);
    	loop h over: allHouses{
    		//write " nR et maxC : "+ h.numberResidents +"  "+h.maxCapacity;
    		if(h.numberResidents<h.maxCapacity)
    		{
    			return h;
    		}
    	}
    	//TODO: handle the case where there is no available houses in the mini-city ( when the population will be dynamic )
    	warn " No available house in the mini-city";
    	return nil;
    }
    action addTrainStation{
		// Creating a trainStation at the center of the miniCity
		create trainStation number: 1 returns:tsl;
		trainStation ts <- tsl[0];
		bool hasSucceded<- AddBuildingAtSpecificPos(ts,self.location);
//		write " trainStation added successfully to miniCity "+ self.name +" ? "+ hasSucceded;
		if not hasSucceded
		{
			ask ts
			{
				do die;
			}
		}
		assert hasSucceded; // a trainStation must be created at the center of miniCity for the simulation to be coherent ! if error maybe try with a bigger miniCity size or lower the trainStation size !
	}
    
    // There has to be a manager so the buildings don't manage the stock directly.
	bool askForResource (string resource, float quantity) { // Placeholder for stock management.
		if (resourceStock[resource] >= quantity) { //TODO: To be tested ; can cause errors if resource isn't a key of the stock. 
			resourceStock[resource] <- resourceStock[resource] - quantity;
				
			return true;
		} 
		else {
			return false;
		}
	}
	
	// Give the max resources on the stock, might be uncomplete.
	bool askForResources (map<string, float> resources) { // Placeholder for stock management.
		list<string> keys <- resources.keys;
		loop i over: range(length(resources)-1) {
			if (resourceStock[keys[i]] >= resources[keys[i]]) { // To be tested ; can cause errors if resource isn't a key of the stock.
				resourceStock[keys[i]] <- resourceStock[keys[i]] - resources[keys[i]];
				return true;
			} 
			else {
				return false;
			}
		}
	}
	
	// Only give the resources if their is enough of every resources asked
	bool askForResources_complete (map<string, float> resources) { // Placeholder for stock management.
		list<string> keys <- resources.keys;
		int index <- 0;
		loop i over: range(length(resources)-1) {
			if (resourceStock[keys[i]] >= resources[keys[i]]) { // To be tested ; can cause errors if resource isn't a key of the stock.
//				write ""+resources[keys[i]]+"kg of "+keys[i]+" added to the meal, stock = "+resourceStock[keys[i]]+"kg";
				resourceStock[keys[i]] <- resourceStock[keys[i]] - resources[keys[i]];
				// EFM
				total_consommation[i] <- total_consommation[i] + resources[keys[i]];
				//
				index <- i;
			} else {
//				write " quantity asked : "+resources[keys[i]]+", quantity in stock : "+resourceStock[keys[i]];
//				write "there is no "+keys[i]+" enough";
				break;
			}
		}
		if index != length(resources)-1 and index > 0 { // if we don't have the quantity of some resource in stock, give the resources back
			loop i over: range(index-1) { 
//				write "we give "+keys[i]+" back";
				resourceStock[keys[i]] <- resourceStock[keys[i]] + resources[keys[i]];
				// EFM
				total_consommation[i] <- total_consommation[i] - resources[keys[i]];
				//
			}
			return false;
		} else {
			return true;
		}
		
	}
	
	action giveResource (string resource, float quantity) {
		resourceStock[resource] <- resourceStock[resource] + quantity;
	}
	
	
	action giveResources (map<string, float> resources) {
		list<string> keys <- resources.keys;
		loop i over: range(length(resources)-1) {
			resourceStock[keys[i]] <- resourceStock[keys[i]] + resources[keys[i]];	
		}
	}
    
	// DWELLING
	
	// check for ressources if a woodenHouse can be built, and do it if it can and returns the reference to the house, or nil if it couldnt built the house
	woodenHouse buildWoodenHouse{
		
		// check min and max bounds for number floors possible, given the current resourceStock
		float curr_wood_quantity <- resourceStock["wood"];
		int bound_max_nb_of_floors <- curr_wood_quantity div wood_consumption_per_floor;
		
		
		if ( bound_max_nb_of_floors<min_nb_floors_wooden_houses ) // not enough resources
		{
			warn "Warning in buildWoodenHouse method :a woodenHouse could not be added to the miniCity ( not enough wood in the miniCity)";
			return nil;
			
		}
		else
		{
			create woodenHouse number:1 returns: wH;
			
			int new_nb_of_floors <- rnd(min_nb_floors_wooden_houses,bound_max_nb_of_floors);
			ask wH
			{
				do resetSpecificNumberOfFloors(new_nb_of_floors);
			}
			bool couldObtainResource <- askForResource( "wood", new_nb_of_floors*wood_consumption_per_floor); 
			assert couldObtainResource; // should always be true inn this "else" section
			bool hasSucceded <- addBuilding(wH[0]);
			if not hasSucceded
			{
				ask wH //destroy woodenHouse instance
				{
					do die;
				}
				warn "Warning in buildWoodenHouse method :a woodenHouse could not be added to the miniCity ( no enough space left in the miniCity)";
				return nil;
			}
			return wH[0];
		}
	}
	
	// check for ressources if a modularHouse can be built, and do it if it can and returns the reference to the house, or nil if it couldnt built the house
	modularHouse buildModularHouse{
		
		float total_plastic_needed <- plastic_consumption_per_house; 
		if (askForResource ("plastic",total_plastic_needed))
		{
			
			create modularHouse number:1 returns: mH;
			bool hasSucceded <- addBuilding(mH[0]);
			
			if not hasSucceded 
			{
				
				ask mH //destroy modularHouse instance
				{
					do die;
				}
				
				do giveResource("plastic",total_plastic_needed); // giving back resources taken from resourceStock
				warn "Warning in modularHouse method :a modularHouse could not be added to the miniCity ( not enough space left in the miniCity)";
				return nil;
			}
			else
			{
				return mH[0]; //the operation is a success!
			}
			
		}
		do giveResource("plastic",total_plastic_needed); // giving back resources taken from resourceStock
		warn "Warning in modularHouse method: a modularHouse could not be added to the miniCity ( not enough plastic in the miniCity resourceStock)";
		return nil;
		
	}
	
	//if possible will add a building in the mini-city at any location that is available, return false if it fails to do so
	bool addBuilding(building b){
		list<geometry> allShapes <- l_buildings collect each.shape ;
		
		float size_cell <- min_length_building;
		
		point p <- PickRandomLocationInCircle(self.diameter, self.location, size_cell, allShapes , b.shape);
		
		if (p!= nil) {
			ask b {
				set location <- p;
				set minicity <- myself;
				add b to:myself.l_buildings;
			}
		}
		return p!=nil;
	}

	//if possible will add a building in the mini-city at a specific position, return false if it fails to do so
	bool AddBuildingAtSpecificPos(building b, point pos){
		geometry objectToFit <- b.shape at_location pos;
        list<geometry> allShapes <- l_buildings collect each.shape;
         //this miniCity circle
         geometry c <- self.shape;
         
        //check thats its in the circle
	    if ( c inter objectToFit = objectToFit) {
			//check that it wont collide with another building/miniCity
	        bool validSpot<-true;
	        loop g over: allShapes {
	            if (objectToFit inter g != nil) {
	                validSpot<-false;
	            }
	        }
	        //if a valid spot has been found
	        if validSpot{
				ask b {
					set location <- pos;
					set minicity <- myself;
					add b to:myself.l_buildings;
				}
	        	return true;
	        }	
		}
		return false;
	}
    
    action moveMiniCity( point p ) { // move miniCity and all its buildings
    	loop b over: l_buildings {
        	point offsetToCenter <- b.location-self.location;
        	b.location <- p + offsetToCenter;
        }
        self.location <- p;
    }
    
    action initMiniCityBuildingsFromMap( map<string,int> initMap) {
		  /*
		 initMatrix is a matrix where eah element is of form <NameOfTheSpeciesWeWantToAdd,HowManyWeWillTryToInstantiate>
		 this method will then create all buildings and try to ad them to the mini-City
		  */
		loop i over: range(length(initMap)-1) {
			list<building> l_b<-[];
			
			//get name of building class and number of buildings
			string b_type <- initMap.keys[i]; 
			int b_nb<- initMap.values[i];
			
			//create a list of buildings of the corresponding building type
			switch species(b_type) {
				match woodenHouse { create woodenHouse number:b_nb returns: l; if l != nil { l_b <- l_b+l; }} //  works; create the good type and number of buildings 
				match modularHouse { create modularHouse number:b_nb returns: l; if l != nil { l_b <- l_b+l; }}
				match park { create park number:b_nb returns: l; if l != nil { l_b <- l_b+l; }}
				match plasticFactory { create plasticFactory number:b_nb returns: l; if l != nil { l_b <- l_b+l; }}
//				match trainStation { create trainStation number:b_nb returns: l; if l != nil { l_b <- l_b+l; }}
				match school { create school number:b_nb returns: l; if l != nil { l_b <- l_b+l; }}
				match business { create business number:b_nb returns: l; if l != nil { l_b <- l_b+l; }}
				match healthCenter { create healthCenter number:b_nb returns: l; if l != nil { l_b <- l_b+l; }}
				match funZone { create funZone number:b_nb returns: l; if l != nil { l_b <- l_b+l; }}
				default { warn " the class name '"+b_type+"' does not correspond to any building types"; }
			}
			
			//adding all buildings of this type to the miniCity
			loop b over:l_b {
				bool hasSucceded <- addBuilding(b);
				if hasSucceded {
//					write "creation of " +b_type+" went just great !  (unless...? (°!° ) )";
				}
				if not hasSucceded {
					ask b {
						//write("DIE : "+string(b));
						do die;
					}
					warn "Warning in initMiniCityBuildingsFromMat method : all of the buildings could not be added to the miniCity ( most likely because there is no more room left in the miniCity for this building)";
				}
				//assert not hasSucceded warning:true; //launch a warning if all of our buildings could not be added to the miniCity ( most likely because there is no more room left in the miniCity for this building)
			}
		}
	}
	
	
	int initMiniCityBuildingsFromParameters{
		//assert distribution percentages are valids
		assert percentWoodenHouse>=0 and percentWoodenHouse<=1;
		assert percentFunZone + percentBusiness +percentPlasticFactory + percentSchool + percentHealthCenter = 1;
		
		//init from percentages
		int nb_minors <- individus count (each.age <= 18);
		int nb_adults <- (population - nb_minors);
		int nb_actives <- individus count (each.activity = "Actif"); // with state ?
		
//		int nb_trainStation <- 1;
		int nb_schools <- ceil ( nb_minors/maxStudentsCapacity );
		int nb_woodenHouse <-  ceil (  percentWoodenHouse * population/(max_dwelling_cap*min_nb_floors_wooden_houses) );
		int nb_modularHouse <-  ceil (  ( 1 - percentWoodenHouse) * population/max_dwelling_cap);
		//write "nb of Housing per mini city "+nb_woodenHouse+nb_modularHouse;
		int nb_business <- ceil ( percentBusiness * nb_adults/max_number_employees );
		int nb_plasticFactory <-  ceil (  percentPlasticFactory * nb_adults  /max_number_employees);
		int nb_healthCenter <-  ceil (   percentHealthCenter * nb_adults  /max_number_employees);
		int nb_funZone <-  ceil (  percentFunZone * nb_adults  /max_number_employees);
		
		int nb_parks <-  ceil ( (nb_schools + nb_woodenHouse + nb_modularHouse + nb_business + nb_plasticFactory + nb_healthCenter + nb_funZone )/nb_of_buildings_per_park  ); 
		
		map<string,int> initMap <- ["school"::nb_schools,"woodenHouse"::nb_woodenHouse,"modularHouse"::nb_modularHouse,
		"business"::nb_business,"plasticFactory"::nb_plasticFactory,"healthCenter"::nb_healthCenter,"funZone"::nb_funZone,"park"::nb_parks];
		
		
		//here initMap is directly given to the method
		do initMiniCityBuildingsFromMap(initMap);	
		
		return 0;
	}
    
    // diplay
	rgb color <- #blue ;
    aspect default {
    	draw self.shape color: color ;
    }
}

species BigCity parent:MiniCity {
	string name;
	float constellationDiameter;
	int constellationPopulation;
//	matrix availability_matrix_bigcity;
//	int availability_matrix_bigcity_size;
	list<MiniCity> list_minicities_constellation;
	// buildings...
	
	// diplay
    aspect default {
    	rgb color_const <- #lightblue ;
//    	rgb color_const_l <- #lightgreen ;
    	draw circle(constellationDiameter/2) color: color_const;
//    	draw circle(mean_diameter_city_l/2) color: color_const_s ;
//		draw self.shape color: #black ;
    }
    
    aspect pt {
		draw square(2#px) color: #red ;
		draw self.shape color: #red ;
    }
	
	list addMiniCity(float diameter_mc){
		list<geometry> allShapes <- [];
		loop bc over: BigCity {
			if bc.location inter circle(self.constellationDiameter, self.location) = bc.location {
				loop mc over: bc.list_minicities_constellation {
					add mc.shape to: allShapes;
				}
			}
		}
		
		float cell_size <- cellSizeBigCityGrid;
		point location_mc <- PickRandomLocationInCircle(self.constellationDiameter, self.location, cell_size, allShapes , circle(diameter_mc/2));
		return [location_mc!=nil, location_mc];
	}
}
