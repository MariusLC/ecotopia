/**
* Name: parameters 
* File with all the parameters of each section
* Author: Cedric Cornède, Damien Legros, Marius Le Chapelier, Petar Calic, Johann Alaya,  Damien Marillet 
*/


model parameters

global{
	//-------------------------------------------------------------Mes paramètres----------------------------------------------------------------------------------
	
	//----Paramètres généraux-----------
	int time_offset <- 6 parameter: "Start Time:" category: "Simulation"; //Time offset for when to start the day
	bool stop_simulation <- true parameter: "Stop Simulation: " category: "Simulation"; //Whether or not to stop the simulation after a certain number of days
	int stop_sim_day <- 1 parameter: "The number of days to stop the simulation after: " category: "Simulation"; //The number of days to stop the simulation after
	float step <- 1 #hour min: 0.0 parameter: "Pas de la simulation" category: "Simulation"; //pas de la simulation
	
	//----Paramètres de la grille--------
	float map_size <- 1240.0 #km parameter: "Longueur de la carte: " category: "Carte";//largeur de la carte
	float grid_size <- 20.0 parameter: "Taille de la grille: " category: "Carte"; //nombre de cases sur une ligne et une colonne
	float cell_size <- map_size / grid_size #km; //taille d'une case
	string grid_filename <- "grid.csv";
	
	//----Paramètres des mini-villes-----
	int nb_minicity_init <- 51 min: 21 max: 5000 parameter: "Nombre de Mini-villes: " category: "MiniCity"; // nombre de mini-villes de la simulation
	float nb_minicity_per_mini_city_sim <- nb_people*nb_minicity_init/nb_people_per_minicity_real; // le nombre de mini-city réelle auquel équivaut une mini-ville simulée.
	int nb_agent_per_minicity <- nb_agent_init div nb_minicity_init ; // le nombre d'agent par mini-ville
	// Variables pour la grille
	float mean_diameter_minicity <- 800#m; // diamètre moyen des mini-villes ;
	float mean_diameter_city_s <- 10000#m; // diamètre moyen des constellations de mini-villes ;
	float mean_diameter_city <- 30000#m; // diamètre moyen des constellations de mini-villes ;
	float mean_diameter_city_l <- 360 / 60 * 10 #km; // distance parcourue par un train en 10min
	float cellSizeMiniCityGrid <- 50#m; // taille d'une cellule dans la grille de la mini-ville
	float cellSizeBigCityGrid <- mean_diameter_minicity + 1000#m; // taille d'une cellule dans la grille de la constellation de mini-villes.
	
	//----Paramètres logements-----
	float percentWoodenHouse <- 0.9 min: 0.0 max: 1.0 parameter: "Percentage of wooden houses compared to modular houses" category:"Dwellings";
	float percentBusiness <- 0.2 min: 0.0 max: 1.0 parameter: "Percentage of businesses compared to other companies" category:"Dwellings";
	float percentFunZone <- 0.2 min: 0.0 max: 1.0 parameter: "Percentage of fun zones compared to other companies" category:"Dwellings";
	float percentPlasticFactory <- 0.2 min: 0.0 max: 1.0 parameter: "Percentage of factories compared to other companies" category:"Dwellings";
	float percentSchool <- 0.2 min: 0.0 max: 1.0  parameter: "Percentage of schools compared to other companies" category:"Dwellings";
	float percentHealthCenter <- 0.2 min: 0.0 max: 1.0 parameter: "Percentage of health centers compared to other companies" category:"Dwellings";
	float mean_floor_height <- 2.74 min: 2.5 max: 5.0 parameter: "Height of floors" category:"Dwellings"; // m
	int max_dwelling_cap <- 15 min: 7 max: 25 parameter: "Max capacity of residents per apartment/house" category:"Dwellings"; // Max capacity of residents per apartment/house
	int min_dwelling_cap <- 5 min: 1 max: 7 parameter: "Min capacity of residents per apartment/house" category:"Dwellings"; // Min capacity of residents per apartment/house
	int max_nb_floors_wooden_houses <- 4 min: 1 max: 10 parameter: "Max number of floors for wooden houses" category:"Dwellings";
	int min_nb_floors_wooden_houses <- 3 min: 1 max: 10 parameter: "Min number of floors for wooden houses" category:"Dwellings";
	int nb_solar <- 3  min: 0 max: 10 parameter: "Number of solar panels per wooden house" category:"Dwellings"; // Temporary value
	int nb_eolian <- 1 min: 0 max: 3 parameter: "Number of eolians per wooden house" category:"Dwellings"; // Temporary value
	
	// Parameters of companies (dwelling)
	int max_number_employees <- 100 min: 0 parameter: "Upper bound for the number of employees in a company" category: "Companies";
	int maxStudentsCapacity <- 100 min: 0 parameter: "Upper bound for the number of students in a school" category: "Companies"; //how many students a school can handle
	int maxPatientsHospitalCapacity <- 40 min: 5 max: 200 parameter: "Max capacity of patients in a hospital" category: "Companies";
	int fun_zone_max_capacity <- 100 parameter: "Max capacity of residents per fun_zone" category: "Companies"; 
	list<string> allBusinessTypes <- ["Restaurant", "Plumber", "Artist Workshop","Coffee", "Optician"]; // Feel free to add whatever business type you want
	list<string> activityList <- ["pétanque", "bowling", "tennis", "badminton", "danse", "basketball", "gymnastique", "musuculation", "yoga", "tai-chi", "escalade", "boxe", "judo", "karate", "football", "handball", "volleyball", "escrime", "échecs","cours de chant","jam session","théâtre","guitare","violon","salsa","tango","break-dance","piano"];
	
	// Parameters of emission (dwelling)
	float factory_ges_emission <- 1.3 min: 1.0 max: 1.7 parameter: "GES/kg of bioplastic produced by plastic factories" category:"Factory"; // GES/kg of bioplastic produced
	float plastic_co2_emissions <- 1.3*54.0 min:0.0 parameter: "quantity of CO2 emitted by plastic factories each timestep" category:"Factory"; 
	
	// Parameters of consumption (dwelling)
	float wood_consumption_per_floor <- 4250.0 parameter: "Wood consumption per floor for woodenHouse creation" category:"Dwellings";// kg
	float plastic_consumption_per_house <- 107.0 min: 0.0 parameter: "plastic_consumption_per_house" category:"Dwellings"; // t
	float cotton_consumption_for_plastic <- 4.0 min:1.0 parameter: "cotton_consumption_for_plastic" category:"Factory"; 
	
	// Parameters of production (dwelling)
	float biowaste_per_individual <- 1.786 min: 1.2 max: 2.0 parameter: "Biowaste production per individual per day in kg" category:"Dwellings"; // kg
	float plastic_production <- 54.0 min:0.0 parameter: "kgs of plastic produced by factories each timestep" category:"Factory"; 
	
	// Parameters of parks (dwelling)
	float TreesDensityInParks <- 0.5 min: 0.0 max: 1.0 parameter: "Density of trees in parks relative to forest density " category:"Parks" ; // arbitrary choice: Data from Central Park in NYC; we say that ecotopian parks are less dense than forest, mainly because the trees wont be sequoias and its not a wild environnement.
	int nb_of_buildings_per_park <- 4 min: 1 parameter: "Number of buildings per park" category:"Parks"; // a park is built every nb_of_buildings_per_park at miniCity init
	int nb_of_individual_per_park <- 50 ; // a park every 50 individuals
	
	// All floor areas
	float min_length_building <- sqrt(modular_house_floor_area/2); // modular houses are the smallest buildings and divided by two because of how method generateWidthAndLength works
	float wooden_house_floor_area <- 300.0;
	float modular_house_floor_area <- 250.0;
	float hospital_floor_area <- 100000.0*0.1 ; // the size reduction to 10% is because healthCenters were taking too much space
	float train_station_floor_area <- 170000.0*0.2; // the size reduction to 20% is because trainStations were taking too much space
	float business_floor_area <- 3000.0;
	float school_floor_area <- 3000.0;
	float plastic_factory_floor_area <- 10000.0;
	float fun_zone_floor_area <- 10000.0;
	float standard_park_area <- 10000.0; //placeholder, arbitrary choice 
	
	//buildings density indicators
	int currentNbOfResidentsAtHome ;
	int currentNbOfEmployeesAtWorkPlace;
	int nbOfPossibleResidents;
	int nbOfPossibleEmployees;
	
	float densityOfPeopleInTheirHouse ;
	float densityOfPeopleInTheirWorkPlace;
	
	//----Paramètres des individus------- 
	float  sum_demo <- 0.4453 + 0.0513 + 0.036;
	float  active_rate <- 0.4453/sum_demo min: 0.0 max: 1.0 parameter: "Taux d'actif: " category: "Individu"; 
	float  student_rate <- 0.0513/sum_demo min: 0.0 max: 1.0 parameter: "Taux d'étudiant: " category: "Individu"; 
	float  inactive_rate <- 0.036/sum_demo min: 0.0 max: 1.0 parameter: "Taux d'inactif: " category: "Individu"; 
	float  Age_0_18_rate <- 0.225 min: 0.0 max: 1.0 parameter: "Taux d'individu ayant moins de 18 ans: " category: "Individu";
	float  Age_18_64_rate <- 0.627 min: 0.0 max: 1.0 parameter: "Taux d'individu entre 18 et 64 ans: " category: "Individu";
	float  Age_65_rate <- 0.148 min: 0.0 max: 1.0 parameter: "Taux d'individu entre 18 et 64 ans: " category: "Individu";
	float  Women_rate <- 0.503 min: 0.0 max: 1.0 parameter: "Taux d'homme: " category: "Individu";
	float  Men_rate <- 0.497 min: 0.0 max: 1.0 parameter: "Taux de femme: " category: "Individu";
	// data arbitraire :
	float proba_of_getting_sick <- (1/365) min: 0.0 max: 1.0 parameter: "Probabilité de tomber malade chaque jour" category:"Individu"; 
	int nb_people <- 39512223 parameter: "Population Californie: " category: "Individu"; //nombre d’habitants
	int nb_people_per_minicity_real <- 10000 parameter: "Habitants par Mini-ville: " category: "Individu"; // nombre d’habitants par mini-villes
	// probas activity changing infos
    float proba_become_active_after_school <- 0.5 parameter: "proba_become_active_after_school";
    // une chance par 4 ans
    float proba_become_active_when_etudiant <- (1/1460) parameter: "proba_become_active_after_school";
    float proba_become_inactive_when_etudiant <- (1/1460) parameter: "proba_become_active_after_school";
    // data arbitrarie
    float proba_become_student <- 0.8 parameter: "proba_become_student";
    // une chance par 4 mois
    float proba_become_active <- (1/120) parameter: "proba_become_active";
    float proba_become_inactive <- (1/120) parameter: "proba_become_inactive";
//    float proba_become_active <- (1/2) parameter: "proba_become_active";
//    float proba_become_inactive <- (1/2) parameter: "proba_become_inactive";
    // proba funZone
    float proba_goFunZone <- 0.8 parameter: "proba_goFunZone";
    float proba_fun_at_FunZone <- 0.5 parameter: "proba_fun_at_FunZone";
	
	float proba_travel <- 0.2;	
	
	//----Paramètres de la consommation--
	float full_stomack_time <- 12#h parameter: "Temps après lequel un repas est consommé: " category: "Individu";// définit tous les combien de temps les doivent manger
	float meal_size <- 0.3 parameter: "Taille d'un repas en gramme: " category: "Individu"; // définit la quantité de nourriture par repas (en grammes)
	float meat_ratio <- 1/3 parameter: "Ratio de viande d'un repas: " category: "Individu";// définit la quantité de viande par repas
	float crop_ratio <- 2/3 parameter: "Ratio de légumes d'un repas: " category: "Individu"; // définit la quantité de plantes par repas
	float meat_consomation_agent <- nb_people_per_agent*meal_size*meat_ratio; // viande consommé par un agent par repas
    float crop_consomation_agent <- nb_people_per_agent*meal_size*crop_ratio; // plantes consommé par un agent par repas
//	float meat_consommation_per_minicity <- meat_consomation_agent*nb_agent_per_minicity; // consommation de viande d'une mini-city par repas
//	float crop_consommation_per_minicity <- crop_consomation_agent*nb_agent_per_minicity; // consommation de plantes d'une mini-city par repas
	float meal_consomation_agent <- nb_people_per_agent*meal_size; // nourriture consommé par un agent par repas
	float meal_consomation_agent_per_day <- meal_consomation_agent*(1#day/full_stomack_time);
	float meal_quantity_stock <- meal_consomation_agent_per_day*(1#month/1#day); // consommation agent * nb_repas par jour * nb jours par mois (donc le stock pour un mois de consommation)
	map<string, float> meal <- ["meat"::meal_consomation_agent*meat_ratio, "crop"::meal_consomation_agent*crop_ratio];
	list<float> total_consommation <- [0.0, 0.0]; // consommation globale de viande et légumes
	
	// MATRICE EFM
	float meat_stock <- 0.0;
	float meat_prod <- 0.0;
	float meat_conso <- 0.0;
	float meat_water_conso <- 0.0;
    float meat_ghg_conso <- 0.0;
    float meat_waste_conso <- 0.0;
    
    float crop_stock <- 0.0;
    float crop_prod <- 0.0;
    float crop_conso <- 0.0;
	float crop_water_conso <- 0.0;
	float crop_ghg_conso <- 0.0;
	float crop_waste_conso <- 0.0;
	
	float cotton_stock <- 0.0;
	float cotton_prod <- 0.0;
	float cotton_conso <- 0.0;
	float cotton_water_conso <- 0.0;
    float cotton_ghg_conso <- 0.0;
    float cotton_waste_conso <- 0.0;
    
    float plastic_stock <- 0.0;
    float plastic_prod <- 0.0;
    float plastic_conso <- 0.0;
	float plastic_water_conso <- 0.0;
    float plastic_ghg_emissions <- 0.0;
    float plastic_waste_conso <- 0.0;
    
    float wood_stock <- 0.0;
    float wood_prod <- 0.0;
    float wood_conso <- 0.0;
	float wood_water_conso <- 0.0;
    float wood_ghg_conso <- 0.0;
    float wood_waste_conso <- 0.0;
	
	float tot_water_conso <- 0.0;
	float tot_ghg_conso <- 0.0;
	float tot_waste_conso <- 0.0;
		
	float energy_ghg_emissions <- 0.0;
	float forest_ghg_absorption <- 0.0;
	float agriculture_ghg_emissions <- 0.0;
	
	float biomass_waste_building <- 0.0;
	float biomass_agriculture_conso <- 0.0;
	float biomass_agriculture <- 0.0;
	//
	
	//----Paramètres des agents--
	int nb_agent_init <- 2500 min: 21 max: 50000 parameter: "Nombre d'agents: " category: "Individu"; // nombre d’agents de la simulation
	float nb_people_per_agent <- nb_people / nb_agent_init; // le nombre d'individu auquel correspond un agent
	
	//----Paramètres des forêts----
	int GforestSurface <- 133000 parameter: "Surface totale de la forêt en km2: " category: "Forêts"; //Surface totale de la forêt en km2
	int Gtreeperkm2 <- 500 parameter: "Nombre d'arbres par centaine par km2: " category: "Forêts"; //Nombre d'arbres par centaine par km2
	int Gnb_Trees <- GforestSurface * Gtreeperkm2; //Nombre total d arbre dans les forets de a Californie par paquets de 100
	int Gnb_TreesInit <- GforestSurface * Gtreeperkm2; // Nombre total inital
	int GnbTreesMaxFactor <- 2 parameter: "Facteur du nombre maximum d'arbres: " category: "Forêts"; // "Maximum possible number of trees Factor"
	int GnbTreesMax <- Gnb_Trees * GnbTreesMaxFactor; //Nombre maximum d'arbre possible
	float GtreeConsoGhg <- 0.3 parameter: "kg de GES consommé par un arbre par mois: " category: "Forêts"; //kg de GES consommé par un arbre par mois
	float GcoefNaturalGrowth <- 1.0001 parameter: "Coef de reproduction des arbres: " category: "Forêts"; // "Coef of Natural forest renewal"
	float GcoefNaturalDeath <- 0.99999 parameter: "Coef de mort naturel des arbres: " category: "Forêts"; // "Coef of Natural forest death"
	float Gnb_TreesMinFactor <- 0.5 parameter: "bornne minimum d arbres permis: " category: "Forêts"; // "Minimum alowed tree proportion"
   	int Gnb_TreesMin <- round(Gnb_Trees * Gnb_TreesMinFactor); // Borne Minimum d'arbre en Californie
    float GtreeMass <- 0.5 parameter: "Masse d'un arbre en tonnes: " category:"Forêts"; //masse d'un arbre en tonnes
    float GproportionOk <- 0.75 parameter: "proportion acceptable de la foret apres deforestation: " category: "Forêts"; //"Borne inf de arbre acceptable"
    float GwaistPerTreeFactor <- 1.0 parameter: "Quantité de dechet produit par la coupure d'un arbre en kg: " category: "Forêts"; // "Quantité de dechet produit par la coupure d'un arbre en Kilograme
    float GtreeConsoWater <- 6000.0 parameter: "Consommation d'eau d'un arbre par mois en litre: " category: "Forêts"; // Consomation d'eau d'un arbre par mois en litre
    
    //----Paramètres de l'écosystème----
    float biomass_waste <- 0.0 min: 0.0 parameter: "Initialisation de la quantité de biomasse/engrais: " category: "Ecosystème";
    float biomass_waste_consumed <- 0.0 min: 0.0 parameter: "Initialisation de la quantité de biomasse/engrais consommée " category: "Ecosystème";
    float biomass_waste_produced <- 0.0 min: 0.0 parameter: "Initialisation de la quantité de biomasse/engrais produite " category: "Ecosystème";
    float greenhouse_emissions <- 500000000.0 min: 0.0 parameter: "Initialisation de la quantité de gas à effet de serre: " category: "Ecosystème";
    float greenhouse_emissions_consumed <- 0.0 min: 0.0 parameter: "Initialisation de la quantité de gas à effet de serre consommée " category: "Ecosystème";
    float greenhouse_emissions_produced <- 0.0 min: 0.0 parameter: "Initialisation de la quantité de gas à effet de serre produite " category: "Ecosystème";
    float animal_consommation_crop <- 4 #kg parameter: "Quantité de plantes pour produire un kg de viande: " category: "Ecosystème"; //définit la quantité de plantes consommé pour la production de 1kg de viande
    float animal_consommation_water <- 6000 #l parameter: "Quantité d'eau pour produire un kg de viande: " category: "Ecosystème"; //définit la quantité d'eau consommé pour la production de 1kg de viande
    float water_consommation <- 0 #l parameter: "Initialisation de la consommation d'eau: " category: "Ecosystème"; //consommation de l'eau 
    float crop_consommation_water <-  1211 #l parameter: "Quantité d'eau pour produire un kg de plantes: " category: "Ecosystème"; //définit la quantité d'eau consommé pour la production de 1kg de plantes
    float cotton_consommation_water <-  5260 #l parameter: "Quantité d'eau pour produire un kg de coton: " category: "Ecosystème"; //définit la quantité d'eau consommé pour la production de 1kg de coton
    float animal_production_ghg <- 70 #kg parameter: "Quantité de GES pour produire un kg de viande: " category: "Ecosystème"; //définit la quantité de GHG produit pour la production de 1kg de viande
	float crop_production_ghg <- 1.6 #kg parameter: "Quantité de GES pour produire un kg de plantes: " category: "Ecosystème"; //définit la quantité de GHG produit pour la production de 1kg de plantes
	float cotton_production_ghg <- 2 #kg parameter: "Quantité de GES pour produire un kg de coton: " category: "Ecosystème"; //définit la quantité de GHG produit pour la production de 1kg de coton
	float animal_production_waste <- 10 #kg parameter: "Quantité de déchets pour produire un kg de viande: " category: "Ecosystème"; //définit la quantité de dechet produit pour la production de 1kg de viande
	float crop_conso_waste <- 5 #kg parameter: "Quantité de déchets pour produire un kg de plantes: " category: "Ecosystème"; //définit la quantité de dechet produit pour la production de 1kg de plantes
	float cotton_conso_waste <- 5 #kg parameter: "Quantité de déchets pour produire un kg de coton: " category: "Ecosystème"; //définit la quantité de dechet produit pour la production de 1kg de coton
	
    //----Paramètres de la météo----
	float wind_noise <- 1.0 min: 0.0 parameter: "Bruit dans la génération du vent" category: "Meteo";//bruit dans le choix du vent
	float temperature_noise <- 2.0 min: 0.0 parameter: "Bruit dans la génération de la temperature" category: "Meteo"; //bruit dans le choix de la temperature
	
	//from : https://en.wikipedia.org/wiki/Climate_of_California#cite_note-24
	float temperature_january <- 8.9 min: 0.0 parameter: "Temperature moyenne en janvier: " category: "Meteo";
	float temperature_february <- 11.3 min: 0.0 parameter: "Temperature moyenne en fevrier: " category: "Meteo";
	float temperature_march <- 14.1 min: 0.0 parameter: "Temperature moyenne en mars: " category: "Meteo";
	float temperature_april <- 16.8 min: 0.0 parameter: "Temperature moyenne en avril: " category: "Meteo";
	float temperature_may <- 21.2 min: 0.0 parameter: "Temperature moyenne en mai: " category: "Meteo";
	float temperature_june <- 25.3 min: 0.0 parameter: "Temperature moyenne en juin: " category: "Meteo";
	float temperature_july <- 28.6 min: 0.0 parameter: "Temperature moyenne en juillet: " category: "Meteo";
	float temperature_august <- 27.9 min: 0.0 parameter: "Temperature moyenne en août: " category: "Meteo";
	float temperature_september <- 25.1 min: 0.0 parameter: "Temperature moyenne en septembre: " category: "Meteo";
	float temperature_october <- 19.3 min: 0.0 parameter: "Temperature moyenne en octobre: " category: "Meteo";
	float temperature_november <- 12.8 min: 0.0 parameter: "Temperature moyenne en novembre: " category: "Meteo";
	float temperature_december <- 8.6 min: 0.0 parameter: "Temperature moyenne en decembre: " category: "Meteo";
	
	list temperature_per_month <- [
		temperature_january, 
		temperature_february, 
		temperature_march, 
		temperature_april, 
		temperature_may, 
		temperature_june, 
		temperature_july, 
		temperature_august, 
		temperature_september,
		temperature_october,
		temperature_november, 
		temperature_december]; //in celsius
	
	float rain_january <- 7.7 min: 0.0 parameter: "Jours de pluie par mois en janvier: " category: "Meteo";
	float rain_february <- 8.5 min: 0.0 parameter: "Jours de pluie par mois en fevrier: " category: "Meteo";
	float rain_march <- 7.2 min: 0.0 parameter: "Jours de pluie par mois en mars: " category: "Meteo";
	float rain_april <- 4.5 min: 0.0 parameter: "Jours de pluie par mois en avril: " category: "Meteo";
	float rain_may <- 2.7 min: 0.0 parameter: "Jours de pluie par mois en mai: " category: "Meteo";
	float rain_june <- 0.7 min: 0.0 parameter: "Jours de pluie par mois en juin: " category: "Meteo";
	float rain_july <- 0.3 min: 0.0 parameter: "Jours de pluie par mois en juillet: " category: "Meteo";
	float rain_august <- 0.1 min: 0.0 parameter: "Jours de pluie par mois en août: " category: "Meteo";
	float rain_september <- 0.6 min: 0.0 parameter: "Jours de pluie par mois en septembre: " category: "Meteo";
	float rain_october <- 2.2 min: 0.0 parameter: "Jours de pluie par mois en octobre: " category: "Meteo";
	float rain_november <- 2.2 min: 0.0 parameter: "Jours de pluie par mois en novembre: " category: "Meteo";
	float rain_december <- 7.3 min: 0.0 parameter: "Jours de pluie par mois en decembre: " category: "Meteo";
	
	list rain_per_month <- [
		rain_january, 
		rain_february, 
		rain_march, 
		rain_april, 
		rain_may, 
		rain_june, 
		rain_july, 
		rain_august, 
		rain_september,
		rain_october,
		rain_november, 
		rain_december]; //in days
	
	float sunshine_january <- 46.0 min: 0.0 parameter: "Pourcentage de soleil par mois en janvier: " category: "Meteo";
	float sunshine_february <- 65.0 min: 0.0 parameter: "Pourcentage de soleil par mois en fevrier: " category: "Meteo";
	float sunshine_march <- 77.0 min: 0.0 parameter: "Pourcentage de soleil par mois en mars: " category: "Meteo";
	float sunshine_april <- 85.0 min: 0.0 parameter: "Pourcentage de soleil par mois en avril: " category: "Meteo";
	float sunshine_may <- 91.0 min: 0.0 parameter: "Pourcentage de soleil par mois en mai: " category: "Meteo";
	float sunshine_june <- 94.0 min: 0.0 parameter: "Pourcentage de soleil par mois en juin: " category: "Meteo";
	float sunshine_july <- 96.0 min: 0.0 parameter: "Pourcentage de soleil par mois en juillet: " category: "Meteo";
	float sunshine_august <- 95.0 min: 0.0 parameter: "Pourcentage de soleil par mois en août: " category: "Meteo";
	float sunshine_september <- 93.0 min: 0.0 parameter: "Pourcentage de soleil par mois en septembre: " category: "Meteo";
	float sunshine_october <- 87.0 min: 0.0 parameter: "Pourcentage de soleil par mois en octobre: " category: "Meteo";
	float sunshine_november <- 62.0 min: 0.0 parameter: "Pourcentage de soleil par mois en novembre: " category: "Meteo";
	float sunshine_december <- 42.0 min: 0.0 parameter: "Pourcentage de soleil par mois en decembre: " category: "Meteo";

	list sunshine_per_month <- [
		sunshine_january, 
		sunshine_february, 
		sunshine_march, 
		sunshine_april,
		sunshine_may,
		sunshine_june,
		sunshine_july, 
		sunshine_august,
		sunshine_september,
		sunshine_october,
		sunshine_november,
		sunshine_december]; //in pourcentage
		
	//from :https://www.weather2visit.com/north-america/united-states/fresno-ca.htm
	
	float wind_january <- 9.0 min: 0.0 parameter: "Vitesse de vent en km/h moyenne par mois en janvier: " category: "Meteo";
	float wind_february <- 10.0 min: 0.0 parameter: "Vitesse de vent en km/h moyenne par mois en fevrier: " category: "Meteo";
	float wind_march <- 12.0 min: 0.0 parameter: "Vitesse de vent en km/h moyenne par mois en mars: " category: "Meteo";
	float wind_april <- 13.0 min: 0.0 parameter: "Vitesse de vent en km/h moyenne par mois en avril: " category: "Meteo";
	float wind_may <- 14.0 min: 0.0 parameter: "Vitesse de vent en km/h moyenne par mois en mai: " category: "Meteo";
	float wind_june <- 15.0 min: 0.0 parameter: "Vitesse de vent en km/h moyenne par mois en juin: " category: "Meteo";
	float wind_july <- 13.0 min: 0.0 parameter: "Vitesse de vent en km/h moyenne par mois en juillet: " category: "Meteo";
	float wind_august <- 12.0 min: 0.0 parameter: "Vitesse de vent en km/h moyenne par mois en août: " category: "Meteo";
	float wind_september <- 11.0 min: 0.0 parameter: "Vitesse de vent en km/h moyenne par mois en septembre: " category: "Meteo";
	float wind_october <- 10.0 min: 0.0 parameter: "Vitesse de vent en km/h moyenne par mois en octobre: " category: "Meteo";
	float wind_november <- 9.0 min: 0.0 parameter: "Vitesse de vent en km/h moyenne par mois en novembre: " category: "Meteo";
	float wind_december <- 9.0 min: 0.0 parameter: "Vitesse de vent en km/h moyenne par mois en decembre: " category: "Meteo";
	
	list wind_per_month <- [
		wind_january,
		wind_february,
		wind_march,
		wind_april,
		wind_may,
		wind_june,
		wind_july,
		wind_august,
		wind_september,
		wind_october,
		wind_november,
		wind_december]; //in km/h

	//----Paramètres de l'énergie----
    /*  10% nucléaire, 50% solaire, 20% géothermique, 5% hydro-électrique, 15% éolien. */ 
    map<string, float> GmixEner <- ["nucleaire"::0.1, "solaire"::0.5, "geothermique"::0.2, "hydro"::0.05, "eolien"::0.15] parameter: "Mix énergétique" category: "Energie" ;

	/* Source: Smil, Vaclav, General Energetics : Energy in the Biosphere and Civilization, Wiley, 1991 (ISBN 978-0-471-62905-4), p. 240. */
	float Gsolar_radiation <- 340/1000 parameter: "Energie reçue par le soleil à la surface de la Terre en kW/m2" category: "Energie"; //

    float Gloss_ratio <- 0.07 parameter: "Perte d'énergie dans le transport d'électricité" category: "Energie";

    float Gideal_temp <- 25.0 parameter: "Température d'intérieur idéale" category: "Energie"; // celcius degrees

	float Gflow <- 1.0 parameter: "Débit d'eau dans les barrages" category: "Energie"; // TODO missing data

    float Gco2_wh_eolian <- 0.0 parameter: "Masse en g de CO2/kWh émis par l'éolien" category: "Energie";
    float Gmethane_wh_eolian <- 0.0 parameter: "Masse en g de méthane/kWh émis par l'éolien" category: "Energie";
    
    float Gco2_wh_solar <- 0.0 parameter: "Masse en g de CO2/kWh émis par le solaire" category: "Energie";
    float Gmethane_wh_solar <- 0.0 parameter: "Masse en g de méthane/kWh émis par le solaire" category: "Energie";
    /*Greenhouse Gas Emissions from Geothermal Power Production Thráinn Fridriksson, Almudena Mateos Merino, A. Yasemin Orucu, Pierre Audinet */
    float Gco2_wh_geotherm <- 122.0 parameter: "Masse en g de CO2/kWh émis par le géothermique" category: "Energie"; // g/kWh
    float Gmethane_wh_geotherm <- 18.3 parameter: "Masse en g de méthane/kWh émis par le géothermique" category: "Energie"; // g/kWh

	/*Valuing the greenhouse gas emissions from nuclear power: A critical survey Author links open overlay panelBenjamin K.Sovacoo */
	/* Lifecycle emission */
    float Gco2_wh_nuclear <- 66.0 parameter: "Masse en g de CO2/kWh émis par le nucléaire" category: "Energie"; // g/kWh
    float Gmethane_wh_nuclear <- 0.0 parameter: "Masse en g de méthane/kWh émis par le nucléaire" category: "Energie"; // g/kWh

    float Gco2_wh_dam <- 0.0 parameter: "Masse en g de CO2/kWh émis par les barrages" category: "Energie"; // g/kWh
    float Gmethane_wh_dam <- 0.0 parameter: "Masse en g de méthane/kWh émis par les barrages" category: "Energie"; // g/kWh

	/* source: L’UTILISATION DE L’EAU DANS LES CENTRALES NUCLÉAIRES -- edf 
	 * https://www.edf.fr/sites/default/files/contrib/groupe-edf/producteur-industriel/notes-d-informations/note_gestion_de_leau_2014.pdf
	 * */
	float Gwater_per_kWh_nuclear <- 100 #l parameter: "Quantité d'eau en litres consommée par kWh généré par le nucléaire" category: "Energie";

    float Gconst_nuclear <- 2054794.52055 parameter: "Production constante en kWh/h d'une centrale nucléaire" category: "Energie"; /**production en kw/heure d'une centrale nucléaire */ 
    float Gconst_geotherm <- 634000.0 parameter: "Production constante en kWh/h d'une centrale géothermique" category: "Energie";//kWh/h // 634 MW or 1W = 1J/s
    /* 634 MW = 634MJ/s
     * 634*60 MJ/min
     * 634*60*60MJ/h = 2282400MJ/h
     * = 634000kWh/h
     * = 634MWh/h
     * 1kWh = 3.6 MJ
     * Ok donc en fait 1kW = 1 kWh/h
     */
     
	 /*** paramétres reliés a la consommation  **/
     /*
      * The volumetric heat capacity of a material is the heat capacity of a sample of the substance divided by the volume of the sample
      * The SI unit of volumetric heat capacity is joule per kelvin per cubic meter
      * air : 0.00121 J.cm-3.K-1
      * 1 Wh = 3600 J => 1 J = 1/3600 Wh
      * 1 m3 = 1000000 cm3
      * - wikipedia
      */
     // energyRequired_m2_degree
     float GwhRequired_m3_1degree <- ((1000000 * 0.00121)/3600)/1000 parameter: "Energie en kWh nécessaire pour chauffer d'un degré un mètre cube" category: "Energie";
     
     float Gelectric_cons <- 0.0 max:100.0 min:0.0 parameter: "Consommation électrique des électro-ménagers d'une maison en kWh" category: "Energie"; // consommation des électro-menagers
     

	  /* https://www.physics.uci.edu/~silverma/actions/HouseholdEnergy.html?fbclid=IwAR3Xqmj7UUT5JBDfh29qb2-TPo8RI9M9SHwJvZ8Sllb05EAbLlka6J3dNjQ 
	   * 6000kWh / household / year for 3 residents
	   * => 2000 kWh/year/pers
	   * => (2000/365)/24 kWh/h/pers
	   ***/
	   
      float Gcons_1_pers <- (2000/365)/24 parameter: "Consommation d'une personne en kWh/h" category: "Energie"; /**kw/h */  /*   */
      float Glighting <-0.1039954338 parameter: "Consommation en kWh/h de l'éclairage dans un bâtiment" category: "Energie";  /**kw/h   */ /*company:20.28 kwh */ /* Forestcamp: */  /* hospital :589.72*  /* PlasticProd : 16,28*/
      float Gconsomachine_usine <- 0.2283105023 parameter: "Consommation en kWh/h d'une usine" category: "Energie";// TODO missing source// kw/h  /*2000kw par année */ /*company:  */ /* Forestcamp :  */  /*hospital: 700 */ /*PlasticProd : */  
      float Gconsomachine_hopital <- 700/(365*24) parameter: "Consommation en kWh/h des appareils d'un hôpital" category: "Energie"; // TODO missing source // kw/h
      float Gconsomachine_plastic <- 2000/(365*24) parameter: "Consommation en kWh/h des appareils d'une usine de plastique" category: "Energie"; // TODO missing data // kw/h
      float Gmin_conso <- 0.010 parameter: "Consommation de base des bâtiments en kWh/h" category: "Energie";// TODO missing data /*kw/h */ /*company:  */ /* Forestcamp :  */  /*hospital: */ /*PlasticProd : */  
      
		float Gsolar_yield <- 0.18 min: 0.0 max:1.0 parameter: "Rendement d'un panneau solaire" category: "Energie"; // rendement
		float Gheight_waterfall <- 10 #m parameter: "Hauteur de chute d'eau dans les barrages en mètres" category: "Energie";
		
		/* Etude de modules photovoltaïques 
		après 20 ans de fonctionnement
		Association Hespul
		Auteur (s) : G. RAZONGLES */
		float Glife_duration_solar <- 40 * 365 * 24 #h parameter: "Durée de vie d'un panneau solaire en heures";
		
	
	//transport
	float TrainSPEED <- 360 #km / #h ;
	float walkSpeed <- 5 #km / #h ;
	float TrainEnergy<- 45.0; //#Wh / #siege /#km;  energy consumed by a train per passenger km
	
	float Train_CO2_Emission <- 30 #gram; //Train CO2 emission per passenger km 
	float TrainCo2_emitted;
	float TrainEnergy_used ;
	
	float truck_CO2_Emission <- 10 #gram; //Camion CO2 emission per passenger km
	int max_cap<-10;//capacité maximum d'une arrete à un tick donné (à ajuster)
	float truckCo2_emitted;
	
	float STOCK_MIN <- 500.0; //TODO: Update
	list<float> STOCKS_MIN <- [wood_consumption_per_floor*max_nb_floors_wooden_houses,plastic_consumption_per_house*cotton_consumption_for_plastic,plastic_consumption_per_house, meal_consomation_agent_per_day*(1#week/1#day)*(nb_agent_init/nb_minicity_init)*meat_ratio,meal_consomation_agent_per_day*(1#week/1#day)*(nb_agent_init/nb_minicity_init)*crop_ratio];
	float maxPlasticStocks <- 5000.0; //TODO: Update 
	
	
}
