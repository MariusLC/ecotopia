/**
* Name: individual
* Based on the internal empty template. 
* Author: Cedric Cornede and Damien Marillet
* Tags: 
*/
model individual

import "../models/california_environment.gaml"
import "mini_city.gaml"
import "parameters.gaml"
import "../dwelling/building.gaml"
import "../transport/reseauTrain.gaml"

global {
	
    int pop_mc <- nb_agent_per_minicity;
    
    int week_day <- -1;
    
    // On initalise un maximum et un minimum pour le début et la fin du travail 
    int min_work_start <- 4;
	int max_work_start <- 10;
	int min_work_end <- 21; 
	int max_work_end <- 22; 
	
	float min_speed <- 4 #km / #h;
	float max_speed <- 6 #km / #h; 
	

    list create_individu(int nb_people_to_create, MiniCity mc){
    
    	list<individu> pop_created <- [];
    	list pop_per_ages <- [Age_0_18_rate,Age_18_64_rate,Age_65_rate] collect (each*nb_people_to_create);
		list activity_per_ages <- ["Scolarise", "Inactif", "Inactif"];
		list activities <- ["Scolarise", "Actif", "Inactif","Etudiant"];
		list age_per_ages <- [[0,18], [19, 64], [65, 100]];
		
//		write "nb_people_to_create = "+nb_people_to_create;
//		write "pop_per_ages = "+pop_per_ages;
    	int nb_Men_init <- nb_people_to_create*Men_rate;
    	int nb_Women_init <- nb_people_to_create*Women_rate;
    	int nb_people_worked_init <- pop_per_ages[1]*active_rate;
	    int nb_student_init <- pop_per_ages[1]*student_rate;
	    int nb_inactive_init <- pop_per_ages[1]*inactive_rate;
//	    write "nb_people_worked_init = "+nb_people_worked_init;
//	    write "nb_student_init = "+nb_student_init;
//	    write "nb_inactive_init = "+nb_inactive_init;

		loop ages_id from:0 to:length(pop_per_ages)-1 {
			create individu number: pop_per_ages[ages_id] returns:pop{
				age <- rnd (int(age_per_ages[ages_id][0]),int(age_per_ages[ages_id][1]));
	        	activity <- activity_per_ages[ages_id]; 
	            objective <- "resting";    
	            speed <- min_speed + rnd (max_speed - min_speed) ;  
	            minicity <- mc;
	            gender <- "Femme"; 
	            
	            // dwelling
	            loop times:3{
					add item:one_of (activityList where not (each in activityPref)) to:activityPref;
					/* On sélectionne 3 activités préférées au hasard */
				}
			}
			
			if ages_id = 1 and pop != nil { // 18-64
				// set actif, inactif ou etudiant
				list indexs <- shuffle(range(length(pop)-1)); // supposed to be len(pop_created)
//				write "length(pop) = " + length(pop);
//				write "nb_people_worked_init = " + nb_people_worked_init;
				loop ind_id from:0 to:nb_people_worked_init {
//					write "ind_id="+ind_id;
//					write indexs[ind_id];
					pop[indexs[ind_id]].activity <- "Actif";
				}
				loop ind_id from:nb_people_worked_init to:nb_people_worked_init+nb_student_init {
					pop[indexs[ind_id]].activity <- "Etudiant";
				}
			}
			
			add all: pop to: pop_created; // to be tested;
		}
		// add agent if there is less agent generate
		int i <- 0;
		int ages_id;
		string activity_id;
//		write "length(pop_created) = " + length(pop_created);
//		write "nb_people_to_create = " + nb_people_to_create;
		list diff <- (pop_per_ages collect [pop_per_ages index_of each, each-int(each)] sort -each[1]);
//		write "diff = "+ diff;
		loop while: (length(pop_created) < nb_people_to_create){ // ? -1
//			write "i = "+i;
			ages_id <- diff[i][0];
			create individu returns:pop {
				age <- rnd (int(age_per_ages[ages_id][0]),int(age_per_ages[ages_id][1]));
	        	activity <- activity_per_ages[ages_id];
	            objective <- "resting";    
	            speed <- min_speed + rnd (max_speed - min_speed) ;  
	            minicity <- mc;
	            gender <- "Femme";
	            
	            if (ages_id = 1){
		            float var<- rnd(0,1);
					if (var < active_rate){
					activity  <- "Actif";
					}
					else if (var < active_rate + student_rate){
					activity  <- "Etudiant";
					}
					else{
					activity  <-  "Inactif";
					}
				}
	            
	            // dwelling
	            loop times:3{
					add item:one_of (activityList where not (each in activityPref)) to:activityPref;
					/* On sélectionne 3 activités préférées au hasard */
				}
	    	}
	    	add pop[0] to: pop_created; 
	    	i <- (i = 2) ? 0 : i+1;
		}
		
		
		// On met des caractéristiques aux individus parmis ceux déjà crée
		list indexs <- shuffle(range(length(pop_created)-1)); // supposed to be len(pop_created)
//		write "nb_people_to_create = " + (nb_people_to_create-1);
//		write "pop_created = " + pop_created;
//		write "nb_Men_init = " + nb_Men_init;
		loop ind_id from:0 to:nb_Men_init { // setting men
//			write "ind_id="+ind_id;
//			write indexs[ind_id];
			pop_created[indexs[ind_id]].gender <- "Homme";
		}
		
//		write "length(pop_created) = "+length(pop_created);
		return pop_created;
	}
}

species individu skills:[moving] control: fsm {
	
	// general infos
	// int id <- individuIDHandler();
	float speed <- (2 + rnd(3)) #km/#h;
    int age;
    string gender;
//    bool employed <- false;
    individu spouse <- nil;
    list<individu> parents <- nil;
    
    // activity infos
    string activity;
    point target;
    string targetActivity <- "";
    list<string> activityPref <- [];
    
    // locations infos
    funZone atFunZone ;//<- nil; // TEMPORAIRE, IL DOIT Y AVOIR DES MEILLEURES SOLUTIONS
    company workPlace ;//<- nil;
    housing house ;//<- nil;
    healthCenter myHospital <- nil;
    bool onTrain <- false;
//    // probas activity changing infos
//    float proba_become_student <- 0.8;
//    float proba_become_active <- 0.6;
//    float proba_become_active_after_school <- 0.8;
//    float proba_become_inactive <- 0.3;
    
    // schedule infos
    int time_to_work ;
	int time_to_sleep <- 23;
	int time_to_fun <- 9;
	string objective;
    float working_time <- 0.0;
    MiniCity minicity;
    int travel_day <- -2;
    int Time_to_travel <- 9 ;
    int TravelingHours <- 6 ;
    city_node Next_station;
    int path_current_edge <- 0;
    bool take_train;
    city_node final_train_dest;
    point final_dest;
    path path_to_follow;
	
	reflex goToWork when: objective = "working" and not (location overlaps workPlace.shape) and take_train = false{
		list<company> comp_mc <- (agents of_generic_species company);
		if(week_day = travel_day){
				
				company comp <- one_of(comp_mc);
				//write("liste of companies "+comp_mc);
				//write("company "+comp);
			    loop while:(comp.minicity = minicity){
     	           remove item:comp from:comp_mc;
     	           comp <- one_of(comp_mc);
        		}
        		target <- any_location_in(comp);	
        		//write("je travaille ailleur");
		}
		else{
			target <- any_location_in (workPlace);	
		}
		
		//write("L'agent "+self.name+" va travailler :)");	
	}
	
	reflex week when: hour_day = 0 {
		week_day <- week_day + 1;
		week_day <- week_day mod 7;
	}

	reflex goHome when: objective = "resting" {
		assert house != nil; 
		assert house.shape != nil; 
		target <- any_location_in (house);
	}
	
    reflex goToSchool when: objective = "study" and not (location overlaps workPlace.shape) and take_train = false{
		list<school> school_mc <- (agents of_generic_species school);
		if(week_day = travel_day){
				
				company _school <- one_of(school_mc);
				//write("liste of companies "+comp_mc);
				//write("company "+comp);
			    loop while:(_school.minicity = minicity){
     	           remove item:_school from:school_mc;
     	           _school <- one_of(school_mc);
        		}
        		target <- any_location_in(_school);	
		}
		else{
			target <- any_location_in (workPlace);
		}
//		write("L'agent "+self.name+" va etudier :)");	
	}

	
	reflex goHaveFun when: targetActivity="" and atFunZone=nil and objective = "fun" and take_train = false{
       	//write("going to have fun " + week_day +" "+ travel_day);
        targetActivity <- one_of (activityPref);
        int tries <- 0;
        bool found <- false;
        list<funZone> closed <- [];
        loop while:not found and tries <10 and length(closed) < length(funZone.population) {
            float min_dist <- #infinity;
            funZone f;
            if( week_day = travel_day){
            	f <- one_of(funZone.population - closed);
            	if(f.minicity = minicity){
     	           add item:f to:closed;
        		}	
        		else{
        			//write("found a funzone outside my minicity");
        		}
	
            }
            else{
            loop fzone over: (funZone.population - closed) {
                   float dist <- self.location distance_to fzone ;

	                   	if min_dist > dist {
	                           min_dist <- dist;
	                          f <- fzone;
	                	}
            }
            
            }
            if(not (f in closed) and f.askToEnter(targetActivity)){
                found <- true;
                atFunZone <- f;
                target <- any_location_in(f);
               // write("L'agent "+self.name+" va s'amuser :)");
            }
            else {
                add item:f to:closed;
            }
            tries<-tries+1;
        }
        if not found{
            targetActivity <- "";
            objective <- "resting"; //on change l'objectif pour éviter qu'il ne rappelle goHaveFun en boucle
        }
    }
	
	reflex gettingSick when: every(#day) and self.state != "state_malade" and flip(proba_of_getting_sick) and take_train = false
	{
		//write "the individual "+self+" has gotten sick ...";
		// go to hospital
		building tmp <- findNewAvailableBuildingOfSpecificType([],"healthCenter",self);
		if tmp!= nil{
			myHospital <- healthCenter(tmp); // will add the individual to the hospital if possible
			target <- any_location_in(myHospital); // the agent must go to the hospital 
			//write " ... and is going to the hospital "+myHospital;
		}
			
        //assert myHospital != nil; // the agent could not find an hospital to welcome her/him
	}
	
	reflex leaveFunZone when: atFunZone!=nil and (self.location overlaps atFunZone.shape) and take_train = false{
		if (atFunZone.currentActivity != targetActivity){
			ask atFunZone{
				do leave;
			}
			atFunZone <- nil;
			targetActivity <- "";
			objective <-  "resting";
		}
	}
	
	reflex goToPark when: target = nil and objective="contemplate_nature" and take_train = false{
		list<park> park_mc <- (agents of_species park);
		park target_park;
		if(week_day = travel_day){
				
				park _park <- one_of(park_mc);
				//write("liste of companies "+comp_mc);
				//write("company "+comp);
			    loop while:(_park.minicity = minicity){
     	           remove item:_park from:park_mc;
     	           _park <- one_of(park_mc);
        		}
        		target_park <- _park;	
//        		write("je vais vers un park dans une autre ville");
		}
		else{
			target_park <- (agents of_species park) closest_to self.location;
			
		}
		if (self.location) overlaps target_park{
			if flip(0.2){
				objective <- "resting";
			}
		}
		else {
			target <- any_location_in (target_park);
		}		
	}
    
	reflex eat when: every(12 #hour){
		// On check si son repas est dispo dans le stock de la ville
		bool meal_available <- minicity.askForResources_complete(meal);
//		write "individual : "+self+"ask for a meal and th answer is "+ meal_available;
   }
   
   reflex grow when : every(365 #day){
   		age <- age + 1;
   }
   
    action goTo(point from, point to){
		//if the destination and the current position are in the same minicity
		/**MiniCity city_passenger;
		float min_dist <- #infinity;
		loop city_pas over: MiniCity.population {
			float dist <- city_pas.location distance_to self ;
			if min_dist > dist {
				min_dist <- dist;
				city_passenger <- city_pas;
			}
		}
		MiniCity city_destination;
		min_dist <- #infinity;
		loop city_dest over: MiniCity.population {
			float dist <- city_dest.location distance_to self.target ;
			if min_dist > dist {
				min_dist <- dist;
				city_destination <- city_dest;
			}
		}**/
		if( take_train = false){
		MiniCity city_passenger <- closest_to(MiniCity, self);
		MiniCity city_destination <- closest_to(MiniCity, to);
				
		Next_station <-  closest_to(city_node, self);
		final_train_dest <- closest_to(city_node, to);
		if city_passenger =  city_destination or Next_station = final_train_dest{
		speed	<- walkSpeed;
		do goto target:to;
				
		}
		else{
			
			// goes to another minicity
			path_current_edge <- 0;
			
			final_dest <- to;
			agentsOnTrain <- agentsOnTrain + 1;
			
			path_to_follow <- path_between( trainNetwork,   Next_station, final_train_dest);

			if length(path_to_follow.edges)>0 {
			do goto target:Next_station.location;			
			take_train <- true;
			}
			else{
				speed	<- walkSpeed;
				do goto target:to;
			}
			
			/** 
			if(location != Next_station.location){
			// move to closest train Station
			   	write(Next_station.location distance_to self);
				do goto target:Next_station.location;
				write("apres goto "+Next_station.location distance_to self);
				
				
			}
			assert location = Next_station.location;
			city_node closest_Dest_Station <- closest_to(city_node, to);
			do goTobyTrain( Next_station, closest_Dest_Station);
			* 
			*/
				
		}
		
		}

	}

	reflex walk when: target!=nil and take_train = false{
		do goTo( location,  target);
		if (location = target) {
            target <- nil;
        } 
		
	}
	
	reflex chose_travel_day when: week_day = 0{
	    if flip(proba_travel) {
	            travel_day <- rnd(0,6);
	        }
	   else {
	   			//write("else");
	        	travel_day <- -1;
	        }
	}
	
	reflex goTobyTrain when: take_train = true{
		assert Next_station != nil;
		assert path_to_follow != nil;
		assert location != nil;
		if(location = Next_station.location and path_current_edge < length(path_to_follow.edges)){
			    
			    try{
				Next_station <- path_to_follow.vertices[path_current_edge+1];
				if(path_current_edge>0){
				   city_edge(path_to_follow.edges[path_current_edge-1]).currCapacity <- city_edge(path_to_follow.edges[path_current_edge-1]).currCapacity - 1;
				   city_edge(path_to_follow.edges[path_current_edge-1]).capacityHour <- city_edge(path_to_follow.edges[path_current_edge-1]).capacityHour +1;
				}
				city_edge(path_to_follow.edges[path_current_edge]).currCapacity <- city_edge(path_to_follow.edges[path_current_edge]).currCapacity +1;
				
				//ralated to Individu
				//move passenger to next station with Speed TrainSpeed
				
				TrainCo2_emitted <- TrainCo2_emitted+ distance_to(location, Next_station)*Train_CO2_Emission;
				greenhouse_emissions <- greenhouse_emissions+ distance_to(location, Next_station)*Train_CO2_Emission/1000;
				
				TrainEnergy_used <- TrainEnergy_used+ distance_to(location, Next_station)*TrainEnergy;
	
				path_current_edge <- path_current_edge+1; 
				speed <- TrainSPEED;		
				do goto target:Next_station.location;
				
				}
				catch{
					speed	<- walkSpeed;
					take_train <- false;
				}
		}
		if(location = final_train_dest.location) and length(path_to_follow.edges)>0{
			city_edge(path_to_follow.edges[length(path_to_follow.edges)-1]).currCapacity <- city_edge(path_to_follow.edges[length(path_to_follow.edges)-1]).currCapacity - 1;			
			speed	<- walkSpeed;
			do goto target:final_dest;
			take_train <- false;
		}
    }
    
    state state1 initial: true { // état d'initialisation
    	transition to: state_actif when: activity = "Actif";
    	transition to: state_inactif when: activity = "Inactif";
    	transition to: state_scolarise when: activity = "Scolarise";
    	transition to: state_etudiant when: activity = "Etudiant";
    }
    
    
    state state_actif { // état actif

    	transition to: state_malade when:myHospital != nil; // go to state_malade
		
		transition to: state_inactif when: age>=65{ // on regarde si l'individu devient retraité
    		do quitJob;
    	}
    	
    	transition to: state_inactif when: (hour_day = 12 and flip(proba_become_inactive)){ // on regarde si l'individu perd son emploi
    		do quitJob;
    	}
  
	   	if(hour_day >= workPlace.openTime and workPlace.open and objective = "resting"){
	    	objective <- "working";
	    }
	    
    
	    if(objective = "working" and target = nil) {
	   		working_time <- working_time + 1;
	   }
	    
	    if(hour_day > workPlace.closingTime and objective = "working"){
	    	if flip(proba_goFunZone) { // chance d'aller à la Funzone après le travail
	    			if flip(proba_fun_at_FunZone) {
	    				objective <- "fun" ;
			     	}
			        else {
			     		objective <- "contemplate_nature" ;
			     	}
			     }
			     else{ // l'individu rentre chez soi sinon
			     	objective <- "resting";
			   	}
	    }
	    
        if(hour_day > time_to_sleep or hour_day < workPlace.openTime){ // l'individu rentre chez lui après un certain temps
			objective <- "resting" ;
			
		}
	}
   
   
	state state_inactif { // état inactif 
		transition to: state_malade when:myHospital != nil; // go to state_malade
		bool already_tried_today <- false;
    	company new_company <- nil; // la nouvelle entreprise ou il va travailler si il retrouve un travail
		
    	// il décide de chercher un travail selon une proba ( le 1/16 correspond à la proba distribue sur les heures de la journee (time_to_sleep-7 = 16 )
    	if myHospital = nil and hour_day > 7 and hour_day < time_to_sleep and age<65 and flip(1/16 * proba_become_active) and not already_tried_today{
    		//company tmp <- findNewAvailableBuildingOfSpecificType([], "company",self); not compatible with this way to proceed, maybe could still do with a workaround but right now its easier to just pick a random one
    		company tmp <- one_of (agents of_generic_species company);
    	    if flip(1 - (tmp.numberEmployees/tmp.maxNbEmployees)) {
    	    	if tmp.addEmployee(self){
    	    		new_company <- tmp;
    	    	}
    	    }
    	    already_tried_today <- true;
    	}
    
    	transition to: state_actif when: new_company!=nil and age<65{ // on regarde si l'individu retrouve un travail
//    		( "L'agent "+self.name+" a trouvé un travail dans l'entreprise"+new_company);
    		workPlace <- new_company;
    		objective <- "resting";
    	}
    	    	
    	if(hour_day > time_to_fun and objective = "resting" and flip(0.6)){// l'individu peut soit rester chez lui ou aller à la funzone
	    	if flip(0.5) {
	    		objective <- "fun" ;
			}
			else {
			    objective <- "contemplate_nature" ;
			}
	    }
	   
	   if(hour_day > time_to_sleep or hour_day < 8) {
			objective <- "resting" ;
			already_tried_today <- false;
		}
		
	}
	
	state state_malade // état malade 	
	{ 
		bool still_sick<-false;
		// if still sick, stay at hospital
		if myHospital!=nil
		{
			target <- any_location_in(myHospital);
			still_sick<- true;
		}
		
		//if agent is not sick anymore, go back to previous state
		transition to: state_actif when:myHospital=nil and workPlace !=nil and [workPlace] of_generic_species company; // go back to being active
    	transition to: state_inactif when: myHospital=nil and workPlace = nil; // go back to being inactive
    	transition to: state_scolarise when: myHospital=nil and workPlace = nil and [workPlace] of_generic_species school and age<=18 ; // go back to being scolarise
    	transition to: state_etudiant when: myHospital=nil and workPlace = nil and [workPlace] of_generic_species school and age>18 ; // go back to being etudiant
		
		// go back to home
		if still_sick{
			objective <- "resting";
			//write "the agent "+self+" has transitionned to another state ";
		}
		
    }
    

    state state_scolarise // état scolarise
   { 

        transition to: state_malade when:myHospital != nil; // go to state_malade
    
    	
    	company new_school <- nil; // la nouvelle école ou il va aller si il devient étudiant
    	
    	company new_company <- nil; // l'entreprise ou il va travailler si il devient actif
    	
    	// A 18 ans on regarde si il trouve une école ou un travail
    	if myHospital = nil and age>18{
    		if  flip(proba_become_student){
	    		building tmp <- findNewAvailableBuildingOfSpecificType([], "school",self);
	    	    new_school <- school(tmp);
    	        }
    	    else if flip(proba_become_active_after_school){
    	    	building tmp <- findNewAvailableBuildingOfSpecificType([], "company",self);
    	    	new_company <- company(tmp);
    	    }
    	 }
    
    	transition to: state_etudiant when: new_school!=nil and age>18{ // on regarde si l'individu devient étudiant à sa majorité
    			do quitSchool;
    	    	workPlace <- new_school;
    			assert [workPlace] of_generic_species school;
    	    	
    	} 
    	transition to: state_actif when: new_company!=nil and age>18{ // si l'individu ne devient pas étudiant alors il devient actif
    	    	do quitSchool;
    	    	workPlace <- new_company;
    	}
    	
    	transition to: state_inactif when: age>18{ // si l'individu ne devient pas étudiant alors il devient actif
    		do quitSchool;
    	}
    	
    	
    	if(hour_day >= workPlace.openTime and workPlace.open and objective = "resting") {
	    	objective <- "study" ;
	    }
    
	    if(objective = "study" and target = nil) {
	   		working_time <- working_time + 1;
	   }
	    
	    if(hour_day > workPlace.closingTime and objective = "study"){
	    	if flip(0.8) { // chance d'aller à la Funzone après le travail
	    		if flip(0.5) {
	    			objective <- "fun" ;
				}
				else {
			    	objective <- "contemplate_nature" ;
				}	
			}
		    else{ // l'individu rentre chez soi sinon
				objective <- "resting";
			}
	    }
	    
        if(hour_day > time_to_sleep or hour_day <  workPlace.openTime){ // l'individu rentre chez lui après un certain temps
			objective <- "resting" ;
		}
	}
    
state state_etudiant { // état étudiant
    
    	assert [workPlace] of_generic_species school;
    	
    	//assert [workPlace] of_generic_species school;
    	transition to: state_malade when: myHospital != nil; // go to state_malade

    	company new_company <- nil; // l'entreprise ou il va travailler si il devient actif
    	bool already_tried_today <- false;
    	
    	// il décide de chercher un travail selon une proba
    	if myHospital =  nil and hour_day > 7 and hour_day < time_to_sleep and age<65 and flip(1/16 * proba_become_active_when_etudiant) and not already_tried_today{
    		//company tmp <- findNewAvailableBuildingOfSpecificType([], "company",self); not compatible with this way to proceed, maybe could still do with a workaround but right now its easier to just pick a random one
    		company tmp <- one_of (agents of_generic_species company);
    	    if flip(1 - (tmp.numberEmployees/tmp.maxNbEmployees)) {
    	    	if tmp.addEmployee(self){
    	    		new_company <- tmp;
    	    	}
    	    }
    	    already_tried_today <- true;
    	}
    
    	
    	transition to: state_actif when: new_company!=nil{ // on regarde si l'étudiant devient actif
    			do quitSchool;
    	    	workPlace <- new_company;
    			//write 'agent '+self+" is in state "+self.state+ "and has for workplace a company : "+self.workPlace;
    	}
    	
    	transition to: state_inactif when: (hour_day = 12 and flip(proba_become_inactive_when_etudiant)) or age>=65{ // on regarde si l'étudiant devient inactif
    		do quitSchool;
		}
    	
		if(hour_day >= workPlace.openTime and workPlace.open and objective = "resting") {
	    	objective <- "study" ;
	    }
    
	    if(objective = "study" and target = nil) {
	   		working_time <- working_time + 1;
	   }
	    
	    if(hour_day > workPlace.closingTime and objective = "study"){
	    	if flip(0.8) { // chance d'aller à la Funzone après le travail
	    		if flip(0.5) {
	    			objective <- "fun" ;
				}
				else {
			    	objective <- "contemplate_nature" ;
				}	
			}
			else{ // l'individu rentre chez soi sinon
				objective <- "resting";
			}
	    }
	    
        if(hour_day > time_to_sleep or hour_day < workPlace.openTime ){ // l'individu rentre chez lui après un certain temps
			objective <- "resting" ;
			
		}
			
	}
			   
    
    
    action quitJob{
    	//write "agent "+self+"in state "+self.state+" have quit their job at "+workPlace;
    	assert workPlace!=nil;
    	
	 	ask workPlace
	 	{
			do removeEmployee(myself);
		}
		
		workPlace <- nil;
		objective<- "resting";
    }
    
    action quitSchool{
    	//write "agent "+self+"in state "+self.state+" have quit their school  "+workPlace;
    	assert [workPlace] of_generic_species school;
    	ask school(workPlace)
	 	{
			do removeStudent(myself);
		}
		
    	assert workPlace!=nil;
    	objective<- "resting";
    }
    
   
    
	// supposed to be used only in findNewAvailableBuildingOfSpecificType 
	// called when an individual wants to find a new reference to an available building ( there is still room for her/him) of type typeOfBulding in a given MiniCity
	building findNewAvailableBuildingOfSpecificTypeInMiniCity(MiniCity mc, list<building> buildingsToAvoid, string typeOfBuilding, individu ind){

		
		// look for agent typeOfBulding in my MiniCity
		loop bld over: shuffle(mc.l_buildings) of_generic_species species(typeOfBuilding) //filters the list of building of the minicity
		{
			assert( [bld] of_generic_species building );
			
			if not (bld in buildingsToAvoid) //checks that we do not return a building that we wish to avoid
			{
				//call corresponding methods if necessary and if bld is available returns it
				switch species(typeOfBuilding) 
				{
					match company
					{
						if company(bld).addEmployee(ind)
						{
							return building(bld);
						}
					}
					match healthCenter
					{
						if healthCenter(bld).addPatient(ind)
						{
							return building(bld);
						}
					}
					match funZone 
					{
						return building(bld);
					}
					
					match school{
						if school(bld).addStudent(ind)
						{
							return building(bld);
						}
					}
				}


			}
		}
		return nil;
	}
	

	// called when an individual wants to find a new reference to an available building ( there is still room for her/him) of type typeOfBulding
	// will search in the individual miniCity, then its constellation, then in neighboring big cites
	building findNewAvailableBuildingOfSpecificType(list<building> buildingsToAvoid, string typeOfBuilding, individu ind)
	{
		
		MiniCity myMiniCity <- ind.minicity;
		
		//look for building of type typeOfBulding in myMiniCity
		building bld <-  findNewAvailableBuildingOfSpecificTypeInMiniCity(myMiniCity, buildingsToAvoid,typeOfBuilding, ind) ;
		if (bld!= nil )
		{
			return bld;
		}
		
		//look for building of type typeOfBulding in the constellation of miniCities the individual is in
		list<MiniCity> myConstellation <- myMiniCity.bigcity.list_minicities_constellation;
		
		loop mc over: myConstellation 
		{
			if mc!=myMiniCity
			{
				building bld <-  findNewAvailableBuildingOfSpecificTypeInMiniCity(myMiniCity, buildingsToAvoid,typeOfBuilding, ind) ;
				if (bld!= nil )
				{
					return bld;
				}
			}
		}
		
		//////////DO NOT DELETE\\\\\\\\
		//TODO: when this is possible, if this part also works 
		/* 
		//look for building of type typeOfBulding in the constellation of miniCities next to the one where the individual is in
		list<graph_edge> tmp <- trainNetwork.edges;
		
		//get my minicity node
		city_node node_myMiniCity <- closest_to(city_node, myMiniCity.bigcity);
		assert node_myMiniCity != nil;
		
		//big process with trainNetwork to get all neighboring big cities
		list<city_node> nc_l<-[];
		loop g_e over: tmp{
			if g_e.source = node_myMiniCity{
				add g_e.target to: nc_l;
			}
			if g_e.target = node_myMiniCity{
				add g_e.source to: nc_l;
			}
		}
		list<BigCity> neighboring_big_cites <-[];
		loop e over: nc_l{
			add closest_to(BigCity, e) to: neighboring_big_cites; // si ça ne marche pas avec closest_to rajouter une ref à la big city dans les city_node
		}
		
		//loop over all neighboring big cities to check if any company is hiring
		loop bc over:neighboring_big_cites
		{
			list<MiniCity> constellation <-bc.list_minicities_constellation;
			
			//over all minicities in a given bigCity constellation
			loop mc over: constellation
			{
				if mc!=myMiniCity
				{
					building bld <-  findNewAvailableBuildingOfSpecificTypeInMiniCity(myMiniCity, buildingsToAvoid,typeOfBuilding, ind) ;
					if (bld!= nil )
					{
						return bld;
					}
					
				}
			}
		}
		*/
		warn "an individual could not find building of type "+typeOfBuilding;
		return nil; // no building of type typeOfBulding found
	}
	
	
	
	
    aspect {
		draw circle(10);
	}   
}