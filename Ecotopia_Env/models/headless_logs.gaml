 /**
* Name: mainlogs
* Based on the internal empty template. 
* Author: Marius
* Tags: 
*/


model headless_logs
import "california_environment.gaml"
import "../general/individual.gaml"
import "../general/mini_city.gaml"
import "../general/parameters.gaml"
import "../Ecosystem/farms.gaml"
import "../Ecosystem/forest.gaml"

/* Insert your model definition here */

global {
	
	//LOGS PARAMS
	string log_start;
	string log_folder <- "../logs/";
	float time_logs <- 1#day;
	//int id_run <- 3;
	string general_logs_file_name <-"general_logs";
	string demograhic_logs_file_name <- "demographic_logs";
	string EFM_logs_file_name <- "EFM_logs";
	int nb_log <- 0;
	matrix EFM_values;
	list EFM_names;
	


	reflex calculate_conso {
	    meat_water_conso <- total_consommation[0]*animal_consommation_water;
	    meat_ghg_conso <- total_consommation[0]*animal_production_ghg;
	    meat_waste_conso <- total_consommation[0]*animal_production_waste;
	    
		crop_water_conso <- total_consommation[1]*crop_consommation_water;
		crop_ghg_conso <- total_consommation[1]*crop_production_ghg;
		crop_waste_conso <- total_consommation[1]*crop_conso_waste;
		
		tot_water_conso <- meat_water_conso + crop_water_conso;
		tot_ghg_conso <- meat_ghg_conso + crop_ghg_conso;
		tot_waste_conso <- meat_waste_conso + crop_waste_conso;
	}
	
	
	// LOG RELFEXS
	reflex log when: every (time_logs) {
		
	    // demographic logs
	    int scolarise <- individu count (each.state = "state_scolarise" or ( each.state="state_malade" and each.workPlace != nil and [each.workPlace] of_generic_species school and each.age<18));
	    int inactifs <- individu count (each.state = "state_inactif" or ( each.state="state_malade" and each.workPlace = nil));
	    int actifs <- individu count (each.state = "state_actif" or ( each.state="state_malade" and each.workPlace != nil and [each.workPlace] of_generic_species company and each.age>18 ));
	    int etudiants <- individu count (each.state = "state_etudiant" or ( each.state="state_malade" and each.workPlace != nil and [each.workPlace] of_generic_species school and each.age>18));
	    int malades <- individu count (each.state = "state_malade");
	    int hommes <- individu count (each.gender = "Homme");
	    int femmes <- individu count (each.gender = "Femme");
	    save ""+scolarise+";"+inactifs+";"+actifs+";"+etudiants+";"+malades+";"+hommes+";"+femmes to:log_folder+"/"+log_start+"/"+ demograhic_logs_file_name +".txt"  type: "text" rewrite: false;
	    
		// EFM 
		save ""+biomass_waste+";"+biomass_waste_produced+";"+biomass_waste_consumed+";"+greenhouse_emissions+";"+greenhouse_emissions_produced+";"+greenhouse_emissions_consumed+";"+water_consommation  to: log_folder+"/"+log_start+"/" +EFM_logs_file_name +".txt"  type: "text" rewrite: false;
		
	    // general logs
	    save ""+(time_logs*nb_log)/(1#day)+";"+greenhouse_emissions+";"+biomass_waste+";"+water_consommation+";"+tot_water_conso+";"+tot_ghg_conso+";"+tot_waste_conso+";"+meat_water_conso+";"+crop_water_conso+";"+meat_ghg_conso+";"+crop_ghg_conso+";"+meat_waste_conso+";"+crop_waste_conso to: log_folder+"/"+log_start+"/" +general_logs_file_name +".txt"  type: "text" rewrite: false;
	    
	       //EFM logs:
		float CEnT;
	    float last_Cvm;
	    float last_Clm;
	    float last_TGesEmission;
	   	float Sv;  
		float Sl;
		float Sc;
		float Sp;
		float Sb;
		float last_water_conso;
		float last_cotton_water_conso;
		ask ElectricTransportation at 0 {
    		CEnT <- conso_totale;
    	}
    	ask ElectricTransportation at 1 {
    		CEnT <- CEnT + conso_totale;
			
    	}
    	Sb <- sum((agents of_generic_species MiniCity) collect(each.resourceStock["wood"]));
		Sp <- sum((agents of_generic_species MiniCity) collect(each.resourceStock["plastic"]));
    	
    	Sv <- sum(farm_cell where (each.image=0) collect(each.meat)) + sum((agents of_generic_species MiniCity) collect(each.resourceStock["meat"]));
		Sl <- sum(farm_cell where (each.image=1) collect(each.crop)) + sum((agents of_generic_species MiniCity) collect(each.resourceStock["crop"]));
		Sc <- sum(farm_cell where (each.image=2) collect(each.cotton)) + sum((agents of_generic_species MiniCity) collect(each.resourceStock["cotton"]));  	
		save "day: "+(time_logs*nb_log)/(1#day) + " CEnM: " +conso_buildings +" CEnT: "+CEnT+" CenP: "+conso_plastic+" Pen: "+tot_prod_weekly to: log_folder+"/"+log_start+"/"  + "energyEFM" + ".log" type: "text" rewrite: false;
		save "day: "+(time_logs*nb_log)/(1#day) +" Cvm: "+ (total_consommation[0] - last_Cvm)+ " Sv: "+Sv+" Pv: "+meat_prod  to: log_folder+"/"+log_start+"/"  + "viandeEFM" + ".log" type: "text" rewrite: false;
		save "day: "+(time_logs*nb_log)/(1#day) +" Clm: "+ (total_consommation[1] - last_Clm)+ " Sl: "+Sl+" Pl: "+crop_prod   to: log_folder+"/"+log_start+"/"  + "cropEFM" + ".log" type: "text" rewrite: false;
		save "day: "+(time_logs*nb_log)/(1#day) +" Ccp: "+ cotton_conso+" Sc: "+Sc+" Pc: "+cotton_prod to: log_folder+"/"+log_start+"/"  + "cottonEFM" + ".log" type: "text" rewrite: false;
		save "day: "+(time_logs*nb_log)/(1#day) + " Cpm: "+plastic_conso+" Sp: " +Sp + "Pp: "+plastic_prod  to: log_folder+"/"+log_start+"/"  + "plasticEFM" + ".log" type: "text" rewrite: false;
		save "day: "+(time_logs*nb_log)/(1#day) + " Sb: " +Sb  to: log_folder+"/"+log_start+"/"  + "woodEFM" + ".log" type: "text" rewrite: false;
		
		save "day: "+(time_logs*nb_log)/(1#day) + " CeA: " +(tot_water_conso - last_water_conso)+" CeP: "+(cotton_water_conso - last_cotton_water_conso)  to: log_folder+"/"+log_start+"/"  + "waterEFM" + ".log" type: "text" rewrite: false;
		save "day: "+(time_logs*nb_log)/(1#day) +" EgT: "+ ((truckCo2_emitted + TrainCo2_emitted)/1000 - last_TGesEmission) +" EgP: "+plastic_ghg_emissions+" EgEn: "+energy_ghg_emissions + " Sg: "+greenhouse_emissions +" ghg_absorption: "+forest_ghg_absorption+" EgA: "+agriculture_ghg_emissions  to: log_folder+"/"+log_start+"/"  + "GesEFM" + ".log" type: "text" rewrite: false;
		
		save "day: "+(time_logs*nb_log)/(1#day) +" PbdM: " + biomass_waste_building+" CbdA: "+ biomass_agriculture_conso +" PbdA: "+biomass_agriculture to: log_folder+"/"+log_start+"/"  + "BiomassEFM" + ".log" type: "text" rewrite: false;
		
		// bois
		EFM_values[0,0] <- 0;
		EFM_values[0,0] <- wood_conso; // +CB
		EFM_values[0,2] <- wood_prod-wood_conso; // PB-CB
		EFM_values[0,3] <- Sb; // SB
		// Eau
		EFM_values[1,0] <- 0; // +CEM ils ne boivent pas
		EFM_values[1,2] <- 0 - 0 - 0 - (tot_water_conso - last_water_conso) - (cotton_water_conso - last_cotton_water_conso); //PE −CEM −CEE −CEA −CEP pas de stock + ils boivent pas
		EFM_values[1,3] <- 0; // SE
		EFM_values[1,4] <- 0; // +CEE
		EFM_values[1,5] <- tot_water_conso - last_water_conso; // +CEA
		EFM_values[1,7] <- cotton_water_conso - last_cotton_water_conso; // +CEP
		//Energie
	    EFM_values[2,0] <- conso_buildings; // +CEnM
		EFM_values[2,1] <- CEnT; //CEnT
		EFM_values[2,4] <- tot_prod_weekly - conso_buildings - CEnT - conso_plastic; // +Pen - CEnM - CEnT - CenP
		EFM_values[2,7] <- conso_plastic; // +CEnP
		
		// Viande
		EFM_values[3,0] <-  (total_consommation[0] - last_Cvm); // +CVM 
		EFM_values[3,5] <- meat_prod- (total_consommation[0] - last_Cvm); // PV-CVM
		EFM_values[3,6] <- Sv; // SV
		// Légumes
		EFM_values[4,0] <- (total_consommation[1] - last_Clm); // +CLM 
		EFM_values[4,5] <- crop_prod-(total_consommation[1] - last_Clm); // PL-CLM
		EFM_values[4,6] <- Sl; // SL
		// Coton
		EFM_values[5,5] <- cotton_prod-cotton_conso; // PC-CCP
		EFM_values[5,7] <- Sc; // SC
		EFM_values[5,7] <- cotton_conso; // +CCP
		// Plastique
		EFM_values[6,0] <- plastic_prod ; // +PP
		EFM_values[6,7] <- plastic_prod-plastic_conso; // PP - CMP
		EFM_values[6,8] <- Sp; // +SP
		// Biodechet
		EFM_values[7,0] <-  biomass_waste_building - 0.0 ; // PbdM - CbdM
		EFM_values[7,5] <- 0.0 - biomass_agriculture_conso ; // + CBdM + CBdP consommation apr agriculture 
		EFM_values[7,8] <- 0.0; // +SP
		
		// GES
		EFM_values[8,0] <-  0.0 ; // EgM
		EFM_values[8,1] <- ((truckCo2_emitted + TrainCo2_emitted)/1000 - last_TGesEmission) ; // + EgT
		EFM_values[8,2] <- forest_ghg_absorption - energy_ghg_emissions - plastic_ghg_emissions - agriculture_ghg_emissions - ((truckCo2_emitted + TrainCo2_emitted)/1000 - last_TGesEmission); // PgF -EgM -EgT -EgEn -EgP - EgA 
		EFM_values[8,3] <- greenhouse_emissions;  //Sg
		EFM_values[8,4] <- energy_ghg_emissions;  //EgEn
		EFM_values[8,5] <- agriculture_ghg_emissions;  //EgA
		EFM_values[8,7] <- plastic_ghg_emissions;  //EgP
		
	    save "day: "+(time_logs*nb_log)/(1#day) to: log_folder+"/"+log_start+"/"  + "EFM" + ".log" type: "text" rewrite: false;
		save transpose(EFM_values) to: log_folder+"/"+log_start+"/"  + "EFM" + ".log" type: "text" rewrite: false;
		
		
		//meat and crops
		last_Cvm <-  total_consommation[0];
		last_Clm <-  total_consommation[1];
		last_cotton_water_conso <- cotton_water_conso;
		last_water_conso <- tot_water_conso;
		meat_prod <- 0.0;
		crop_prod <- 0.0;
		//energy
		conso_buildings <-0.0;
	    conso_plastic <-0.0;
	    tot_conso_weekly <- 0.0;
	    tot_prod_weekly <- 0.0;
	    
	    //cotton
	    cotton_conso <- 0.0;
	    cotton_prod <- 0.0;
	    
	    //plastic
	    plastic_prod <- 0.0;
	    plastic_conso <- 0.0;
	    //ghg
	    last_TGesEmission <- (truckCo2_emitted + TrainCo2_emitted)/1000;
	    plastic_ghg_emissions <- 0.0;
	    energy_ghg_emissions <- 0.0;
	    forest_ghg_absorption <- 0.0;
	
		nb_log <- nb_log + 1;
	    
	}
	
	//logs by energy
	reflex energy_central_totwater when: every (time_logs) {
	save ""+(time_logs*nb_log)/(1#day) + " "+ tot_water to: log_folder+"/"+log_start+"/"  + "nuclear_water" + ".log" type: "text" rewrite: false;
	tot_water <- 0.0;
	
	}
	/*** 
	reflex energy_totconsom when: every (time_logs)  {
		
		save ""+(time_logs*nb_log)/(1#day) + " " +conso_buildings  to: log_folder+"/"+log_start+"/"  + "Energy Building consumption" + ".log" type: "text" rewrite: false;
		save ""+(time_logs*nb_log)/(1#day) + " " +conso_plastic  to: log_folder+"/"+log_start+"/" + "Energy Plastic consumption" +".log" type: "text" rewrite: false;
		save ""+(time_logs*nb_log)/(1#day) + " " +tot_conso_weekly  to: log_folder+"/"+log_start+"/" + "energy_tot_conso_weekly" + ".log" type: "text" rewrite: false;
		save ""+(time_logs*nb_log)/(1#day) + " " +tot_prod_weekly  to:log_folder+"/"+log_start+"/" +  "energy_tot_prod_weekly" + ".log" type: "text" rewrite: false;
		conso_buildings <-0.0;
	    conso_plastic <-0.0;
	    tot_conso_weekly <- 0.0;
	    tot_prod_weekly <- 0.0;
	}
	
	***/
	//energy for transport
	reflex update_energy_transport{
		ask ElectricTransportation at 0 {
    		conso_totale <- (truckEnergy_used - last)/1000;
    		last <- truckEnergy_used;
    	}
    	ask ElectricTransportation at 1 {
    		conso_totale <- (TrainEnergy_used - last)/1000;
    		last <- TrainEnergy_used;
    	}	
	}
	
	//Transport logs
	reflex log_trucks when: every (#day) {

	    int nb_trucks_nonfree <- truck count (each.free = false);
	    int nb_trucks_crops <- truck count (each.free = false and each.typeOfMerchandise = "crop");
	    int nb_trucks_wood <- truck count (each.free = false and each.typeOfMerchandise = "wood");
	    int nb_trucks_meat <- truck count (each.free = false and each.typeOfMerchandise = "meat");
	    int nb_trucks_coton <- truck count (each.free = false and each.typeOfMerchandise = "cotton");	    
	    save "trucks non free "+nb_trucks_nonfree+"coton: "+nb_trucks_coton+" meat: "+nb_trucks_meat+" crop: "+nb_trucks_crops+"wood"+nb_trucks_wood to: log_folder+"/"+log_start+"/" +"trucks_logs.txt" type: "text" rewrite: false;	

		save "day: "+week_day+"energy: "+ truckEnergy_used +" co2: "+truckCo2_emitted to: log_folder+"/"+log_start+"/" +"truck_conso.log" type: "text" rewrite: false;	
		if(week_day = 0){
			truckEnergy_used <- 0;
			truckCo2_emitted <- 0;
		}
	}
	
	reflex log_saturation_edges when: every (#hour) {
		int nb_saturated <- city_edge count (each.capacityHour > max_cap);
			    
		save "hour: "+hour_day+" saturated_edges: "+nb_saturated+" agents_train: "+agentsOnTrain to:log_folder+"/"+log_start+"/" +"saturation_train_logs"+max_cap+".txt" type: "text" rewrite: false;	
	}
		
	reflex log_individues_objectives when: every (#hour) {

		 int nb_agents_traveling <-  individu count (each.travel_day = week_day);
		 int nb_agents_resting <-  individu count (each.objective = "resting");
		 int nb_agents_working <-  individu count (each.objective = "working");
		 int nb_agents_study <-  individu count (each.objective = "study");
		 int nb_agents_fun <-  individu count (each.objective = "fun");
		 int nb_agents_nature <-  individu count (each.objective = "contemplate_nature");

		save "hour: "+hour_day+" working: "+nb_agents_working+" study: "+nb_agents_study+" travel: "+nb_agents_traveling+" resting: "+nb_agents_resting+" fun: "+nb_agents_fun+" nature: "+nb_agents_nature to: log_folder+"/"+log_start+"/" +"individus_planning.txt" type: "text" rewrite: false;	
			
	}
	
	
	init {
		log_start <- string(#now, 'yyyy-MM-dd-HH-mm-ss');
		do load_grid;
		
		// INITIAL RUN PARAMS LOGS
		save "scolarise ; inactifs ; actifs ; etudiants ; malades ; hommes ; femmes" to:log_folder+"/"+log_start+"/" + demograhic_logs_file_name +".txt" type: "text" rewrite: true;
		save "Time_since_start ; greenhouse_emissions ; biomass_waste ; water_consommation ; tot_water_conso ; tot_ghg_conso ; tot_waste_conso ; meat_water_conso ; crop_water_conso ; meat_ghg_conso ; crop_ghg_conso ; meat_waste_conso ; crop_waste_conso" to: log_folder+"/"+log_start+"/" + general_logs_file_name +".txt"  type: "text" rewrite: false;
		save "biomass_waste ; biomass_waste_produced ; biomass_waste_consumed ; greenhouse_emissions ; greenhouse_emissions_produced ; greenhouse_emissions_consumed ; water_consommation"  to: log_folder+"/"+log_start+"/" +EFM_logs_file_name +".txt"  type: "text" rewrite: true;
		
		
		do instanciate_MiniCities;
		
		write "--------------------------------\n\tTEST INITIALISATION\n--------------------------------";
		
		do test_initialisation_MiniCities;
		
		do test_initialisation_inidividus_of_MiniCities;
		
		do test_initialisation_buildings_of_MiniCities;
		
		do test_demography;
		
		//creat two energy transtport
		//0 -> marchandise
		//1 -> personne
		create ElectricTransportation number: 2;
		
		//transport
		
		trainNetwork <- createTrainGraph(file("../includes/california_cities.shp"));
		
		create MerchandiseManager;
		
		create truck number: nb_truck {
			
     		location <- point(1 among city);//todo in V2 reapartir les camions sur les differents champs de manière équitable 
	   						  // et leurs postition initiale sera la postion du champ
	 
	
     	}
     	
    	do createEFMMatrix;
	}
		action createEFMMatrix {
		// matrix 9x10
		EFM_values <- matrix([[0,0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0,0]]);
		// matrix names 9x10
		EFM_names <- [
			["Ménages Variation", "Transport Variation", "Ecosysteme Variation", "Ecosysteme Stock", "Fournisseur d’Energie Variation", "Agriculture Variation", "Agriculture Stock", "Production de plastique Variation", "Production de plastique Stock"],
			["Bois", "Eau", "Energie", "Viande", "Légumes", "Coton", "Plastique", "Biodéchets", "Autres", "GES"]
		];
	}
	
//	reflex EFMCalculate when: every(24 #hours) { // Tests pour verifier si il y a des champs des 3 types sur la carte
//		meat_stock <- sum(farm_cell where (each.image=0) collect(each.meat)) + sum((agents of_generic_species MiniCity) collect(each.resourceStock["meat"]));
//		crop_stock <- sum(farm_cell where (each.image=1) collect(each.crop)) + sum((agents of_generic_species MiniCity) collect(each.resourceStock["crop"]));
//		cotton_stock <- sum(farm_cell where (each.image=2) collect(each.cotton)) + sum((agents of_generic_species MiniCity) collect(each.resourceStock["cotton"]));
//		wood_stock <- sum((agents of_generic_species MiniCity) collect(each.resourceStock["wood"]));
//		plastic_stock <- sum((agents of_generic_species MiniCity) collect(each.resourceStock["plastic"]));
//	}
	
	/**reflex setEFMMatrix_Ecosysteme when: every(24 #hours){

	}**/
	
	action test_initialisation_MiniCities {
		// Calculating the "constellationPopulation" and the length of the "list_minicities_constellation" attributes for every BigCity
		// the first one is supposed to be equals to nb_agent_init, the second one to nb_minicity_init.
		int poptotBC <- 0;
		int nb_mc_BC <- 0;
		loop bigcity over: BigCity {
			poptotBC <- poptotBC + bigcity.constellationPopulation;
			nb_mc_BC <- nb_mc_BC + length(bigcity.list_minicities_constellation);
		}
		
		// Calculating the "population" attribute for every BigCity
		// the first one is supposed to be equals to nb_agent_init, the second one to nb_minicity_init. 
		int poptotMC <- 0;
		loop minicity over: agents of_generic_species MiniCity {
			poptotMC <- poptotMC + minicity.population;
//			write minicity.l_buildings;
		}

		// Prints to see the initialisation's stats
		write "nb_minicity_init = " + nb_minicity_init;
		write "nb MiniCity instanciées = " + length(agents of_generic_species MiniCity);
		write "sum list(MiniCity) des cities = " + nb_mc_BC;
		write "nb_agent_init = " + nb_agent_init;
		write "sum BigCity.pop  = " + poptotBC; // doesn't work
		write "sum MiniCity.pop = " + poptotMC;
		write "nb individus instanciées = " + length(agents of_generic_species individu);
		// Tests initialisation mini-villes
		assert length(agents of_generic_species MiniCity) = nb_minicity_init;
		assert nb_mc_BC = nb_minicity_init;
		assert poptotBC = nb_agent_init;
		assert poptotMC = nb_agent_init;
	}
	
	action test_initialisation_inidividus_of_MiniCities {
		// Tests initialisation individus
		assert length(agents of_generic_species individu) = nb_agent_init;
	}
	
	action test_demography{
		write "nb individus instanciées = " + length(agents of_generic_species individu);
		write "nb individus à plus de 65 ans = " +  length(agents of_generic_species individu where(each.age>=65));
		write "nb individus inactif à plus de 65 ans = " +  length(agents of_generic_species individu where(each.age>=65 and each.activity="Inactif"));
	    write "nb individus à moins de 18 ans = " +  length(agents of_generic_species individu where(each.age<=18));
	    write "nb individus scolarise à moins de 18 ans = " +  length(agents of_generic_species individu where(each.age<=18 and each.activity="Scolarise"));
	    write "nb individus entre 18 et 65 ans = " +  length(agents of_generic_species individu where(each.age<65 and each.age>18));
		write "nb individus inactif entre 18 et 65 ans = " +  length(agents of_generic_species individu where(each.age<65 and each.age>18 and each.activity="Inactif"));
		write "nb individus actif entre 18 et 65 ans = " +  length(agents of_generic_species individu where(each.age<65 and each.age>18 and each.activity="Actif"));
		write "nb individus etudiant entre 18 et 65 ans = " +  length(agents of_generic_species individu where(each.age<65 and each.age>18 and each.activity="Etudiant"));
	}
	
	action test_initialisation_buildings_of_MiniCities {
		// Tests initialisation individus
		loop minicity over: agents of_generic_species MiniCity {
			int nb_tS <- minicity.l_buildings count (string(type_of(each)) = "trainStation");
			assert (nb_tS) = 1; // a minicity have to have a trainStation
		}
		assert length(agents of_generic_species individu) = nb_agent_init;
	}
}

experiment HEADLESS_LOGS_PARALLEL  type:batch repeat:3 parallel:true until:(time_logs*nb_log)/(1#day) > 365 { 
	
}

experiment HEADLESS_LOGS type:gui until:(time_logs*nb_log)/(1#day) > 365{ output{ monitor "debug" value: 1;}}

