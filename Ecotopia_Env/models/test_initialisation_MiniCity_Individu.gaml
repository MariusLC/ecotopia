/**
* Name: MiniCity
* Based on the internal empty template. 
* Author: Marius Le Chapelier
* Tags: 
*/


model test_initialisation_MiniCity_Individu
import "california_environment.gaml"
import "../general/individual.gaml"
import "../general/mini_city.gaml"
import "../general/parameters.gaml"
import "../Ecosystem/farms.gaml"
import "../Ecosystem/forest.gaml"

/* Insert your model definition here */

global {
	
	
	//energy for transport
	reflex update_energy_transport{
		ask ElectricTransportation at 0 {
    		conso_totale <- (truckEnergy_used - last)/1000;
    		last <- truckEnergy_used;
    	}
    	ask ElectricTransportation at 1 {
    		conso_totale <- (TrainEnergy_used - last)/1000;
    		last <- TrainEnergy_used;
    	}	
	}
	
	
	init {
		do load_grid;
		
		do instanciate_MiniCities;
		
		write "--------------------------------\n\tTEST INITIALISATION\n--------------------------------";
		
		do test_initialisation_MiniCities;
		
		do test_initialisation_inidividus_of_MiniCities;
		
		do test_initialisation_buildings_of_MiniCities;
		
		do test_demography;
		
		do test_buildings;
		//creat two energy transtport
		//0 -> marchandise
		//1 -> personne
		create ElectricTransportation number: 2;
		
		//transport
		
		trainNetwork <- createTrainGraph(file("../includes/california_cities.shp"));
		
		create MerchandiseManager;
		
		create truck number: nb_truck {
			
     		location <- point(1 among city);//todo in V2 reapartir les camions sur les differents champs de manière équitable 
	   						  // et leurs postition initiale sera la postion du champ
	 
	
     	}
    	
		
		//fin transport
		
		create SolarPlant {
			//nb_solar <- int((16* 14,6*1000000 )/26);
			prod_constante <- 1.0 *39215 * 1000 *365 *24;
		}
		
		create SolarPlant {
			//nb_solar <- int((16* 6,5*1000000 )/26);
			prod_constante <- 1.0 *5.5 * 1000 *365 *24;
		}
		create SolarPlant {
			//nb_solar <- int((16* 7,8*1000000 )/26);
			prod_constante <- 1.0 *354 * 1000 *365 *24;
		}
		create SolarPlant {
			//nb_solar <- int((16* 18,8*1000000 )/26);
			prod_constante <- 1.0 *250 * 1000 *365 *24;
		}
		
		create SolarPlant {
			//nb_solar <- int((16* 26000)/26);
			prod_constante <- 1.0 *250 * 1000 *365 *24;
		}
				
//		// TEST create_grid et pick_random_location
//		list l <- createCircularGrid( point(500,500), 10, 1);
//		matrix mat <- l[0];
//		int mat_size <- l[1];
//		list shape_object <- [5,5];
//		list res <- pickRandomLocationInMatrix(mat, mat_size, shape_object);
//		
//		// TEST create_grid et pick_random_location
//		MiniCity mc <- MiniCity[0];
//		list l <- createCircularGrid( mc.location, mc.diameter, cellSizeMiniCityGrid);
//		matrix mat <- l[0];
//		int mat_size <- l[1];
//		bool ava <- true;
//		list buildings <- [];
//		loop while: ava{
//			int building_size <- rnd(1,int(mat_size/4));
//			list shape_object <- [building_size,building_size];
//			list res <- pickRandomLocationInMatrix(mat, mat_size, shape_object);
//			if res[0] = -1 {
//				ava <- false;
//			} else {
//				add [building_size, res] to: buildings;
//			}
//		}
//		write mat;
//		write buildings;
//		write length(individu);
		/*int i <- 0;
		loop ind over: individu {
			write i;
			assert ind.minicity != nil;
			assert ind.house != nil;
			assert ind.workPlace != nil;
			assert ind.atFunZone != nil;
			i <- i+ 1;
		}*/
	}
	
	action test_initialisation_MiniCities {
		// Calculating the "constellationPopulation" and the length of the "list_minicities_constellation" attributes for every BigCity
		// the first one is supposed to be equals to nb_agent_init, the second one to nb_minicity_init.
		int poptotBC <- 0;
		int nb_mc_BC <- 0;
		loop bigcity over: BigCity {
			poptotBC <- poptotBC + bigcity.constellationPopulation;
			nb_mc_BC <- nb_mc_BC + length(bigcity.list_minicities_constellation);
		}
		
		// Calculating the "population" attribute for every BigCity
		// the first one is supposed to be equals to nb_agent_init, the second one to nb_minicity_init. 
		int poptotMC <- 0;
		loop minicity over: agents of_generic_species MiniCity {
			poptotMC <- poptotMC + minicity.population;
//			write minicity.l_buildings;
		}

		// Prints to see the initialisation's stats
		write "nb_minicity_init = " + nb_minicity_init;
		write "nb MiniCity instanciées = " + length(agents of_generic_species MiniCity);
		write "sum list(MiniCity) des cities = " + nb_mc_BC;
		write "nb_agent_init = " + nb_agent_init;
		write "sum BigCity.pop  = " + poptotBC; // doesn't work
		write "sum MiniCity.pop = " + poptotMC;
		write "nb individus instanciées = " + length(agents of_generic_species individu);
		// Tests initialisation mini-villes
		assert length(agents of_generic_species MiniCity) = nb_minicity_init;
		assert nb_mc_BC = nb_minicity_init;
		assert poptotBC = nb_agent_init;
		assert poptotMC = nb_agent_init;
	}
	
	action test_initialisation_inidividus_of_MiniCities {
		// Tests initialisation individus
		assert length(agents of_generic_species individu) = nb_agent_init;
	}
	
	action test_demography{
		write "nb individus instanciées = " + length(agents of_generic_species individu);
		write "nb individus à plus de 65 ans = " +  length(agents of_generic_species individu where(each.age>=65));
		write "nb individus inactif à plus de 65 ans = " +  length(agents of_generic_species individu where(each.age>=65 and each.activity="Inactif"));
	    write "nb individus à moins de 18 ans = " +  length(agents of_generic_species individu where(each.age<=18));
	    write "nb individus scolarise à moins de 18 ans = " +  length(agents of_generic_species individu where(each.age<=18 and each.activity="Scolarise"));
	    write "nb individus entre 18 et 65 ans = " +  length(agents of_generic_species individu where(each.age<65 and each.age>18));
		write "nb individus inactif entre 18 et 65 ans = " +  length(agents of_generic_species individu where(each.age<65 and each.age>18 and each.activity="Inactif"));
		write "nb individus actif entre 18 et 65 ans = " +  length(agents of_generic_species individu where(each.age<65 and each.age>18 and each.activity="Actif"));
		write "nb individus etudiant entre 18 et 65 ans = " +  length(agents of_generic_species individu where(each.age<65 and each.age>18 and each.activity="Etudiant"));
	}
	
	action test_buildings{
		
		write "nb of buildings = "+length(agents of_generic_species building);
		write "nb of housings = "+length(agents of_generic_species housing);
		write "nb of companies = "+length(agents of_generic_species company);
		write "nb of schools = "+length(agents of_generic_species school);
		write "nb of factories = "+length(agents of_generic_species factory);
		write "nb of parks = "+length(agents of_generic_species park);
		write "nb of healthCenter = "+length(agents of_generic_species healthCenter);
		write "nb of funZone = "+length(agents of_generic_species funZone);
		assert agents of_generic_species housing all_match (each.numberResidents <= each.maxCapacity);
		assert agents of_generic_species housing all_match (each.numberResidents = length(each.residents));
		//TODO: there are empty houses, change that
		//assert agents of_generic_species housing all_match (each.numberResidents > 0);
		assert agents of_generic_species company all_match (each.numberEmployees <= max_number_employees);
		assert agents of_generic_species company all_match (each.numberEmployees = length(each.employees));
		//TODO: there are empty companies, change that
		//assert agents of_generic_species company all_match (each.numberEmployees > 0);
		assert agents of_generic_species healthCenter all_match (each.numberOfPatients <= maxPatientsHospitalCapacity);
		assert agents of_generic_species school all_match (each.numberOfStudents <= maxStudentsCapacity);
		
		/* 
		int currentNbOfResidentsAtHome <-   ( (agents of_generic_species individu) count (each overlaps each.house) ) ;
		int currentNbOfEmployeesAtWork <-   (( (agents of_generic_species individu) where (each.workPlace!= nil) ) count ( each overlaps each.workPlace) ) ;
		int nbOfPossibleResidents <- (agents of_generic_species housing) sum_of (each.maxCapacity);
		int nbOfPossibleEmployees <- (agents of_generic_species company) sum_of (each.maxNbEmployees);
		
		write "nbOfPossibleResidents = "+nbOfPossibleResidents;
		write "currentNbOfResidentsAtHome ="+currentNbOfResidentsAtHome;
		write "density of people in housing = "+currentNbOfResidentsAtHome/nbOfPossibleResidents;
		write "nbOfPossibleEmployees = "+nbOfPossibleEmployees;
		write "currentNbOfEmployeesAtWork ="+currentNbOfEmployeesAtWork;
		write "density of people in company = "+currentNbOfEmployeesAtWork/nbOfPossibleEmployees;
		*/
	}
	
	action test_initialisation_buildings_of_MiniCities {
		// Tests initialisation individus
		loop minicity over: agents of_generic_species MiniCity {
			int nb_tS <- minicity.l_buildings count (string(type_of(each)) = "trainStation");
			assert (nb_tS) = 1; // a minicity have to have a trainStation
		}
		assert length(agents of_generic_species individu) = nb_agent_init;
	}
}

experiment test_env type: gui {    
	
    output {
	    
	    layout horizontal([vertical([0::6721,3::3279])::5000,vertical([horizontal([1::5000,2::5000])::5000,horizontal([4::5000,5::5000])::5000])::5000]) tabs:false toolbars:false;
		
		//display map visualisation
	 	display farms_display type: opengl {
	    	image "fronteers" gis: "../includes/california_fronteers.shp" color: rgb('white');
	    	species mountains aspect: base;
	    	species forest aspect: base transparency: 0.65;
	    	species desert aspect: base transparency: 0.65;
	    	species river aspect: base;
	        species city aspect: base;
	        species MiniCity;
	        species BigCity transparency: 0.7;
	        species BigCity aspect: pt;
	        species geothermal aspect: base;
	        species nuclear aspect: base;
	        species hydroelectric aspect: base;
	        species city_edge aspect:base;
	        species farm_cell aspect: base transparency: 0.75;
	        grid farm_cell border: #black transparency: 0.75;
	        event mouse_up action: cell_management;
	        species truck aspect: triangle;
	    }
	    
	     //display the action buttons
		display farms_charts background:#white refresh: every(1 #day){
			species button aspect: base transparency: 0.65 size: {1,0.2} position: {0, 0};
			event mouse_down action:activate_button;    
			chart "Water consumption" size: {1,0.4} position: {0, 0.2} type: series
			{
                data "Water" value: water_consommation color: #blue;
            }
            
            chart "Tree quantity" size: {1,0.4} position: {0, 0.6} type: series
            {
                data "Thousands of Trees" value: first(ForestCamp).nb_Trees /10 color: #green;
            }
		}
		
		display transports_charts refresh: every(12 #hours){
	        chart "Co2_trucks" type: series size: {1, 0.4} position: {0, 0} {
	     	   data "truckCo2_emitted" value: (truckCo2_emitted) style: line color: #black ;
	        }
	   	
	        chart "Energy_trucks" type: series size: {1, 0.4} position: {0, 0.25} {
	     	   data "truckEnergy_used" value: (truckEnergy_used) style: line color: #green ;
	        }
       		
       
	        chart "Co2_trains" type: series size: {1, 0.4} position: {0, 0.5} {
	     	   data "trainCo2_emitted" value: (TrainCo2_emitted) style: line color: #black ;
	        }
	   		
	   		
	        chart "Energy_trains" type: series size: {1, 0.4} position: {0, 0.75} {
	     	   data "trainEnergy_used" value: (TrainEnergy_used) style: line color: #green ;
	        }
	       	
			//species ecosystime aspect: base size: {0.2,0.2} position: {0, 0.8};
			//species ecosyseason aspect: base size: {0.2,0.2} position: {0.5, 0.8};
	    }
		display energy_charts refresh: every(1 #cycle){
			chart "Energie Production" size: {0.33,0.9} position: {0, 0} type: series {
                data "production" value: tot_prod color: #green;
            }
            chart "Energy consumption" size: {0.33,0.9} position: {0.33, 0} type: series {
                data "Consumption" value: total_consumption color:  #red;
            }
            chart "Différence d'énergie" size: {0.33,0.9} position: {0.66, 0} type: series {
                data "la difference" value: tot_prod - total_consumption color:  #red;
            }
		}
		
		display dwelling_charts refresh: every(2 #hours){
			chart "density of people in housings" size: {1,0.5} position: {0, 0} type: series
			{
				data "density in %" value: densityOfPeopleInTheirHouse color:  #lightblue;
            }
            
            chart "density of people at their work" size: {1,0.5} position: {0, 0.5} type: series
            {
            	
            	data "density in %" value: densityOfPeopleInTheirWorkPlace color:  #darkblue;
            }
        }
		
		display ghg_biomass_charts refresh: every(1 #day){
			chart "Biomass waste" size: {1,0.5} position: {0, 0} type:series
			{
				data "kg" value: biomass_waste color:#green;
			}
			chart "Greenhouse emissions" size: {1,0.5} position: {0, 0.5} type:series
			{
				data "kg" value: greenhouse_emissions color:#red;
			}
		}

    }

}
