/**
* Name: mainlogs
* Based on the internal empty template. 
* Author: Marius
* Tags: 
*/


model mainlogs
import "california_environment.gaml"
import "../general/individual.gaml"
import "../general/mini_city.gaml"
import "../general/parameters.gaml"
import "../Ecosystem/farms.gaml"
import "../Ecosystem/forest.gaml"

/* Insert your model definition here */

global {
	
	//LOGS PARAMS
	float time_logs <- 1#week;
	string general_logs_file_name <-"logs.txt";
	string demograhic_logs_file_name <- "demographic_logs.txt";
	int nb_log <- 0;
	
	

	reflex calculate_conso {
	    meat_water_conso <- total_consommation[0]*animal_consommation_water;
	    meat_ghg_conso <- total_consommation[0]*animal_production_ghg;
	    meat_waste_conso <- total_consommation[0]*animal_production_waste;
	    
		crop_water_conso <- total_consommation[1]*crop_consommation_water;
		crop_ghg_conso <- total_consommation[1]*crop_production_ghg;
		crop_waste_conso <- total_consommation[1]*crop_conso_waste;
		
		tot_water_conso <- meat_water_conso + crop_water_conso;
		tot_ghg_conso <- meat_ghg_conso + crop_ghg_conso;
		tot_waste_conso <- meat_waste_conso + crop_waste_conso;
		
//		write "ghg";
//		write tot_ghg_conso;
//		write meat_ghg_conso;
//		write crop_ghg_conso;
//		
//		write "waste";
//		write tot_waste_conso;
//		write meat_waste_conso;
//		write crop_waste_conso;
//		
//		write "water";
//		write tot_water_conso;
//		write meat_water_conso;
//		write crop_water_conso;
	}
	
	// LOG RELFEXS
	reflex log when: every (time_logs) {
//		// check file
//		if not file_exists(general_logs_file_name){
//	    	write "ERROR : log file not found";
//	    }
	    
	    // demographic logs
	    int scolarise <- individu count (each.state = "state_scolarise" or ( each.state="state_malade" and each.workPlace != nil and [each.workPlace] of_generic_species school and each.age<18));
	    int inactifs <- individu count (each.state = "state_inactif" or ( each.state="state_malade" and each.workPlace = nil));
	    int actifs <- individu count (each.state = "state_actif" or ( each.state="state_malade" and each.workPlace != nil and [each.workPlace] of_generic_species company and each.age>18 ));
	    int etudiants <- individu count (each.state = "state_etudiant" or ( each.state="state_malade" and each.workPlace != nil and [each.workPlace] of_generic_species school and each.age>18));
	    int malades <- individu count (each.state = "state_malade");
	    int hommes <- individu count (each.gender = "Homme");
	    int femmes <- individu count (each.gender = "Femme");
	    save ""+scolarise+";"+inactifs+";"+actifs+";"+etudiants+";"+malades+";"+hommes+";"+femmes to:demograhic_logs_file_name type: "text" rewrite: false;
	    
	    // general logs
	    save ""+(time_logs*nb_log)/(1#day)+";"+greenhouse_emissions+";"+tot_water_conso+";"+tot_ghg_conso+";"+tot_waste_conso+";"+biomass_waste+";"+meat_water_conso+";"+crop_water_conso+";"+meat_ghg_conso+";"+crop_ghg_conso+";"+meat_waste_conso+";"+crop_waste_conso to: general_logs_file_name type: "text" rewrite: false;
	    
	    nb_log <- nb_log + 1;
	}
	
	reflex save_gridd when: hour_day = 2 {
		do save_grid;
	}
	
	
	//energy for transport
	reflex update_energy_transport{
		ask ElectricTransportation at 0 {
    		conso_totale <- (truckEnergy_used - last)/1000;
    		last <- truckEnergy_used;
    	}
    	ask ElectricTransportation at 1 {
    		conso_totale <- (TrainEnergy_used - last)/1000;
    		last <- TrainEnergy_used;
    	}	
	}
	
	
	
	
	init {
		do load_grid;
        
		// INITIAL RUN PARAMS LOGS
		save "scolarise ; inactifs ; actifs ; etudiants ; malades ; hommes ; femmes" to:demograhic_logs_file_name type: "text" rewrite: true;
		save "Time_since_start ; greenhouse_emissions ; biomass_waste ; tot_water_conso ; tot_ghg_conso ; tot_waste_conso ; meat_water_conso ; crop_water_conso ; meat_ghg_conso ; crop_ghg_conso ; meat_waste_conso ; crop_waste_conso" to:general_logs_file_name type: "text" rewrite: true;
		
		do instanciate_MiniCities;
		
		write "--------------------------------\n\tTEST INITIALISATION\n--------------------------------";
		
		do test_initialisation_MiniCities;
		
		do test_initialisation_inidividus_of_MiniCities;
		
		do test_initialisation_buildings_of_MiniCities;
		
		do test_demography;
		
		//creat two energy transtport
		//0 -> marchandise
		//1 -> personne
		create ElectricTransportation number: 2;
		
		//transport
		
		trainNetwork <- createTrainGraph(file("../includes/california_cities.shp"));
		
		create MerchandiseManager;
		
		create truck number: nb_truck {
			
     		location <- point(1 among city);//todo in V2 reapartir les camions sur les differents champs de manière équitable 
	   						  // et leurs postition initiale sera la postion du champ
	 
	
     	}
	}
	
	action load_grid {
		file my_csv_file <- csv_file("grid.csv",",");
        matrix data <- matrix(my_csv_file);
        ask farm_cell {
            image <- int(data[grid_x,grid_y]);
            switch int(data[grid_x,grid_y]) {
				match 0 {color <- #red; quantity_available <- meat;}
				match 1 {color <- #green; quantity_available <- crop;}
				match 2 {color <- #blue; quantity_available <- cotton;}
				match 3 {color <- #white; image <- -1;}
			}
        }
	}
	
	action test_initialisation_MiniCities {
		// Calculating the "constellationPopulation" and the length of the "list_minicities_constellation" attributes for every BigCity
		// the first one is supposed to be equals to nb_agent_init, the second one to nb_minicity_init.
		int poptotBC <- 0;
		int nb_mc_BC <- 0;
		loop bigcity over: BigCity {
			poptotBC <- poptotBC + bigcity.constellationPopulation;
			nb_mc_BC <- nb_mc_BC + length(bigcity.list_minicities_constellation);
		}
		
		// Calculating the "population" attribute for every BigCity
		// the first one is supposed to be equals to nb_agent_init, the second one to nb_minicity_init. 
		int poptotMC <- 0;
		loop minicity over: agents of_generic_species MiniCity {
			poptotMC <- poptotMC + minicity.population;
//			write minicity.l_buildings;
		}

		// Prints to see the initialisation's stats
		write "nb_minicity_init = " + nb_minicity_init;
		write "nb MiniCity instanciées = " + length(agents of_generic_species MiniCity);
		write "sum list(MiniCity) des cities = " + nb_mc_BC;
		write "nb_agent_init = " + nb_agent_init;
		write "sum BigCity.pop  = " + poptotBC; // doesn't work
		write "sum MiniCity.pop = " + poptotMC;
		write "nb individus instanciées = " + length(agents of_generic_species individu);
		// Tests initialisation mini-villes
		assert length(agents of_generic_species MiniCity) = nb_minicity_init;
		assert nb_mc_BC = nb_minicity_init;
		assert poptotBC = nb_agent_init;
		assert poptotMC = nb_agent_init;
	}
	
	action save_grid {
		string grid_file_name <-"grid.csv";
		int current_x <- 0;
		int current_y <- 0;
		string elem <- "";
		ask farm_cell {
			elem <- elem+self.image+",";
			if grid_x = grid_size-1 {
				save elem to:grid_file_name type: "text" rewrite: false;
				elem <- "";
				current_y <- current_y+1;
			}
		}
	}
	
	action test_initialisation_inidividus_of_MiniCities {
		// Tests initialisation individus
		assert length(agents of_generic_species individu) = nb_agent_init;
	}
	
	action test_demography{
		write "nb individus instanciées = " + length(agents of_generic_species individu);
		write "nb individus à plus de 65 ans = " +  length(agents of_generic_species individu where(each.age>=65));
		write "nb individus inactif à plus de 65 ans = " +  length(agents of_generic_species individu where(each.age>=65 and each.activity="Inactif"));
	    write "nb individus à moins de 18 ans = " +  length(agents of_generic_species individu where(each.age<=18));
	    write "nb individus scolarise à moins de 18 ans = " +  length(agents of_generic_species individu where(each.age<=18 and each.activity="Scolarise"));
	    write "nb individus entre 18 et 65 ans = " +  length(agents of_generic_species individu where(each.age<65 and each.age>18));
		write "nb individus inactif entre 18 et 65 ans = " +  length(agents of_generic_species individu where(each.age<65 and each.age>18 and each.activity="Inactif"));
		write "nb individus actif entre 18 et 65 ans = " +  length(agents of_generic_species individu where(each.age<65 and each.age>18 and each.activity="Actif"));
		write "nb individus etudiant entre 18 et 65 ans = " +  length(agents of_generic_species individu where(each.age<65 and each.age>18 and each.activity="Etudiant"));
	}
	
	action test_initialisation_buildings_of_MiniCities {
		// Tests initialisation individus
		loop minicity over: agents of_generic_species MiniCity {
			int nb_tS <- minicity.l_buildings count (string(type_of(each)) = "trainStation");
			assert (nb_tS) = 1; // a minicity have to have a trainStation
		}
		assert length(agents of_generic_species individu) = nb_agent_init;
	}
}

experiment SAVE_GRID type: gui {    
	
    output {
	    
//	    layout horizontal([vertical([horizontal([0::6721,3::3279])::5000, horizontal([0::6721,3::3279])::5000])::5000,vertical([1::5000,2::5000])::5000]) tabs:false toolbars:false;
		layout horizontal([vertical([0::9000,1::1000])::5000, 2::5000]);
		
		//display map visualisation
	 	display farms_display type: opengl {
	    	image "fronteers" gis: "../includes/california_fronteers.shp" color: rgb('white');
	    	species mountains aspect: base;
	    	species forest aspect: base transparency: 0.65;
	    	species desert aspect: base transparency: 0.65;
	    	species river aspect: base;
	        species city aspect: base;
	        species MiniCity;
	        species BigCity transparency: 0.7;
	        species BigCity aspect: pt;
	        species geothermal aspect: base;
	        species nuclear aspect: base;
	        species hydroelectric aspect: base;
	        species city_edge aspect:base;
	        species farm_cell aspect: base transparency: 0.75;
	        grid farm_cell border: #black transparency: 0.75;
	        event mouse_up action: cell_management;
	        species truck aspect: triangle;
	    }
	    
	    //display the action buttons
		display buttons background:#white refresh: every(1 #day){
			species button aspect: base transparency: 0.65 size: {1,0.2};
			event mouse_down action:activate_button;
		}

		display ghg refresh: every(1 #day){
			chart "Ghg consumption" size: {1,0.33} position: {0, 0} type:series y_log_scale:false
			{
				data "Ghg production" value: greenhouse_emissions color:#black;
				data "Ghg conso" value: tot_ghg_conso color:#blue;
				data "Ghg conso meat" value: meat_ghg_conso color:#red;
				data "Ghg conso crop" value: crop_ghg_conso color:#green;
			}
			
			chart "Water consumption" size: {1,0.33} position: {0, 0.33} type:series y_log_scale:false
			{
//				data "Water production" value: water_consommation color:#black;
				data "Water conso" value: tot_water_conso color:#blue;
				data "Water conso meat" value: meat_water_conso color:#red;
				data "Water conso crop" value: crop_water_conso color:#green;
			}
			
			chart "Waste consumption" size: {1,0.33} position: {0, 0.66} type:series y_log_scale:false
			{
				data "Waste production" value: biomass_waste color:#black;
				data "Waste conso" value: tot_waste_conso color:#blue;
				data "Waste conso meat" value: meat_waste_conso color:#red;
				data "Waste conso crop" value: crop_waste_conso color:#green;
			}
		}

    }

}

