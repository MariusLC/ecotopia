/**
* Name: InitTest
* Based on the internal empty template. 
* Author: damien
* Tags: 
*/


model InitTest


import "building.gaml"


global {
	geometry shape <- rectangle(1200#m,1200#m); //shape of the world ( for display)
	
	init 
	{

		// CREATING TWO MINI-CITIES AND MAKING SURE THEY DONT COLLIDE
		create MiniCity number: 2 returns:mcl {
//			location <- minicity_location;
			diameter <- mean_diameter_minicity;
//			population <- pop; 
//			availability_matrix_minicity <- grid_minicity[0];
//			availability_matrix_minicity_size <- grid_minicity[1];
//			resourceStock <- Rstock;
		}
		
		MiniCity mc1<- mcl[0];
		 
		MiniCity mc2<- mcl[1];
		
		ask mc2 //making sure mc2 dont collide with m1
		{
			if ( (mc1 inter mc2)!=nil )
			{
				loop while: ( (mc1 inter mc2)!=nil )
				{
					do moveMiniCity(mc2.location-{100,100}); // just moving it up and left (arbitrary choice)
				}
			} 
		}
		
		//init buildings
		map<string,int> initMap <- [woodenHouse::3,'park'::2];
		
		ask mc1
		{
			do initMiniCityBuildingsFromMap(initMap);
		}
	
	
		ask mc2
		{
			//here initMatrix is directly given to the method
			do initMiniCityBuildingsFromParameters;
		}
		
	}
}	
		
experiment main type: gui {
	parameter "Max capacity of residents per apartment/house" var: maxCap min: 7 max: 25;
	parameter "Min capacity of residents per apartment/house" var: minCap min: 1 max: 7;
	parameter "Percentage of wooden houses compared to modular houses" var: percentWoodenHouse min: 0.0 max: 1.0 ;
	parameter "Percentage of businesses compared to other companies" var: percentBusiness min: 0.0 max: 1.0;
	parameter "Percentage of fun zones compared to other companies" var: percentFunZone min: 0.0 max: 1.0;
	parameter "Percentage of factories compared to other companies" var: percentPlasticFactory min: 0.0 max: 1.0;
	parameter "Percentage of schools compared to other companies" var: percentSchool min: 0.0 max: 1.0;
	parameter "Percentage of health centers compared to other companies" var: percentHealthCenter min: 0.0 max: 1.0;
	parameter "Number of solar panels per wooden house" var: nb_solar min: 0 max: 10; // Temporary value
	parameter "Number of eolians per wooden house" var: nb_eolian min: 0 max: 3; // Temporary value
	parameter "Biowaste production per individual per day in kg" var: biowaste_per_individual min: 1.2 max: 2.0; // kg Temporary value
	parameter "GES/kg of bioplastic produced by plastic factories" var: factory_ges_emission min: 1.0 max: 1.7; // Temporary value
	parameter "plastic_consumption_per_house" var:  plastic_consumption_per_house min:0.0;
	parameter "Number of buildings per park" var: nb_of_buildings_per_park min:1 ; // a park is built every nb_of_buildings_per_park at miniCity init
	parameter " Upper bound for the number of employees in a company " var:max_number_employees min:0;
	
    output {
    	inspect "miniCities_inspector" value: MiniCity type: table;
    	monitor "Co2 emitted" value: greenhouse_emissions;

    	display MiniCities background: #lightblue type:opengl 
    	{
    		species MiniCity;
    		//species agents of_generic_species housing;
    		species modularHouse;
    		species woodenHouse;
    		species trainStation;
    		species school;
    		species business;
    		species park;
    		species healthCenter;
    		species funZone;
    		species plasticFactory;
    	}
    }
}
