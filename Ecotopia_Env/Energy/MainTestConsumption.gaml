/**
* Name: MainTest
* Based on the internal empty template. 
* Author: ubuntu
* Tags: 
*/


model MainTestConsumption
import "skeleton.gaml"

global {
	ElectricalGrid grid;
	float provided_energy <-7000.0;
	
	init{
		create TestElectricalGrid returns: eg{
			
		}
		create TestBuildingEnergy number: 1{
	    create buildingtest returns: build{
			nb_pers <- 5;
		}
		myBuilding <- build at 0;
        cons_1_pers  <- 6.0;
        area <- 60 #m2; 
    	lighting <- 10.0;
    	whRequired_m3_1degree <- GwhRequired_m3_1degree ; 
    	consomachine <-4.0 ;
    	
	}
        create TestEnergyPlugin number: 1 
        {
        has_energy <-false;
//        ext_temp <- 40.0;
        }
        
        create TestElectricTransportation number: 1 {
        	tot_km <- 10.0;
        	conso_km <-5.0;
        }
        
}
}
species TestEnergyPlugin parent: EnergyPlugin{
  
  
}

species TestElectricalGrid parent: ElectricalGrid
{
	list<float> demand_energy(EnergyPlugin d) {
		return [provided_energy, d.energy_consumption()];
	}
	
}
/*    	
species TestBuildingEnergy parent: BuildingEnergy {

     reflex energy_consumption{
     invoke consumption;
	write ("energie_maison => " +super.energy_consumption());
	write ("ask for energy building =>" +ask_for_energy());
	  }
	 }

species TestElectricTransportation parent: ElectricTransportation{
	reflex energy {
     invoke consumption;
	write ("energie_transport => " +super.energy_consumption());
	write ("ask for energy  transport=>" +ask_for_energy());
			
	}	
}*/

species buildingtest parent:building{
	int nb_pers;
	
	int getNbPeople{
		return nb_pers;
	}
}

species TestBuildingEnergy parent: BuildingEnergy {
    ElectricalGrid grid;
    reflex energy_consumption{
    invoke energy_consumption;
    write ("energie_maison => " +super.energy_consumption());
    write ("ask for energy building =>" +super.ask_for_energy());
      }
     }

species TestElectricTransportation parent: ElectricTransportation{
    ElectricalGrid grid ;
    reflex energy_consumption {
    invoke energy_consumption;
    write ("energie_transport => " +super.energy_consumption());
    write ("ask for energy  transport=>" +super.ask_for_energy());

    }
}
	
experiment  "Try it";

