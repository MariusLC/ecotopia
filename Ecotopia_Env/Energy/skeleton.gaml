/**
* Name: Energy
* Based on the internal empty template. 
* Author: ubuntu
* Tags: 
*/


model Energy

/* Insert your model definition here */
import "../dwelling/building.gaml"
import "../Ecosystem/ecosystem.gaml"
import "../Ecosystem/weather.gaml"
import "../general/parameters.gaml"


global {
	ElectricalGrid eg;
	init{
		create ElectricalGrid{
		}	
		eg <- ElectricalGrid at 0;
	}
	float tot_water <- 0.0;
	float tot_prod <- 0.0;
	float tot_prod_weekly <- 0.0;
	float total_consumption <-0.0;
	float tot_conso_weekly <- 0.0;
	float conso_buildings <-0.0;
	float conso_plastic <-0.0;
	

	reflex tot{
		tot_conso_weekly <- tot_conso_weekly + total_consumption;
		tot_prod_weekly <- tot_prod_weekly + tot_prod;
		tot_prod <- 0.0;
		total_consumption <-0.0;
	}
	  

}

species EnergyPlugin {
	float x; // pourrait etre inutile
	float y; 
	
    ecosystem ecosystem_var;
	
	float co2_tick;
	float methane_tick;
//	float cons_basis <- Gmin_conso;
//	float prod_basis;
	float ideal_temp <- Gideal_temp;
	bool has_energy ;
//	float ext_temp function: {temperature of weather_cell(self.location)}; // teméprature extérieure on la récupére de l'écosystème change en fonction du temps
	
	int last_prod_tick <- -1;
	float total_prod;
//	float sunshine function: {sun of weather_cell(self.location)}; // ensoleillement max
	//float wind_force; //km/h
	float co2_wh_eolian;
	float co2_wh_solar;
	float methane_wh_eolian;
	float methane_wh_solar;
	int tick;
	ElectricalGrid grid;
	
	
	
	float production <- 0.0; //prod du tock courant
		
	int nbSolarPanel_Eolian (int a, int b){
		//pour une surface de 26m2 on a 16 panneaux de 280 Wc, donc 1,6m2 le panneau 
		int surface <- a*b;
		int panel <- int ((16*surface) / 26);

		return int(panel/2);
	}

	
	float energy_production {return 0.0;}
	float energy_consumption {return 0.0;}

	float take (float quantity) {
		float res <- min(quantity, production);
		production <- production - res;
		return res;
	}



	float convert_KW_KWh (float energy, float nb_hour) {
		float convert;
		convert <- energy * nb_hour;
		return convert;
	}
	
	bool ask_for_energy {
		return has_energy;
	}
	action update_energy ( bool b) { 
		has_energy <- b;
	}
	action heating {}
	
}

species PlantEnergy parent: EnergyPlugin {
	int age;
	int life_duration;
	float co2_per_wh;
	float methane_per_wh;
	reflex generate_ghg {}
	reflex get_older {}
	// La génération de ghg dépend de la production électrique et donc du type de centrale
	// Impossible de définir cette génération ici
	// Ce fut une erreur de mettre generate_ghg en reflex
	// Qui plus est, la génération se fait lors de la production électrique...
//    reflex generate_ghg {
//        ecosystem.add(self.co2_tick, self.methane_tick)
//    }
}


species NuclearPlant parent: PlantEnergy schedules: [] {
//	float water_per_kWh <- Gwater_per_kWh_nuclear;	
	float waterDemand <- 0.0;
	float const_prod <- Gconst_nuclear;
	
	init {
		self.co2_per_wh <- Gco2_wh_nuclear;
		self.methane_per_wh <- Gmethane_wh_nuclear;
		
	}
	
	float energy_production {
//		write self.name + " Produces energy";
		if (last_prod_tick < cycle){
   			last_prod_tick <-cycle;
   			
   			production <- Gconst_nuclear;
   			tot_prod <- tot_prod+production;
   			water_consommation <- water_consommation + Gwater_per_kWh_nuclear * production;
   			tot_water <- tot_water + Gwater_per_kWh_nuclear * production;
//   			write "tot_water " + tot_water;
//   			write "water per kWh " + Gwater_per_kWh_nuclear;
			/* GES */
			float _co2_per_wh <- co2_per_wh;
			float _curr_prod <- production;
			float _methane_per_wh <- methane_per_wh;
			greenhouse_emissions <- greenhouse_emissions + _co2_per_wh * _curr_prod + _methane_per_wh * _curr_prod;
			energy_ghg_emissions <- energy_ghg_emissions +_co2_per_wh * _curr_prod + _methane_per_wh * _curr_prod;
			greenhouse_emissions_produced <- greenhouse_emissions_produced + _co2_per_wh * _curr_prod + _methane_per_wh * _curr_prod;
			
			/*  */
		}
		return production;
	}
}

species GeoTherm parent: PlantEnergy schedules: [] {

	init {
		self.co2_per_wh <- Gco2_wh_geotherm;
		self.methane_per_wh <- Gmethane_wh_geotherm;
		
	}

	
	float energy_production {
		if (last_prod_tick < cycle){
   			last_prod_tick <-cycle;
   			production <- Gconst_geotherm;
   			tot_prod <- tot_prod+production;
   		}
		
		return production;
	}
	
}

species Dam parent: PlantEnergy schedules: [] {
	
	init {
		self.co2_per_wh <- Gco2_wh_dam;
		self.methane_per_wh <- Gmethane_wh_dam;
	}

		
	float energy_production {
		if (last_prod_tick < cycle){
   			last_prod_tick <-cycle;
   			
   			production <- 9.8 * Gheight_waterfall * Gflow;
   			tot_prod <- tot_prod+production;
   		}
		
		return production;
	}
}

species Constructer parent: EnergyPlugin {
    list Building_list; //liste des maisons en cours de construction
    action place_plant {}
    bool build_dam {return false;}
    bool build_solar {return false;}
    bool build_nuclear {return false;}
    bool build_geotherm {return false;}

}



species BuildingEnergy parent: EnergyPlugin {
    float cons_1_pers;
    int nb_solar;
    int nb_windmills;
    float area;
    float volume; 
    float lighting <- Glighting;
    float whRequired_m3_1degree <- GwhRequired_m3_1degree; 
    float consomachine;
    float min_conso <- Gmin_conso;
    //WaterDistributor water_distrib;
    
    
	float methane_wh_eolian <- Gmethane_wh_eolian;
	float co2_wh_eolian <- Gco2_wh_eolian;
	float methane_wh_solar <- Gmethane_wh_solar;
	float co2_wh_solar <- Gco2_wh_solar;
	building myBuilding;
	
	

    float energy_production {
    	float yield <- Gsolar_yield;
   		if (last_prod_tick < cycle){
   			last_prod_tick <- cycle;
   			//pour les eoliennes
   			float rho <-1.204; //kg/m3
   			float s <- 3.1416; //si taille pale=1m
   			float wind_force <- myBuilding.minicity.weath_cell.wind;
   			int solar_power <-myBuilding.minicity.weath_cell.sun;
   			
   			float eoliane_production <- (nb_windmills * (rho * s * 0.5 * wind_force / 3.6)) / 1000; //il manque force du vent
   			
   			//pour le solaire
			// 1 panel = 1.6 m2
   			float solar <- (yield * 1.6 * Gsolar_radiation * solar_power / 2) / 1000; //kWc
		    float prod_solar <- nb_solar * solar;
			//GES
			float co2_eolian <- eoliane_production * co2_wh_eolian;
			float co2_solar <- prod_solar * co2_wh_solar;
			
	    	float methane_eolian <- eoliane_production * methane_wh_eolian;
			float methane_solar <- prod_solar * methane_wh_solar;
			greenhouse_emissions <- greenhouse_emissions + co2_eolian + co2_solar + methane_eolian + methane_solar;
			greenhouse_emissions_produced <- greenhouse_emissions_produced + co2_eolian + co2_solar + methane_eolian + methane_solar;
			production <- eoliane_production + prod_solar;
			tot_prod <- tot_prod+production; 
			energy_ghg_emissions <- energy_ghg_emissions + co2_eolian + co2_solar + methane_eolian + methane_solar;
			
			return production;
		}
		return production;
	}
	
	
	reflex energy_consumption {
		do consumption;
	}
	
	action consumption {
    	list tmp <- eg.demand_energy(self);
    	float provided_energy <- tmp at 0; 
    	float demand_energy <- tmp at 1;
    	do update_energy(provided_energy >= demand_energy);
     }
      reflex energy_consumption {
       do consumption;
    }
    float energy_consumption {
    	float cons_tmp <- cons_1_pers;
    	if first(ecosystem).night =1{
				cons_tmp <- 0;
			}
    	float res <-max(min_conso,(cons_tmp * myBuilding.getNbPeople() + lighting+consomachine + heating()) * nb_people_per_agent);

    	if (string(type_of(myBuilding)) = "plasticFactory")
    	{
    		 conso_plastic <- conso_plastic+res;
    	}
    	else {
    		 conso_buildings <-conso_buildings+res;
    		}
    	total_consumption <- total_consumption+res;
    return(res);
    }
    
    float heating  {
    	return max([0, (ideal_temp - (temperature of weather_cell[int(myBuilding.location[0]), int(myBuilding.location[1])]))*whRequired_m3_1degree*volume]);
    }
    reflex generate_ghg {}
    
}

species SolarPlant parent: PlantEnergy schedules: [] {
    //int nb_solar ;
    float prod_constante;
    float yield <- Gsolar_yield;

	init {
		self.co2_per_wh <- Gco2_wh_solar;
		self.methane_per_wh <- Gmethane_wh_solar;
		
	}

    
    float energy_production {
    
    	//des panneaux solaires monocristallins, le rendement peut être de 16 % à 24 %.
    	if (last_prod_tick < cycle){
    		last_prod_tick <-cycle;
    		
    		//pour le solaire
   			//float solar <- yield * 1.6 * Gsolar_radiation * (sun of weather_cell[int(self.location[0]), int(self.location[1])]) ; //Wc
   			
   		
			//production <- nb_solar * solar;
			switch first(ecosystem).night {
				match 1 {production <-  0;}
				match 0 {production <-  yield * prod_constante;}
			}
			tot_prod <- tot_prod+production;
    	}
    	
    	return production;
    }

 }
    

species ElectricalGrid schedules: [] {
    list<EnergyPlugin> plants_list; 
    float loss_rate  <- Gloss_ratio;
    
    list<agent> get_all_instances(species<agent> spec) {
        return spec.population +  spec.subspecies accumulate (get_all_instances(each));
    }
    
    list<float> demand_energy (EnergyPlugin d) {
		//list<PlantEnergy> LPlants <- list<PlantEnergy> (PlantEnergy.subspecies accumulate(each.population));
		list<PlantEnergy> LPlants <- list<PlantEnergy> (get_all_instances(PlantEnergy));
		//mettre si la distance influ sur les perte en ligne
		//LPlants <- LPlants sort_by (each distance_to d.location);
		float needEner <- max(0, d.energy_consumption() - d.energy_production());
		float demanded_energy <- needEner;
		float totTake <- 0.0;
		float loss <- loss_rate;
		if length(LPlants) > 0{
			loop c from: 0 to: length(LPlants)-1 { 
				ask LPlants at c { 
					do energy_production; // ne fait rien si déjà lancé dans ce tour
					float lack <- needEner - totTake;
					if lack <= 0 {
						break;
					}
					// loss représente les pertes dûes au transport d’électricité
					totTake <- totTake + take(lack / (1.0 - loss)) * (1.0 - loss); 
				}
			}	
		}
		return [totTake, demanded_energy];

	}
}

species HospitalEnergy parent: BuildingEnergy {
}

species PlasticProdEnergy parent: BuildingEnergy {
}
species ForestCampEnergy parent: BuildingEnergy {
    }
 
species EnergieEcole parent: BuildingEnergy {
    reflex energy_consumption{
    	return(0.0);
    }
}

species CompanyEnergy parent: BuildingEnergy {
}


species ElectricTransportation parent: EnergyPlugin {
    float conso_km; 
    float conso_totale ;//si le transport nous donne cette consommation on l 
    float tot_km; // si on nous donne le nombre de km parcourru par tick 
    float last;
	    

    action consumption {
    	list tmp <- eg.demand_energy(self);
    	float provided_energy <- tmp at 0; 
    	float demand_energy <- tmp at 1;
    	do update_energy(provided_energy >= demand_energy);
    }
    
    reflex energy_consumption {
		do consumption;
	}

    float  energy_consumption {
    	//return conso_km*tot_km; // sinon return conso_totale
    	total_consumption <- total_consumption+conso_totale;
    	return conso_totale;
	}
}
