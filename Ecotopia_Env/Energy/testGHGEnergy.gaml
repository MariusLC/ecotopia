/**
* Name: testGHGEnergy
* Based on the internal empty template. 
* Author: bruce
* Tags: 
*/


model testGHGEnergy

import "skeleton.gaml"
import "../general/parameters.gaml"

/* Insert your model definition here */

global {
	float global_curr_prod <- 10.0;
	float global_methane_per_wh <- 4.0;
	float global_co2_per_wh <- 2.0;
	int nbNuclear <- 1;
	int nbHouse <- 1;
	float global_production_eolian <- 1.0;
	float global_production_solar <- 1.0;
	
	float g_methane_wh_eolian <- 0.0;
	float g_co2_wh_eolian <- 0.0;
	float g_methane_wh_solar <- 0.0;
	float g_co2_wh_solar <- 0.0;
	float loss <- 0.3;
	
	init {
		
		create ElectricalGrid returns: eg{
			loss_rate <- loss;
		}
		
		create NuclearPlantTest number: nbNuclear {
			co2_per_wh <- global_co2_per_wh;
			methane_per_wh <- global_methane_per_wh;
			production <- global_curr_prod;
		}
		create BuildingEnergyTest number: nbHouse {
			methane_wh_eolian <- g_methane_wh_eolian;
			co2_wh_eolian <- g_co2_wh_eolian;
			methane_wh_solar <- g_methane_wh_solar;
			co2_wh_solar <- g_co2_wh_solar;
		
		}
		
	}
	
}

species NuclearPlantTest parent: NuclearPlant {
	float energy_production {
//		curr_prod <- 1.0; // Partie production = Ghada
		/* GES */
		float _co2_per_wh <- co2_per_wh;
		float _curr_prod <- production;
		float _methane_per_wh <- methane_per_wh;
		write(self.name + ": ");
		write("\t _co2_per_wh " + _co2_per_wh);
		write("\t _methane_per_wh " + _methane_per_wh);
		write("\t _curr_prod " + _curr_prod);
		
		greenhouse_emissions <- greenhouse_emissions + _co2_per_wh +_methane_per_wh;
		/*ask ecosystem {
			do add_ghg(_co2_per_wh * _curr_prod, _methane_per_wh * _curr_prod);
		}*/
		/*  */
		
		return production;
	}
	reflex produce {
		write(self.name + " produces electricity!");
		float prod <- energy_production();
	}
}

species BuildingEnergyTest parent: BuildingEnergy {
    
	reflex produce {
		write(self.name + " produces electricity!");
		float prod <- energy_production();
	}

}



experiment test type: gui {
	
	parameter "global_curr_prod:" var: global_curr_prod category: "Nuclear";
	parameter "global_methane_per_wh:" var: global_methane_per_wh category: "Nuclear";
	parameter "global_co2_per_wh:" var: global_co2_per_wh category: "Nuclear";
	parameter "nbNuclear:" var: nbNuclear;
	parameter "nbHouse:" var: nbHouse;
	parameter "global_production_eolian:" var: global_production_eolian category: "House";
	parameter "global_production_solar:" var: global_production_solar category: "House";
	
	parameter "g_methane_wh_eolian:" var: g_methane_wh_eolian category: "House";
	parameter "g_co2_wh_eolian:" var: g_co2_wh_eolian category: "House";
	parameter "g_methane_wh_solar:" var: g_methane_wh_solar category: "House";
	parameter "g_co2_wh_solar:" var: g_co2_wh_solar category: "House";
	
	
}