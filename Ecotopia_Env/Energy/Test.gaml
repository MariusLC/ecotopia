/**
* Name: Test
* Based on the internal empty template. 
* Author: ghada
* Tags: 
*/




model Test
import "skeleton.gaml"

global{
  	float loss <- 0.3;
  	
	init{
		create ElectricalGrid returns: eg{
			loss_rate <- loss;
		}
		
		
		//Test
		create EnergyPlugin  returns:myEP{
				
//			sunshine <- 1000.0;
		}
		
		write ("Test " + (myEP at 0).energy_production());
		
		//geothermie
		create GeoTherm  returns:myES{
				
			
		}
		write ("Geotherm " + (myES at 0).energy_production());
		
		//nucleair
		create NuclearPlant  returns:myN;
		write ("Nucleair " + (myN at 0).energy_production());
		
		
		
		//solar plant
		create SolarPlant returns:mySP{
				
//			sunshine <- 1000.0;
		
			nb_solar <-3;
		}
		write ("Solar Plant " + (mySP at 0).energy_production());
		
		
		//hydro
		create Dam returns:myD{
			last_prod_tick <-0;		
		    flow <- 2400.0;
	
		}
		write ("Dam " + (myD at 0).energy_production());
		
		//building
		create BuildingEnergy returns:myB{
			last_prod_tick <-0;		
		    nb_solar <- 40;
	  
	   		nb_windmills <- 40;
//	   		sunshine <- 1000.0;
		}
		write ("Building " + (myB at 0).energy_production());
	}
	
	
}



/* Insert your model definition here */




experiment name  {	
	
	// Define parameters to explore here if necessary
	// parameter "My parameter" category: "My parameters" var: one_global_attribute;
	// Define a method of exploration
	// method exhaustive minimize: one_expression;
	// Define attributes, actions, a init section and behaviors if necessary
	// init { }

}