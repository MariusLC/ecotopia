/**
* Name: MainTest
* Based on the internal empty template. 
* Author: ubuntu
* Tags: 
*/
/*
 * Fichier test pour la distribution
 * 
 * Scénario : 1 plant 1 building
 * 
 */


model Testdistri

import "skeleton.gaml"

global {

	
	float return_take <- 1.0; 
	float loss <- 0.1 min:0.0 max:0.99; 
	int nbPlant <- 2;
	int nbBuilding <- 1;
	float nrg_consumption <- 2.5;
	float nrg_production <- 2.0;
	float nrg_production_build <- 0.0;
	
	init{
		create ElectricalGrid returns: eg{
			loss_rate <- loss;
		}
		create TestPlant number: nbPlant;
		create TestBuildingEnergy number: nbBuilding;
	}
	
	
}

species TestEnergyPlugin parent: EnergyPlugin{
	ElectricalGrid grid;
	float energy_production {
		write(self.name + " returns the production " + nrg_production);
		
		return nrg_production;
	}
	
	float energy_consumption {
		write(self.name + " returns the consumption " + nrg_consumption);
		
		return nrg_consumption;
	}
	// demand_energy est censé renvoyer un couple de valeurs
	bool ask_for_energy {
		list<float> energy_values <- grid.demand_energy(self);
		float provided_energy <- energy_values at 0;
		float demanded_energy <- energy_values at 1;
		write(self.name + " asks for energy " + demanded_energy + " and is given " + provided_energy);
		return provided_energy = demanded_energy;
	}
	
	reflex a_f_e_reflex {
		write(self.name + " asks for energy");
		bool enough <- ask_for_energy();
		
	}
	
}


species TestBuildingEnergy parent: TestEnergyPlugin {

	float energy_production {
		write(self.name + " returns the production (building) " + nrg_production_build);
		
		return nrg_production_build;
	}
}


species TestPlant parent: PlantEnergy {
	float take (float quantity) {
		if nrg_production >= quantity{
			write("Someone takes " + quantity + " from " + self.name);
			return quantity;	
		}
		write("Someone takes " + nrg_production + " from " + self.name);
		return nrg_production;
	}
	float energy_production {
		write(self.name + " returns the production " + nrg_production);
		return nrg_production;
		
	}
}

experiment test type: gui {
	
	parameter "nbPlant" var:nbPlant;
	parameter "nbBuilding" var:nbBuilding;
	parameter "nrg_consumption" var:nrg_consumption;
	parameter "nrg_production" var:nrg_production;
	parameter "nrg_production_build" var:nrg_production_build;
	
}

