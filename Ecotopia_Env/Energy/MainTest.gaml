/**
* Name: MainTest3
* Based on the internal empty template. 
* Author: ubuntu
* Tags: 
*/


model MainTest

/* Insert your model definition here */
import "skeleton.gaml"
import "../general/parameters.gaml"


global{
	float loss <-0.3;
//	float provided_energy <-7000.0;
	float sun <- 1000.0;
	float wind <- 100.0; 
	int nb_buildings <- 2;
	int nb_plants <- 1;
//	float g_curr_prod <- 10.0;
	float g_methane_per_wh <- 4.0;
	float g_co2_per_wh <- 2.0;
//	float g_production_eolian <- 1.0;
//	float g_production_solar <- 1.0;
	
	float g_methane_wh_eolian <- 0.0;
	float g_co2_wh_eolian <- 0.0;
	float g_methane_wh_solar <- 0.0;
	float g_co2_wh_solar <- 0.0;
	
	int g_nb_pers <- 3;
	float g_cons_1_pers <- 6.0;
	float g_area <- 60 #m2;
	float g_lighting <- 10.0;
	float g_energyRequired_m2_degree <- 5.0;
	float g_consomachine <- 4.0;
	int g_nb_solar <- 4;
	int g_nb_windmills <- 4;
	
	float g_const_prod <- 7400.0;
	
	reflex say_cycle {
		write "-=== " + cycle + " ===-";
	}
	
	init{
		create TestElectricalGrid returns: eg{
			loss_rate <- loss;
		}
		
		
		create TestBuildingEnergy number: nb_buildings {
			create buildingtest returns: build{
				nb_pers <- g_nb_pers;
			}
			myBuilding <- build at 0;
        	cons_1_pers  <- g_cons_1_pers;
        	area <- g_area;
    		lighting <- g_lighting;
    		whRequired_m3_1degree <- GwhRequired_m3_1degree;
    		consomachine <- g_consomachine ;
    		
		    nb_solar <- g_nb_solar;
	  		
	   		nb_windmills <- g_nb_windmills;
	   		
//	   		wind_force <- wind;
//	   		sunshine <- sun;
	   		
	   		methane_wh_eolian <- g_methane_wh_eolian;
			co2_wh_eolian <- g_co2_wh_eolian;
			methane_wh_solar <- g_methane_wh_solar;
			co2_wh_solar <- g_co2_wh_solar;
		}
    	
    	create TestNuclearPlant number: nb_plants {
    		const_prod <- g_const_prod;
    		co2_per_wh <- g_co2_per_wh;
			methane_per_wh <- g_methane_per_wh;
    	}
    	
	
	}
	
}

species buildingtest parent:building{
	int nb_pers;
	
	int getNbPeople{
		return nb_pers;
	}
}

species TestBuildingEnergy parent: BuildingEnergy {

	int nbSolarPanel_Eolian (int a, int b){
		int res <- super.nbSolarPanel_Eolian(a, b);
		write "[nbSolarPanel_Eolian] " + name + " computes the number of solar panels with a=" + a + " and b=" + b + " = " + res;
		return res;
	}


	float energy_production {
		float res <- super.energy_production();
		write "[energy_production] " + name + " produces " + res;
		return res;
	}

    reflex energy_consumption{
//		write "[reflex energy_consumption]" + self.name + " consumes";
		invoke consumption;
	}
	action consumption {
		write "[action consumption] " + name + " consumes";
		invoke consumption;
	}
	
	float energy_consumption {
		float res <- super.energy_consumption();
		write "[float energy_consumption] " + name + " has a consumption of " + res;
		return res;
	}
	float heating  {
		float res <- super.heating();
		write "[heating] " + name + " has a heating of " + res;
		return res;
	}
	reflex blackout when: not has_energy {
		write "[blackout] " + name + " does not have enough energy to function!";
	}
}


species TestElectricalGrid parent: ElectricalGrid{
	list<float> demand_energy (EnergyPlugin d) {
		write "[demand_energy]" + name + " is asked energy by " + d.name;		
		list<float> res <- super.demand_energy(d);
		write ("[demand_energy]" + d.name + " needs " + d.energy_consumption() + " wh");
		write ("[demand_energy]" + d.name + " produces " + d.energy_production() + " wh");
		write ("[demand_energy]" + d.name + " asks for " + res at 1 + " wh");
		write ("[demand_energy]" + d.name + " receives " + res at 0 + " wh");
		return res;
	}
//	list<agent> get_all_instances(species<agent> spec) {
//		list<agent> res <- super.get_all_instances(spec);
//		write "[get_all_instances] " + name + " lists all instances of " + spec + " : " + res;
//		return res;
//	}
}

species TestNuclearPlant parent: NuclearPlant {
	
	float take (float quantity) {
		float res <- super.take(quantity);
		write "[take] " + name + " is taken " + quantity + " and is left with " + production;
		return res;
	}
	bool ask_for_energy {
		bool res <- super.ask_for_energy();
		write "[ask_for_energy] " + name + " is asked if it has energy. Answer : " + res;
		return res;
	}
	action update_energy ( bool b) {
		write "[update_energy] " + name + "updates its energy with " + b; 
		invoke update_energy;
	}
	
	float energy_production {
		float res <- super.energy_production();
		write("[energy_production] " + name + " produces " + res);
		
		return res;
	}
}



experiment test type: gui {

	output {
		
	}
	
	float minimum_cycle_duration <- 1#s;

	parameter "Number of plants:" var: nb_plants;
	parameter "Number of buildings:" var: nb_buildings;

	parameter "Methane emitted per wh of the plant:" var: g_methane_per_wh category: "Plant";
	parameter "CO2 emitted per wh of the plant:" var: g_co2_per_wh category: "Plant";
	
	
	parameter "Number of solar panels per building:" var: g_nb_solar category: "Building";
	parameter "Number of windmills per building:" var: g_nb_windmills category: "Building";
	parameter "Number of persons per building:" var: g_nb_pers category: "Building";
	parameter "Consumption per person (wh):" var: g_cons_1_pers category: "Building";
	parameter "Surface area of buildings (m2):" var: g_area category: "Building";
	parameter "Consumption of lighting (wh):" var: g_lighting category: "Building";
	parameter "Energy Required to increase 1 m2 by 1 degree (wh):" var: g_energyRequired_m2_degree category: "Building";
	parameter "Energy consumption by machines of buildings (wh):" var: g_consomachine category: "Building";

	parameter "Methane emitted per wh by eolian:" var: g_methane_wh_eolian category: "Building";
	parameter "CO2 emitted per wh by eolian:" var: g_co2_wh_eolian category: "Building";
	parameter "Methane emitted per wh by solar:" var: g_methane_wh_solar category: "Building";
	parameter "CO2 emitted per wh by solar:" var: g_co2_wh_solar category: "Building";
	


}