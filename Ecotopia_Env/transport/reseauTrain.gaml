/**
* Name: reseauTrain
* Based on the internal empty template. 
* Author: Julien
* Tags: 
*/

model reseauTrain
import "../general/mini_city.gaml"
import "../general/parameters.gaml"

global{
	graph trainNetwork;
	int agentsOnTrain;
	list<pair<int,int>> edges<-[]; //edges contient les arretes finales du graphe
	float max_dist<- 100000.0; //distance maximale à laquelle toutes villes sont reliée par un train (en metre je suppose) 
	list<city_node> city_nodes;
	//fonction permettant de créer le graphe représentant le réseau ferré entre les villes en Californie données dans un shapefile
	//le réseau est en 2 construit en 2 fois : d'abord un arbre couvrant de poids minimum pour relier efficacement toutes les villes
	// puis on relie les villes proches les une des autres (parametre à ajuster).	
	graph createTrainGraph(file city_file){

		graph trainGraph <-spatial_graph([]);
		// on recupere les villes pour créer un graphe sur lequel on va appliquer l'algo de Kruskal pour génerer le réseau	
		list<point> nodes <- [];
		create city_location from: city_file;
		loop c over:city_location{
			add c.location to:nodes;
			trainGraph<- trainGraph add_node(c.location);
		}
		nb_cities <- length(list(city_location));
		//initialisation du Kruskal
		int nb_edges<-0;
		float cost <-0.0;
		pair min_ind<-0::0;
		float min <-#max_float;
		list<pair<int,int>> elim;
		graph testGraph <- copy(trainGraph);
		//déroulement du Kruskal
		loop while: (nb_edges<nb_cities-1){
			loop i from:1 to:nb_cities-1{
				loop j from:0 to:i-1{

					if(not ((i::j) in elim)){
						if((city[i] distance_to city[j])<min){
							min<-(city[i] distance_to city[j]);
							min_ind<-i::j;
							
							
						}
					}
				}
			}

			
			testGraph <- copy(trainGraph) add_edge (nodes at first(min_ind)::nodes at last(min_ind));

			if(nb_cycles(testGraph)=0){
				cost<-cost + city[first(min_ind)] distance_to city[last(min_ind)];
				nb_edges<-nb_edges+1;
				add min_ind to:elim;
				add min_ind to:edges;
				trainGraph <- trainGraph add_edge (nodes at first(min_ind)::nodes at last(min_ind));
				 
			}else{
				testGraph <- copy(trainGraph);
				add min_ind to:elim;
			}
			min<-#max_float;
			min_ind<-(0::0);
			
		}
		//on créer le graphe final des trains entre les grandes villes 
		//qui va contenir un arbre couvrant trouvé avec Kruskal et relier les villes proches et permettre un affichage sur la carte
		//création du réseau entre les constellations de mini-villes
		city_nodes<-[];
		int num_bigcity <- 0;
		loop bigCity over:BigCity{
			num_bigcity <- num_bigcity+1;
			create city_node  returns:bcn{
				city_node bc_node <- self;
				set location <- bigCity.location;
				set isBigCity <- true;
				set num <- num_bigcity;
				loop mc over:bigCity.list_minicities_constellation{
					if(MiniCity(mc)!=MiniCity(bc)){
						create city_node  returns:mcn{
							set isBigCity <- false;
							set bc <- bc_node;
							set location <- MiniCity(mc).location;
						
						}
						add mcn[0] to:self.list_mcn;
					}		
				}
			}
			add bcn[0] to:city_nodes;
		}
			write edges;
			return city_node[0].my_graph;
			
		
	}
	/**init{
		write "init";
		trainNetwork <- createTrainGraph(file("../includes/california_cities.shp"));
	}
	* 
	*/

		
}
//cette species est juste là pour récuperer la localisation des villes depuis le shapefile
species city_location{
	
}
//species des noeuds des villes dans le graphe féroviaire
species city_node parent:graph_node edge_species:city_edge{
	bool isBigCity;
	int num;
	list<city_node> list_mcn<-[];
	city_node bc;
	//fonction déterminant si un noeud est relié à un autre, la décision se fait en fonction du résultat du Kruskal et de la distance géographique entre les 2 villes
	 
	bool related_to(city_node other){
		if isBigCity{
			if(other.isBigCity = false){
				return(other in list_mcn);
			}
			else{
				return ((num::other.num) in edges) or ((other.num::num) in edges)or (topology(world) distance_between [self.location, other.location] < max_dist);
			}
		}
		else{

			return (other = bc);
		}
	}
	//affichage des villes sous la forme de cercles bleus
	aspect base{
		draw circle(3#px) color:#blue;
	}
}


//species des arretes entre les villes, elles contiennent la distance entre les villes ainsi que la capacité de l'arrete courante et maximale
species city_edge parent:graph_edge{
	float distance<-(self.target.location distance_to self.source.location);
	int max_capacity<-max_cap;
	int currCapacity<-0;
		
	int capacityHour<-0;

	reflex razCapacityHour{
		capacityHour <- currCapacity;
	}
	aspect base{
		if(currCapacity = 0){
			draw shape color: #black;
			}
		else if(capacityHour < max_capacity){
		    draw shape color: #green;
			
		}
		else{
			draw shape color: #red;
		}
	}
}

