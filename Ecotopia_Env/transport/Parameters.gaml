/**
* Name: Parameters
* Based on the internal empty template. 
* Author: ced
* Tags: 
*/


model Parameters

/* Insert your model definition here */

global{
	//-------------------------------------------------------------My Parameters----------------------------------------------------------------------------------
	
	// Parameters for individuals
	float  active_rate <- 0.4453 min: 0 max: 1 parameter: "Taux d'actif: " category: "Individu"; 
	float  student_rate <- 0.0513 min: 0 max: 1 parameter: "Taux d'étudiant: " category: "Individu"; 
	float  inactive_rate <- 0.036 min: 0 max: 1 parameter: "Taux d'inactif: " category: "Individu"; 
	float  Age_0_18_rate <- 0.225 min: 0 max: 1 parameter: "Taux d'individu ayant moins de 18 ans: " category: "Individu";
	float  Age_18_64_rate <- 0.627 min: 0 max: 1 parameter: "Taux d'individu entre 18 et 64 ans: " category: "Individu";
	float  Age_65_rate <- 0.148 min: 0 max: 1 parameter: "Taux d'individu entre 18 et 64 ans: " category: "Individu";
	float  Women_rate <- 0.503 min: 0 max: 1 parameter: "Taux d'homme: " category: "Individu";
	float  Men_rate <- 0.497 min: 0 max: 1 parameter: "Taux de femme: " category: "Individu";
	
	//Time offset for when to start the day
	int time_offset <- 6 parameter: "Start Time:" category: "Initial";
	
	//Whether or not to stop the simulation after a certain number of days
	bool stop_simulation <- true parameter: "Stop Simulation: " category: "Initial";
	
	//The number of days to stop the simulation after
	int stop_sim_day <- 1 parameter: "The number of days to stop the simulation after: " category: "Initial";
}