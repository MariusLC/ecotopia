/**
* Name: TransportMarchandise
* Based on the internal empty template. 
* Author: Roza & Smail
* Tags: 
*/


model TransportMarchandise
import "../models/california_environment.gaml"
import "../general/mini_city.gaml"
import "../Ecosystem/farms.gaml"
import "../Ecosystem/forest.gaml"
import "../general/parameters.gaml"



/* Insert your model definition here */
global{
	
	list<point>  woodFields;

	//Trucks***********************************
	float truckSpeed <- 90 #km/ #h;
	float truckCapacity ;
	float truckEnergy_used ;
	float truckEnergy <- 0.7 ; // #kW / #km
	int nb_truck <- 10000;
	float MAX_CAPACITY <- 40 #ton;
    //trucks ************************************


	file shape_file_buildings <- file("../includes/building.shp");
    file shape_file_roads <- file("../includes/road.shp");
    file shape_file_bounds <- file("../includes/bounds.shp");
     
    int nb_people <-100;
    graph the_graph;
    int repair_time <- 2 ;
    float destroy <- 0.02; 
    float min_speed <- 1.0 #km / #h;
    float max_speed <- 5.0 #km / #h; 
    list<building> residential_buildings ;
    list<building> industrial_buildings ;

	
	/**init {
	    
	    
	    create mountains from: shape_file_mountains;
    	create forest from: shape_file_forests;
    	create desert from: shape_file_desert;
    	create city from: shape_file_cities with: [name:: read('NAME')];
    	create geothermal from: shape_file_geothermal;
    	create nuclear from: shape_file_nuclear;
    	create hydroelectric from: shape_file_hydroelec;
    	create river from: shape_file_rivers;

	
	truckCo2_emitted <-0.0;
	truckEnergy_used <-0.0;
		
    create building from: shape_file_buildings with: [type::string(read ("NATURE"))] {
        if type="Industrial" {
        color <- #blue ;
        }
    }

    
    //Mechandise **************************************************************
    create truck number: nb_truck {
     	location <- point(1 among city);//todo in V2 reapartir les camions sur les differents champs de manière équitable 
	   						  // et leurs postition initiale sera la postion du champ
	 
	
     }
     
    create MerchandiseManager ;

    list<point> cottonFields <- list<point>(3 among hydroelectric);
	woodFields <- list<point>(3 among geothermal);
    //créer le graphe du réseau des trains tel que fait dans reseauTrain.gaml 
    
    
    }*/
    
	
}

species road schedules: []  {
    float destruction_coeff <- rnd(1.0,2.0) max: 2.0;
    int colorValue <- int(255*(destruction_coeff - 1)) update: int(255*(destruction_coeff - 1));
    rgb color <- rgb(min([255, colorValue]), max ([0, 255 - colorValue]),0)  update: rgb(min([255, colorValue]),max ([0, 255 - colorValue]),0) ;
    
    aspect base {
    draw shape color: color ;
    }
}

species MerchandiseManager{

	//farm_cell closestField;
	list<farm_cell> location_legumes;
	list<farm_cell> location_animaux;
	list<farm_cell> location_coton;
	list<forest> location_bois;
	bool not_init <- true;
	
	action init_list {
		
		loop i over: farm_cell.population{
//			write(i);
//			write(i.image);
			if i.image=0{ //si cest une ferme viande
		  		//if ville.location distance_to i.location < distance_min {
		  			//distance_min_legumes <- ville.location distance_to i.location;
		  			add item:i to: location_animaux;
				//}
			}
			if i.image=1{ //si cest un champ de legume
		  		//if ville.location distance_to i.location < distance_min {
		  			//distance_min_animaux <- ville.location distance_to i.location;
		  			add item:i to: location_legumes;
				//}
			}
			if i.image=2{ //si cest un champ vde coton
		  		//if ville.location distance_to i.location < distance_min {
		  			//distance_min_coton <- ville.location distance_to i.location;
		  			add item:i to: location_coton;
				//}
			}
		}
		loop i over: forest.population{
            add item:i to: location_bois;
        }
//        write(location_animaux);
//        write(location_legumes);
//        write(location_coton);
//        write(location_bois);
        not_init <- false;
	}
	
	list getClosestField ( point terminalPosition ,  string typeOfMerchandise, list excludedLocations){ 
		if (not_init){
//			write("test_init");
			do init_list;
		}
//		write(typeOfMerchandise);
		float dist_min <- #max_float;
		farm_cell fieldCell <- nil;
		float fieldStock <- 0.0;
		point fieldLocation <- nil;
		list<point> location_type;
		switch typeOfMerchandise {
			match "crop"{
				loop i over: (location_legumes){
					if not (i.location in excludedLocations) and (distance_to(terminalPosition,i.location)<dist_min){
						dist_min <- distance_to(terminalPosition,i.location);
						fieldCell <- i;
						}
					}
				}
			match "meat"{
				loop i over: (location_animaux){
					if not (i.location in excludedLocations) and (distance_to(terminalPosition,i.location)<dist_min){
						dist_min <- distance_to(terminalPosition,i.location);
						fieldCell <- i;
					}
				}
			}
			match "cotton"{
//				write("test cotton");
//				write(location_coton);
				loop i over: (location_coton){
					if not (i.location in excludedLocations) and (distance_to(terminalPosition,i.location)<dist_min){
						dist_min <- distance_to(terminalPosition,i.location);
						fieldCell <- i;
					}
				}
			}
			match "wood" {
				loop i over: (location_bois){
					if(distance_to(terminalPosition,i.location)<dist_min){
						dist_min <- distance_to(terminalPosition,i.location);
						fieldLocation <- i.location;
						fieldStock <- #max_float; //le stock de bois est vraiment énorme, et on ne peut pas le récupérer depuis forest
					}
				}
			}
		}
		//TODO : Ajouter le bois
		if fieldCell != nil{
			return [fieldCell];
		}
		else {
			return  [fieldLocation, fieldStock];
		}
	}
	
	truck getClosestFreeTrucks(point cField){
		list<truck> trucks <- list<truck>( nb_truck among truck );
		map<bool, list<truck>> free_trucks <- trucks group_by (each.free = true);
		float min_dist <- #infinity;
		truck closest_truck <- nil;
		loop free_truck over: free_trucks[true] {
			float dist <- free_truck.location distance_to cField ;
			if min_dist > dist {
				min_dist <- dist;
				closest_truck <- free_truck;
			}
		}
		assert closest_truck != nil;
		return closest_truck;
	}
	
	//TODO : Utiliser commandMerchandise dans les villes qui ont besoin de ressource
	
	action CommandMerchandise( MiniCity mn_city, point terminalPosition ,string  type ,float quantity){
		list excludedFieldLocations <- [];
		list L <- getClosestField(terminalPosition , type, []);
		point fieldLocation;
		float fieldStockRemaining;
		farm_cell fieldCell;
		if type = "wood"{
			fieldLocation <- L[0];
			fieldStockRemaining <- L[1];
		}
		else {
			fieldCell <- L[0];
			fieldLocation <- fieldCell.location;
			fieldStockRemaining <- fieldCell.quantity_available;
		}
		float still_need <- quantity;
		loop while:still_need > 0 { //Tant que la capacité collective de tous les camions commandés ne suffit pas,
									 // on en cherche d'autres
			truck closestTruck <- getClosestFreeTrucks(fieldLocation);
			ask closestTruck{
				free <- false;
				float quantitySampled <- min(fieldStockRemaining, maxCapacity - currentCapacity, still_need);
				fieldStockRemaining <- fieldStockRemaining - quantitySampled;
				if type != "wood"{
					fieldCell.quantity_available <- fieldCell.quantity_available - quantitySampled; //reduce quantity_available in the field
					targetField <- fieldCell;
				}
				still_need <- still_need - quantitySampled;
				targetOrder <- quantitySampled;
				//do moveTo  m_city: m_city fieldPosition: closestField  terminalPosition: terminalPosition  typeOfMerchandise: typeOfMerchandise  quantity: quantity ;
				fieldPosition <- fieldLocation;
				target <- fieldLocation;
				cityPosition <- terminalPosition;
				typeOfMerchandise <- type;
				initPosition <- location;
				if type="wood"{
					state <- "goingToForest";	
				}
				else {
					state <- "goingToField";
				}
				m_city <- mn_city;		
			}
			if still_need > 0 and fieldStockRemaining = 0{
				// SI LE CHAMP COURANT N'A PLUS LA CAPACITE PROJETEE POUR REMPLIR LA COMMANDE, ON ENVOIE LES CAMIONS VERS UN AUTRE CHAMP
				add item:fieldLocation to:excludedFieldLocations;
				L <- getClosestField(terminalPosition, type, excludedFieldLocations);
				fieldLocation <- L[0];
				fieldStockRemaining <- L[1];
			}
		}
	}
}

species truck skills:[moving] {
	bool free;
	string merchandiseTransported ;
	//farm_cell targetField ;
	point target;
	farm_cell targetField;
	point fieldPosition;
	point cityPosition;
	string state;
	string typeOfMerchandise;
	point initPosition;
	MiniCity m_city;
	float maxCapacity;
	float currentCapacity;
	float targetOrder;
	init{
		target <- nil;
		merchandiseTransported <- "None";
		free <- true;
		currentCapacity <- 0.0;
		maxCapacity <- MAX_CAPACITY;
	}
	
//	reflex moveToField when: state= "goingToField"{
//		target <- fieldPosition;
//	}
//	
//	reflex moveToCity when: state = "goingToCity"{
//		target <- cityPosition;
//	}
	
	reflex move when: target != nil{

        do goto target:target speed:truckSpeed;
//        write "truck "+self+"is "+state;
        if(location = cityPosition and state = "goingToCity"){

		  //write(state +"  " + typeOfMerchandise);
          //write "truck "+self+"arrived to city";
        	state <- "free";
        	truckCo2_emitted <- truckCo2_emitted+ distance_to(fieldPosition,initPosition)*truck_CO2_Emission;
			truckEnergy_used <- truckEnergy_used+ distance_to(fieldPosition,initPosition)*truckEnergy;
     		greenhouse_emissions <-greenhouse_emissions + truckCo2_emitted+ distance_to(fieldPosition,initPosition)*truck_CO2_Emission/1000;
        	 //TODO : Ajouter la possibilité pour un camion de transporter des ressources entre les villes selon les 
        	 //différents stocks (les échanges intervilles doivent etre possibles) 
        	 
        	ask m_city {
        	 	//Pas besoin de vérifier les quantités, normalement commande est fait de manière a ce que ce soit déjà bon
        	 	do giveResource (myself.typeOfMerchandise, myself.currentCapacity);
        	 	myself.currentCapacity <- 0.0;
        	}
        	 
        	free <- true;
        	 
        }
        if(location overlaps targetField and state = "goingToField"){
        	//write "truck "+self+"arrived to field";
        	truckCo2_emitted <- truckCo2_emitted+ distance_to(self.location,cityPosition)*truck_CO2_Emission;
			truckEnergy_used <- truckEnergy_used+ distance_to(self.location,cityPosition)*truckEnergy;
			greenhouse_emissions <- greenhouse_emissions+ distance_to(self.location,cityPosition)*truck_CO2_Emission/1000;
			
			switch targetField.image{
				match 0{
					if(targetField.meat>=targetOrder){
							targetField.meat<-targetField.meat-targetOrder;
							self.currentCapacity<-targetOrder;
						}else{
							self.currentCapacity<-targetField.meat;
							targetField.meat<-0.0;
						}
				}
				match 1{
					if(targetField.crop>=targetOrder){
							targetField.crop<-targetField.crop-targetOrder;
							self.currentCapacity<-targetOrder;
						}else{
							self.currentCapacity<-targetField.crop;
							targetField.crop<-0.0;
						}
				}
				match 2{
					if(targetField.cotton>=targetOrder){
							targetField.cotton<-targetField.cotton-targetOrder;
							self.currentCapacity<-targetOrder;
						}else{
							self.currentCapacity<-targetField.cotton;
							targetField.cotton<-0.0;
						}
				}
			}
        	state <- "goingToCity";	
        	target <- cityPosition;
        	targetOrder <- 0.0; 
        }
        if(location = fieldPosition and state = "goingToForest"){
        	ask one_of (ForestCamp){
        		do cut(myself.targetOrder / treeMass); 
        	}
        	currentCapacity <- targetOrder;
        state <- "goingToCity";	
        target <- cityPosition;
        }
        if (location = target) {
        	//write "truck "+self+"arrived to target";
            target <- nil;
        } 
    }
	
	aspect triangle {
   		draw  triangle(2#px) color: #green ; // triangle pour apparaitre comme une flèche signifie quelque chose qui est en mouvement comme un camion 
   										  //couleur verte car il roule avec electricité et c'est l'énergie verte 
    }
}