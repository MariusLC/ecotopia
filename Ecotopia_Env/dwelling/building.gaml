/**
* Name: building
* Based on the internal empty template. 
* Author: Mouna Benabid, Damien Marillet, Manel Khenifra, Johann Alaya
* Tags: dwelling
*/

model building

import "../general/individual.gaml"
import "../Energy/skeleton.gaml"
import "../general/parameters.gaml"
import "../Ecosystem/ecosystem.gaml"

global {
	
	
	int hour_day<-0;
	int currentDay <-0; // 0 = lundi, 1 = mardi, ... etc

	reflex hour when: every (1 #hour) {
		hour_day <- hour_day + 1 ;
	}

	reflex dayPasses when: every (1 #day)
	{
		currentDay<-(currentDay+1) mod 7;
		hour_day <- 0;
	}
	
	int idForNextInd <- 0;
	int individuIDHandler {
		idForNextInd <- idForNextInd+1;
		return idForNextInd -1;
	}
	reflex updateBuildingsIndicators
	{
		currentNbOfResidentsAtHome <-   ( (agents of_generic_species individu) count (each overlaps each.house) ) ;
		currentNbOfEmployeesAtWorkPlace <-   (( (agents of_generic_species individu) where (each.workPlace!= nil) ) count ( each overlaps each.workPlace) ) ;
		nbOfPossibleResidents <- (agents of_generic_species housing) sum_of (each.maxCapacity);
		nbOfPossibleEmployees <- (agents of_generic_species company) sum_of (each.maxNbEmployees);
		densityOfPeopleInTheirHouse <- currentNbOfResidentsAtHome/nbOfPossibleResidents;
		densityOfPeopleInTheirWorkPlace <- currentNbOfEmployeesAtWorkPlace/nbOfPossibleEmployees;
	}	
	
}


//for debug purposes
species dot {
	aspect default{
		draw square(1) color:#violet;
	}
}

species building {
	
	list<float> dimensions ; // x: length * y: width * z: height
	MiniCity minicity;
	rgb color;
	BuildingEnergy myModulEnergy;
	
	
	int getNbPeople{
		warn "[building][getNbPeople] : Cette fonction ne devrait pas appelé.";
		return 0;
	}
	
	//initialize color, dimensions, and shape of agents ( used in child classes of bulding)
	action initBuilding(rgb bcolor,float floor_area,int min_floors,int max_floors){
		
		/* bcolor will be color used to draw the building
		 * floor_area is the floor area of the building in m2
		 * the number of floors of the building is an int randomly chosen between min_floors and max_floors
		 */
		 
		self.color<-bcolor; //color used later in aspect
		
		list<float> dim <- self.generateWidthAndLength( floor_area ); // Get length and width to have an area of floor_area m2
		 
		float height <- mean_floor_height * rnd(min_floors,max_floors); // The number of floors can vary between min_floors and max_floors 
		
		add height to: dim; 
		self.dimensions <- dim; 
		
		//modify agent shape
		self.shape<-rectangle(self.dimensions[0],self.dimensions[1]);
		
		//energy
		create BuildingEnergy returns: build{
        	self.cons_1_pers  <- Gcons_1_pers;
    		self.consomachine <- Gelectric_cons ;
    		area <- myself.dimensions[0] * myself.dimensions[1];
    		self.volume <- myself.dimensions[0] * myself.dimensions[1] * myself.dimensions[2];
    		int nb <- nbSolarPanel_Eolian(int(myself.dimensions[0]), int(myself.dimensions[1]));
		    self.nb_solar <- nb;
	  		
	   		self.nb_windmills <- nb;
//	   		self.sunshine <- 1.0;
	   		
			self.myBuilding <- myself;
		}
		myModulEnergy <- build at 0;
		
	}
	
	
	list<float> generateWidthAndLength (float floor_area) { // Returns randomly generated length and width for a building given a floor area
	
		/* With the next line, we randomly choose the dimensions BUT while ensuring that the length is at least half the value of width 
		and at most twice the value of width : this ensures the buildings are not disproportionnaly large or disproportionnaly thin.*/
		float length <- rnd(sqrt(floor_area/2), sqrt(2 * floor_area)); 
		float width <- floor_area/length;
		
		// Assert size proportions are correct
		assert (1/2) <= length/width;
		assert length/width <= 2;
		
		// Assert the product constraint is respected
		assert length * width = floor_area;
		
		return list<float>(length,width);
	}
	
	bool checkIfFits //to check if a building collides with another building in its miniCity - if its exists- ( used for assert )
	{
		if ( self.minicity = nil)
		{
			return true;
		}
		loop b over:self.minicity.l_buildings
		{
			if ( (b!= self) and (b.shape inter self.shape != nil))
			{
				return false;
			}
		}
		return true;
	}
	
	int getNbPeople virtual:true{
		warn "[building][getNbPeople] : Cette fonction ne devrait pas appelé.";
		return 0;
	}
	
	
	aspect default{
		draw shape  color: self.color;
	}
}

species housing parent: building {
	int minCapacity;
	int maxCapacity;
	int numberResidents <- 0;
	list<individu> residents <- [];
	

	
	int getNbPeople {
		return length(residents);
	}
	
	reflex bioWasteProduction when: hour_day = 0 {
//		write "biomass_waste "+biomass_waste;
		biomass_waste <- biomass_waste + biowaste_per_individual*numberResidents;
		biomass_waste_building <-biomass_waste_building + biowaste_per_individual*numberResidents;
		biomass_waste_produced <- biomass_waste_produced + biowaste_per_individual*numberResidents;
		
//		write "biomass_waste+ "+biowaste_per_individual*numberResidents;
//		write "biomass_waste "+biomass_waste;
	}
	

	bool addResident (individu resident) {
			
		if (numberResidents < maxCapacity) {
			add item: resident to: residents;
			numberResidents <- numberResidents + 1;
			resident.house <- self;
			return true;
		}
		else {
			return false;
		}
	}
	
	bool addResidents (list<individu> l_residents) { 
		if (maxCapacity - numberResidents > 0) {
			loop i over: l_residents {
				add i to: residents;
				numberResidents <- numberResidents + 1;
				i.house <- self;
			}
			return true;
		}
		else {
			return false;
		}
	}
	
	bool removeResident (individu resident) {
		
		remove key: resident from: residents;
		numberResidents <- numberResidents - 1;
		return true;
	}
	
	int getNbPeople {
		return length(residents);
	}
	
	reflex dismantleEmptyHouse when: every(1 #week){
		if self.numberResidents = 0{
			//dismantle the house and get rid of all references to this house
			remove self from: self.minicity.l_buildings;
			ask myModulEnergy{
				do die;
			}
			do die;
			
		}
	}

}

species woodenHouse parent: housing {
	int numberFloors;
	
	init {
		numberFloors <- rnd(min_nb_floors_wooden_houses,max_nb_floors_wooden_houses);
		do initBuilding(#cyan,wooden_house_floor_area,numberFloors,numberFloors);
		minCapacity <- min_dwelling_cap*numberFloors; 
		maxCapacity <- max_dwelling_cap*numberFloors;
		


	
		
	}
	
	
	// DO NOT use this method apart of when you are creating a new instance
	action resetSpecificNumberOfFloors(int newNbOfFloors){
		//an assert to make sure nobody misuses the method ( see above )
		assert self.residents = [] and numberResidents=0;
		
		float height <- mean_floor_height * newNbOfFloors; // new height
		self.dimensions[2] <- height; // replacing previous height
		
		//update the house capacities
		minCapacity <- min_dwelling_cap*newNbOfFloors;
		maxCapacity <- max_dwelling_cap*newNbOfFloors;
	}
	
}

species modularHouse parent: housing {
	int numberRooms;
	
	init {
		do initBuilding(#cyan,modular_house_floor_area,1,1);
		minCapacity <- min_dwelling_cap; 
		maxCapacity <- max_dwelling_cap;
		
	}
	
	action addRoom {
		if (numberRooms < maxCapacity) {
			ask self.minicity {
				if askForResource("plastic", plastic_consumption_per_house/myself.maxCapacity) /* Temporary value */ {
					// Does the construction of a new room emit CO2 ?
					// To add : change dimensions
					// EFM
					plastic_conso <- plastic_conso + plastic_consumption_per_house/myself.maxCapacity;
					//
					myself.numberRooms <- myself.numberRooms + 1;
					return true;
				}
				else {
					return false;
				}
			}
		}
		else {
			return false;
		}
	}
	
	action removeRoom {
		// To add : recycling
		if (numberRooms > minCapacity){
			numberRooms <- numberRooms - 1;
			return true;
		}
		else {
			return false;
		}
	}

}

// Company from ecosystem
//species company {
//	
//	int start_work <- rnd (min_work_start, max_work_start);
//    int end_work <- rnd(min_work_end, max_work_end);
//	int time_work <- end_work - start_work ;
//	
//   
//}

species company parent: building {
	
	int numberEmployees <- 0;
	list<individu> employees <- [];
	int maxNbEmployees;
	bool open <- false;
	int openTime <- 8;
	int closingTime <- 20; 
	
	init {
		maxNbEmployees <- max_number_employees;
		numberEmployees <- 0;
	}
	bool addEmployees (list<individu> l_ind) { // Try to add all agents at once in the list of employees (return true if the operation is a success, return false otherwise) 
		if (maxNbEmployees - numberEmployees > 0) {
			list<individu> indToAdd<- [];
			loop i over: l_ind {
				add i to: indToAdd;
				//TODO: on ajoute jamais d'employees
				numberEmployees <- numberEmployees + 1;
			}
			loop i over: indToAdd{
				add i to: employees;
			}
			return true;
		}
		else {
			return false;
		}
	}
	bool addEmployee (individu ind) { // Try to add a single agent (return true if the operation is a success, return false otherwise) 
		return addEmployees([ind]);
	}
	action removeEmployee(individu ind){
		//write "l'agent "+ind+ " a quitté l'entreprise "+self;
		remove ind from: self.employees;
		numberEmployees <- numberEmployees - 1;
	}
	reflex open when: hour_day >= openTime and currentDay!=5 and currentDay!=6{ //on va pas au travail samedi ou dimanche
		//write("L'entreprise " + self.name + " ouvre.");
		open <- true;
	}
	
	reflex close when: hour_day >= closingTime {
		//write("L'entreprise " + self.name + " ferme.");
		open <- false;
	}
	
	int getNbPeople {
		return numberEmployees;
	}
	
	/*
	reflex dismantleEmptyCompany when: every(1 #week)
	{
		//essential buildings
		if type_of(self)= trainStation or healthCenter{
			return;
		}
		write " dismantling company "+self;
		if (self.numberEmployees = 0)
		{
			//dismantle the company and get rid of all references to this company
			remove self from: self.minicity.l_buildings;
			ask myModulEnergy
			{
				do die;
			}
			do die;
		}
	}
	*/
	
}

species factory parent: company {
	bool currentlyProducing <- true;
	action gasEmissions virtual: true;
	action startProduction {
		currentlyProducing <- true;
	}
	action stopProduction {
		currentlyProducing <- false;
	}
}

species plasticFactory parent: factory {
	init {
		//the height for a plastic Factory can vary between 2 and 4 mean floor height (arbitrary choices)
		do initBuilding(#darkblue,plastic_factory_floor_area,2,4);
		
	}
	
	float gasEmissions {
		return plastic_co2_emissions;
	}
	
	reflex plasticProduction when:currentlyProducing{
		ask self.minicity {
			if (askForResource("cotton", cotton_consumption_for_plastic)) {
		 		do giveResource("plastic", plastic_production);
		 		// EFM
		 		cotton_conso <- cotton_conso + cotton_consumption_for_plastic;
		 		plastic_prod <- plastic_prod + plastic_production;
		 		//
		 		greenhouse_emissions <- greenhouse_emissions + myself.gasEmissions();
		 		greenhouse_emissions_produced <- greenhouse_emissions_produced + myself.gasEmissions();
		 		plastic_ghg_emissions <- plastic_ghg_emissions + myself.gasEmissions();
		 	}
		 	/*else {
		 		write "no more cotton :(";
		 	}*/
		}
	}

}

species activitySchedule schedules: [] {
	string activityName;
	int day;
	int startTime;
	int endTime;
	
	bool checkSchedule (int d, int t) {
		return (day = d and t > startTime and t < endTime);
	}
}


species business parent: company {
	string businessType;
	
	init {
		businessType <- shuffle(allBusinessTypes)[0]; // A business type is chosen amongst all the available types
		
		// The height of a business can vary between 1 and 2 mean floor height (arbitrary choices)
		do initBuilding(#turquoise,business_floor_area,1,2);

	}

}

species trainStation parent: company {
	init {
		// The height of a train station can vary between 3 and 4 mean floor height (arbitrary choices)
		do initBuilding(#darkred,train_station_floor_area,3,4);
	}
}

species healthCenter parent: company {
	int numberOfPatients <- 0;
	int capacity <- maxPatientsHospitalCapacity;
	map<individu,float> patientsMap <- []; // The individual associated with the time remaining before he/she can leave
	
	init {
		 // The height for a hospital can vary between 3 and 6 mean floor height (arbitrary choices)
		do initBuilding(#blue,hospital_floor_area,3,6);
		
	}
	
	bool addPatient (individu i) {
		if(numberOfPatients = capacity) 
		{
			// write " sorry mister or miss " + i.name + "but we're full";
			return false;
		}
		else 
		{
			float durationTime <- self.getDurationHospital(i);
			add durationTime at: i to: patientsMap;
			numberOfPatients <- numberOfPatients + 1;
			i.myHospital <- self;
			//write " the agent "+i+" is officialy taken care of in the hospital "+self;
			return true;
		}
	}
	
	float getDurationHospital (individu ind) { // in nb of tic (i.e. in hours)
		// Mean values from https://www.cdc.gov/mmwr/preview/mmwrhtml/mm5517a7.htm
		if (ind.age<65) {
			return gauss(5,1); // The mean is 5 days, the std as been arbitrarely chosen to be one day
		} 
		else {
			return gauss(7,1); // The mean is 7 days, the std is 1 day
		}
	}
	
	reflex patientsHandler when: every(#day) { // Decrements durationTime of the patients and filter out the patients who dont need to stay anymore
		map<individu, float> tmpMap <- [];
		
		loop i over: patientsMap.keys 
		{ 
			// make sure the state is coherent, all Patients in the hospital are sick
			assert i.myHospital!=nil;
			
			patientsMap[i] <- patientsMap[i] - 1; // Decrements durationTime

			if (patientsMap[i] > 0) { // Filter the map 
				add patientsMap[i] at: i to: tmpMap; 
			}
			else // the individual i is not sick anymore
			{
				assert i.myHospital!=nil; // to check that this part of indivu FSM works fine
				i.myHospital <- nil;
				//write "the hospital "+self+" is letting "+i+ " free";
			}
		}
		
		patientsMap <- tmpMap;
		//write "test for patients handler, after filtering" + patientsMap;
	}

}

species school parent: company {
	int numberOfStudents <- 0;
	int max_student_capacity <- maxStudentsCapacity;
	list<individu> students<- [];
	init {
		//The number of floors for a school can vary between 1 and 2 (arbitrary choices)
		do initBuilding(#lightblue,school_floor_area,1,2);
	}
	
	bool addStudents(list<individu> l_ind) { // Try to add all agents at once in the list of students (return true if the operation is a success, return false otherwise) 
		if (max_student_capacity - numberOfStudents > 0) 
		{
			list<individu> indToAdd<- [];
			loop i over: students {
				add i to: indToAdd;
				numberOfStudents <- numberOfStudents + 1;
			}
			loop i over: indToAdd{
				add i to: students;
			}
			return true;
		}
		else 
		{
			return false;
		}
	}
	
	bool addStudent (individu ind) { // Try to add a single agent (return true if the operation is a success, return false otherwise) 
		return addStudents([ind]);
	}
	action removeStudent(individu ind){
		//write "l'agent "+ind+ " a quitté l'ecole "+self;
		remove ind from: self.students;
		numberOfStudents <- numberOfStudents - 1;
	}
}

species funZone parent: company {
	init {
		// The number of floors for a fun zone can vary between 1 and 3 (arbitrary choices)
		do initBuilding(#orange,fun_zone_floor_area,1,3);
		
	
		list<activitySchedule> L <- [];
		list<string> openAct <- copy (activityList);
		int t <- 8; /* Heure d'ouverture */
		int maxTime <- 22; /* Heure de fermeture ARBITRAIRE, SUJET A CHANGEMENT */
		int d <- 0; /* On va parcourir les jours de la semaine pour créer aléatoirement une liste d'activité */
		/*TODO: Il est trop couteux de générer un emploi du temps pour CHAQUE funZone... en pratique, on en génère 5 ou 6 au hasard
		 * et on les affecte au hasard a chaque funZone. */
		loop while: (d < 7) {
			create activitySchedule number: 1 returns: a{
				string s <- one_of (openAct); /* A vérifier : one_of renvoie-t-il un choix aléatoire, ou juste le premier élément ? */
				activityName <- s;
				remove item: s from: openAct; /* On enlève l'activité de la liste des "ouverts" pour ne pas faire de doublons */
				day <- d;
				startTime <- t;
				endTime <- t + 2;
			}
			add item: a[0] to: L; /* Dégueulasse mais on peut pas faire autrement, y'a pas de create while*/
			t<-t+2;
			if t = maxTime {
				t <- 8;
				d <- d + 1;
			}
			
			if openAct = [] {
				openAct <- copy(activityList); /* Si on a parcouru toutes les activités, autoriser les doublons */
			}
		}
		activities <- L;

	}
	
	list<activitySchedule> activities; // String describes the type of activity, and list of 3 ints for schedules format:
											  // day / start time / end time
	int maxCapacity <- fun_zone_max_capacity;
	int currentCapacity <- 0;
	string currentActivity <- "";
	
	bool askToEnter (string activity) {
		if (currentCapacity <= maxCapacity and activity=currentActivity) {
			currentCapacity <- currentCapacity + 1;
			return true;
		}
		return false;
	}
	
	action leave {
		currentCapacity <- currentCapacity-1;
	}
	
	reflex updateSchedule {
//		activitySchedule a <- one_of (activities where each.checkSchedule(currentDay,hour_day));
		activitySchedule a <- shuffle(activities) first_with (each.checkSchedule(currentDay,hour_day));
		if(a!=nil)
		{
			currentActivity <- a.activityName;
		}
		else
		{
			currentActivity <- "closed";
		}
	}
}
species park parent: building
{
	int nbTrees;
	init
	{
		//a park has no roof/height
		do initBuilding(#green,standard_park_area,0,0);
		int TreesPerSquareKmInParks<- TreesDensityInParks*Gtreeperkm2;
		nbTrees <- round(self.dimensions[0]*self.dimensions[1]* (TreesPerSquareKmInParks/(10^6))); // 10^6 for conversion in square meters
		
	}
	action setNewFloorArea(float new_floor_area) // CAREFUL : to use BEFORE adding the park in a miniCity ( we are changing its dimensions: we dont want it to collide with an aleready added building now) !!
	{
		//a park has no roof/height
		do initBuilding(#green,new_floor_area,0,0);
		int TreesPerSquareKmInParks<- TreesDensityInParks*Gtreeperkm2;
		nbTrees <- round(self.dimensions[0]*self.dimensions[1]* (TreesPerSquareKmInParks/(10^6))); // 10^6 for conversion in square meters
		
		assert checkIfFits();
	}
	
	int getNbPeople {
		return 0;
	}

	
	
	reflex consumeGHG when: every(30 #days)  //every k=30 days
	{  
         float conso <- nbTrees * GtreeConsoGhg;
         //write "dayCount "+ #days+" conso :  "+conso+ "f or " + nbTrees+ "  trees";
         greenhouse_emissions <- greenhouse_emissions  - conso;
         greenhouse_emissions_consumed <- greenhouse_emissions_consumed  + conso;
     }

     reflex growthTrees when :  every(30 #days ) //every k=30 days
     {
     	nbTrees <- round(nbTrees * GcoefNaturalGrowth);
     }
}
