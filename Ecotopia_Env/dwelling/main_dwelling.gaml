/**
* Name: main
* Based on the internal empty template. 
* Author: Mouna Benabid, Damien Marillet, Manel Khenifra, Johann Alaya
* Tags: dwelling
*/


model main

import "building.gaml"
/* File to test our classes*/


global {
	geometry shape <- rectangle(1200#m,1200#m); //shape of the world ( for display)
	
	// Parameters
	int maxCap <- 20;
	int minCap <- 5;
	float percentWood <- 0.9;
	float percentBusiness <- 0.25;
	float percentFunZone <- 0.25;
	float percentFactory <- 0.25;
	float percentSchool <- 0.25;
	float percentHealthCenter <- 0.25;
	int nb_solar <- 3; // Temporary value
	int nb_eolian <- 1; // Temporary value
	float biowaste_per_ind <- 1.786; // kg ; Biowaste production per individual per day
	float factory_ges_emission <- 1.3; // GES/kg of bioplastic produced
	
	
	//temporary : for test purposes
	MiniCity mc1;
	MiniCity mc2;
	

	init {
		/* CREATION DES INDIVIDUS !!DOIT RESTER EN HAUT CAR SONT UTILISES POUR L'INIT DES COMPANY!! */
		create individu number: 100 returns: ind_list;
		// CREATING TWO MINI-CITIES AND MAKING SURE THEY DONT COLLIDE
		create MiniCity number: 2 returns:mcl{
			diameter <- mean_diameter_minicity;
			resourceStock <- create_map(["cotton","plastic"], [1000.0,0.0]);
			create plasticFactory number: 3 returns: pf_l {
				minicity <- myself;
			}
			loop pf over: pf_l { 
				bool hasSucceded<- addBuilding(pf);
				write " plastic factory added successfully ? "+ hasSucceded;
				if not hasSucceded{
					ask pf{
						do die;
					}
				}
			}
			write self.location;
		}

		mc1<- mcl[0];
		
		mc2<- mcl[1];
		ask mc2 //making sure mc2 dont collide with m1
		{
			if ( (mc1 inter mc2)!=nil )
			{
				loop while: ( (mc1 inter mc2)!=nil )
				{
					write 'moving';
					do moveMiniCity(mc2.location-{100,100});
				}
				
			}
		}
		
		list<individu> whResidents <- [];
		list<individu> mdResidents <- [];
		
		int nb_ind <- round(percentWood * length(ind_list));
		
		loop h from: 0 to: nb_ind - 1 { 
			add ind_list[h] to: whResidents;
		}
		loop h from: nb_ind to: length(ind_list) - 1 { 
			add ind_list[h] to: mdResidents;
		}
		
		int nbWoodenHouse <- 2;
		int i <- 0;
		 
		create woodenHouse number: nbWoodenHouse {
			i <- i + 1;
			maxCapacity <- maxCap;
			minCapacity <- minCap;

			int l <- round(length(whResidents)/nbWoodenHouse);
			
			loop j from: (l*i) - l to: (l*i)-1 { 
				do addResident (whResidents[j]);
				whResidents[j].house <- self; // chaque habitant doit connaitre sa maison  
			}
			write "Test list residents WH: " + residents;
			
			woodenHouse w <- self;
			
			float random <- rnd(0.0, 1.0);
			
			if (random < 0.5){
				ask mc1 {
					bool hasSucceded<- addBuilding(w);
					write " wooden house added successfully ? "+ hasSucceded;
					if not hasSucceded{
						ask w {
							do die;
						}
					}
				}
			}
			else {
				ask mc2 {
					bool hasSucceded<- addBuilding(w);
					write " wooden house added successfully ? "+ hasSucceded;
					if not hasSucceded{
						ask w {
							do die;
						}
					}
				}
			}
		}
		

		int nbModularHouse <- 2;
		i <- 0;
		
		create modularHouse number: nbModularHouse {
			i <- i + 1;
			maxCapacity <- maxCap;
			minCapacity <- minCap;
			
			int l <- round(length(mdResidents)/nbModularHouse);
			
			loop j from: (l*i) - l to: (l*i)-1 { 
				do addResident (mdResidents[j]);
				mdResidents[j].house <- self;
			}
			
			write "Test list residents MD: " + residents;
			
			modularHouse m <- self;
			
			float random <- rnd(0.0, 1.0);
			
			if (random < 0.5){
				ask mc1 {
					bool hasSucceded<- addBuilding(m);
					write " modular house added successfully ? "+ hasSucceded;
					if not hasSucceded{
						ask m {
							do die;
						}
					}
				}
			}
			else {
				ask mc2 {
					bool hasSucceded<- addBuilding(m);
					write " modular house added successfully ? "+ hasSucceded;
					if not hasSucceded{
						ask m {
							do die;
						}
					}
				}
			}
		}
		
		create funZone number:1 returns:fz;
		ask mc1{
			do addBuilding(fz[0]);
		}

		
	
		 /*// try adding a park
		    create park returns: parks;
		    park p<- parks[0];
		    ask mc1{
				bool hasSucceded<- addBuilding(p);
				write " park added successfully ? "+ hasSucceded;
				if not hasSucceded{
					ask p{
						do die;
					}
				}
			}
			do pause;
			//check if asset for changing park dimensions works
			ask p
			{
				write self.city;
				do setNewFloorArea(5000);
			}
			do pause;
	}*/
	
	/*reflex testBuildingsAllocation when: every(100#cycles){
		
		//assert the mini-cities references have been instanciated 
		if (mc1!=nil and mc2!=nil) 
		{
			
			//try adding a business to mc1
			create business number:1 returns: l_b;
			business bs <- l_b[0];
			ask mc1{
				bool hasSucceded<- addBuilding(bs);
				write " business added successfully ? "+ hasSucceded;
				if not hasSucceded{
					ask bs{
						do die;
					}
				}
			}
		    do pause;
		    
		    // try adding a park to mc2
		    create park returns: parks;
		    park p<- parks[0];
		    ask mc2{
				bool hasSucceded<- addBuilding(p);
				write " park added successfully ? "+ hasSucceded;
				if not hasSucceded{
					ask p{
						do die;
					}
				}
			}
*/
		}
	}
 
experiment main type: gui {
	
	parameter "Max capacity of residents per apartment/house" var: maxCap min: 7 max: 25;
	parameter "Min capacity of residents per apartment/house" var: minCap min: 1 max: 7;
	parameter "Percentage of wooden houses compared to modular houses" var: percentWood min: 0.0 max: 1.0;
	parameter "Percentage of businesses compared to other companies" var: percentBusiness min: 0.0 max: 1.0;
	parameter "Percentage of fun zones compared to other companies" var: percentFunZone min: 0.0 max: 1.0;
	parameter "Percentage of factories compared to other companies" var: percentFactory min: 0.0 max: 1.0;
	parameter "Percentage of schools compared to other companies" var: percentSchool min: 0.0 max: 1.0;
	parameter "Percentage of health centers compared to other companies" var: percentHealthCenter min: 0.0 max: 1.0;
	parameter "Number of solar panels per wooden house" var: nb_solar min: 0 max: 10; // Temporary value
	parameter "Number of eolians per wooden house" var: nb_eolian min: 0 max: 3; // Temporary value
	parameter "Biowaste production per individual per day in kg" var: biowaste_per_ind min: 1.2 max: 2.0; // kg Temporary value
	parameter "GES/kg of bioplastic produced by plastic factories" var: factory_ges_emission min: 1.0 max: 1.7; // Temporary value
    		
    
    output {
    	/*inspect "plastic_factory_inspector" value: plasticFactory type: agent;
    	inspect "hospital_inspector" value: healthCenter type: table;*/
    	inspect "individu_inspector" value: individu type: table;
    	/*inspect "business_inspector" value: business type: table;
    	inspect "schools_inspector" value: school type: table;*/
    	inspect "funzone_inspector" value: funZone type: table;
    	//inspect "woodenhouse_inspector" value: woodenHouse type: table;
    	//inspect "modularhouse_inspector" value: modularHouse type: table;
    	//inspect "park_inspector" value: park type: table;
    	//inspect "miniCities_inspector" value: miniCity type: table;
    	// inspect "plastic_factory_inspector" value: plasticFactory type: table;
    	//monitor "Co2 emitted" value: global_co2_emissions;

    	// from GAMA website : By default, the resolution of the output image is 500x500px (note that when no unit is provided, the unit is #px (pixel) ).
    	display MiniCities background: #lightblue type:opengl
    	{
    		species individu;
    	
			//camera_pos<- mc.location;
			
    		species MiniCity;
    		//species agents of_generic_species housing;
    		species modularHouse;
    		species woodenHouse;
    		species trainStation;
    		species school;
    		species business;
    		species park;
    		species healthCenter;
    		species funZone;
    		species plasticFactory;
    		//species dot;
    	}
    }
}
