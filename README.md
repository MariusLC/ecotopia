# Ecotopia Groupe B

Ce projet s'inscrit dans le cadre du cours MOSIMA du master ANDROIDE de Sorbonne Université, encadré par Jean-Daniel Kant, Cédric Herspon et Maël Franceschetti. L'objectif est de simuler la Californie telle qu'elle est décrite dans le livre Ecotopia afin de répondre à la question suivante : la société décrite dans le livre permet-elle de maintenir l'équilibre écologique ?

## Installation

Pour télécharger le projet, entrez la commande suivante dans un terminal à l'emplacement désiré :

    git clone https://gitlab.com/ecotopia/worldB.git
    
Le projet est réalisé dans l'environnement GAMA (https://gama-platform.org/). Vous devez possèder la version _Alpha Version 1.8.2_, avec JDK. Consulter le lien suivant : 

    https://github.com/gama-platform/gama/releases
    
## Configuration

Pour tester l'implémentation, ouvrez GAMA, et assurez vous d'avoir ajouté le dossier ```Ecotopia_Env``` aux _User models_.

Pour ce faire, dans l'onglet _Models_, faites un clique droit sur _User models_, puis sélectionnez _Import..._ -> _GAMA Project..._. Cochez _Select root directory_, puis appuyez sur le bouton _Browse..._ à côté. Dans l'explorateur de fichier, sélectionnez le dossier _worldB/Ecotopia_Env_.

## Lancer une expérience

Pour lancer une expérience, vous pouvez opérer de différentes manières.

### Expérience avec gui

Pour obtenir une expérience avec visualisation, dans GAMA, ouvrez le fichier ```Ecotopia_Env/models/test_initialisation_MiniCity_Individu.gaml```, puis lancez l'expérience ```test_env```. La simulation s'ouvre et affiche la carte de la Californie ainsi que des graphes de données clées de simulation, comme la consommation énergétique par exemple.


### Expérience sans gui

On peut vouloir, pour des raisons de performances, exécuter des expériences sans afficher la visualisation, et/ou choisir d'exécuter plusieurs expériences en parallèle. Pour ce faire, ouvrez le fichier ```Ecotopia_Env/models/headless_logs.gaml```, et lancez l'expérience ```HEADLESS_LOGS```. Pour modifier le nombre d'exécutions en parallèle, vous pouvez modifier le paramètre de l'expérience ```repeat```, positionné à 3 par défaut.

### Exécution via terminal

Vous pouvez, pour améliorer encore les performances, lancer une expérimentation ou plusieurs via le terminal, voir ```https://gama-platform.org/wiki/Headless```. Le fichier ```.xml``` à utiliser est ```Ecotopia_Env/headless.xml```.


## Paramétrage

L'ensemble des paramètres de simulation sont définis dans ```Ecotopia_Env/general/parameters.gaml```, où ils sont rangés par catégorie. Vous pouvez choisir de les modifier directement ici en modifiant les valeurs, ou alors, après l'initialisation d'une expérience, vous pouvez changer les valeurs des paramètres grâce au panneau généralement situé sur la gauche. Une fois les paramètres changés, vous devez réinitialiser l'expérience, en cliquant sur le bouton dédié en haut de l'écran (flèche qui tourne) ou en appuyant sur ```Ctrl + R```.

## Génération des logs

Les fichiers de logs générés sont placés dans le dossier ```Ecotopia/logs``` dans un nouveau dossier dont le nom est de la forme ```'yyyy-MM-dd-HH-mm-ss'```. Les fichiers de logs générés sont les suivants : 

- ```demographic_logs.txt```, qui contient respectivement pour chaque colonne : 
    -  Le nombre d'individus **scolarisés**
    -  Le nombre d'individus **inactifs**
    -  Le nombre d'individus **actifs**
    -  Le nombre d'individus **étudiants** 
    -  Le nombre d'individus **malades** 
    -  Le nombre d'individus **hommes** 
    -  Le nombre d'individus **femmes**
-  ```general_logs.txt```, qui contient respectivement pour chaque colonne : 
    -  Le numéro de ligne (1 ligne par jour)
    -  **Quantité de gaz à effet de serre totale (GES)**
    -  La quantité de **déchets de biomasse**
    -  La **consommation en eau totale** (l)
    -  La **consommation en eau** par la production de **viande** et l'**agriculture** combinés
    -  Quantité de **GES** émis par la **génération d'électricité**
    -  Quantité de **biomasse consommée**
    -  **Consommation d'eau** pour la production de **viande** (l)
    -  **Consommation d'eau** pour l'**agriculture** (l)
    -  **GES** émis pour la production de **viande**
    -  **GES** émis pour l'**agriculture**
    -  Consommation des **déchets** liés à la production de **viande**
    -  Consommation des **déchets** liés à l'**agriculture**
-  ```nuclear_water.log```, qui contient respectivement pour chaque colonne : 
    -  Le numéro de ligne (1 ligne par jour)
    -  Quantité d'**eau** 'utilisée' par les **centrales nucléaires** (l)
-  ```Energy Building consumption.log```, qui contient respectivement pour chaque colonne : 
    -  Le numéro de ligne (1 ligne par jour)
    -  **Consommation électrique** des **bâtiments**
-  ```Energy Plastic consumption.log```, qui contient respectivement pour chaque colonne : 
    -  Le numéro de ligne (1 ligne par jour)
    -  **Consommation électrique** des usines de production de **plastique**
-  ```energy_tot_conso_weekly.log```, qui contient respectivement pour chaque colonne : 
    -  Le numéro de ligne (1 ligne par jour) (le fichier est mal nommé)
    -  **Consommation électrique totale**
-  ```Energy energy_tot_prod_weekly.log```, qui contient respectivement pour chaque colonne : 
    -  Le numéro de ligne (1 ligne par jour) (le fichier est mal nommé)
    -  **Production électrique totale**
         
## Membres de l'équipe B

Damien Legros (_Référent général_) (_Environnement_)
Cédric Cornede (_Ministre de l'Environnement_)
Marius Le Chapelier (_Responsable de merge Environnement_)
Petar Calic (_Environnement_)

Mouna Benabid (_Ministre du Logement_)
Damien Marillet (_Responsable de merge Logement_)
Johann Alaya (_Logement_)
Manel Khenifra (_Logement_)

Roza Amokrane (_Ministre du Transport_)
Julien Willaime-Angonin (_Responsable de merge Transport_)
Khalil Derras (_Transport_)
Smail Zidelmal (_Transport_)

Bruce Rose (_Ministre de l'Energie_)
Axel Foltyn (_Responsable de merge Energie_)
Ghada Amairi (_Energie_)
Nabila Ouldbelkacem (_Energie_)
